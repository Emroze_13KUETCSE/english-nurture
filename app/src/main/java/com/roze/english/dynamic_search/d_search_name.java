package com.roze.english.dynamic_search;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.Std_details;

import java.util.ArrayList;
import java.util.List;

public class d_search_name extends AppCompatActivity {
    public static String PREFS_NAME="math";

    EditText et_roll;

    String roll_et="";
    ListView lv;
    DatabaseHelper db_helper = new DatabaseHelper(this);

    String arry[][];

    String ss="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d_search_name);


        et_roll = (EditText) findViewById(R.id.et_roll);
        lv = (ListView) findViewById(R.id.list_v);

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor;

        cursor=db.rawQuery(" SELECT rowid _id, * FROM students",null);

        final int row = cursor.getCount();
        int i=0;
        arry = new String[row][5];

        if(cursor.moveToFirst()){
            do{
                arry[i][0] = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
                arry[i][1] = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
                arry[i][2] = cursor.getString(cursor.getColumnIndexOrThrow("nick_name"));
                arry[i][3] = cursor.getString(cursor.getColumnIndexOrThrow("school"));
                arry[i][4] = cursor.getString(cursor.getColumnIndexOrThrow("batch_time"));
                i++;
            }
            while(cursor.moveToNext());
        }

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        et_roll.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Toast.makeText(d_search.this, charSequence, Toast.LENGTH_SHORT).show();

                ss = String.valueOf(charSequence);

                ss = ss.toLowerCase();

                String s = String.valueOf(charSequence);
                int len = 0;

                for(int k = 0; k < row ;k++){
                    if(arry[k][1].toLowerCase().contains(ss)){
                        len++;
                    }
                }

                //Toast.makeText(d_search.this, String.valueOf(len), Toast.LENGTH_SHORT).show();

                String t[][] = new String[len][5];
                int lll = 0;
                for(int k = 0; k < row;k++){

                    if(arry[k][1].toLowerCase().contains(ss)){

                        t[lll][0] = arry[k][0];
                        t[lll][1] = arry[k][1];
                        t[lll][2] = arry[k][2];
                        t[lll][3] = arry[k][3];
                        t[lll][4] = arry[k][4];
                        lll++;
                    }
                }

                //Toast.makeText(d_search.this, t[1][0], Toast.LENGTH_SHORT).show();

                s_list_adapter listviewadapter;
                List<search_list_element_std_details> list_data = new ArrayList<search_list_element_std_details>();


                for (int j = 0; j < len; j++) {
                    search_list_element_std_details worldpopulation = new search_list_element_std_details(t[j][0],t[j][1]
                            ,t[j][2],t[j][3],t[j][4]);
                    list_data.add(worldpopulation);
                }

                listviewadapter = new s_list_adapter(getApplicationContext(), R.layout.s_by_class_and_batch_listview,
                        list_data);

                // Binds the Adapter to the ListView
                lv.setAdapter((ListAdapter) listviewadapter);

                if(len == 0){
                    len = 0;

                    for(int k = 0; k < row ;k++){
                        if(arry[k][1].toLowerCase().contains(ss)){
                            len++;
                        }
                    }
                    Toast.makeText(getApplicationContext(), String.valueOf(len), Toast.LENGTH_SHORT).show();

                    t = new String[len][5];
                    lll = 0;
                    for(int k = 0; k < row;k++){

                        if(arry[k][1].toLowerCase().contains(ss)){

                            t[lll][0] = arry[k][0];
                            t[lll][1] = arry[k][1];
                            t[lll][2] = arry[k][2];
                            t[lll][3] = arry[k][3];
                            t[lll][4] = arry[k][4];
                            lll++;
                        }
                    }
                    //Toast.makeText(d_search.this, t[1][0], Toast.LENGTH_SHORT).show();



                    for (int j = 0; j < len; j++) {
                        search_list_element_std_details worldpopulation = new search_list_element_std_details(t[j][0],t[j][1]
                                ,t[j][2],t[j][3],t[j][4]);
                        list_data.add(worldpopulation);
                    }

                    listviewadapter = new s_list_adapter(getApplicationContext(), R.layout.s_by_class_and_batch_listview,
                            list_data);

                    // Binds the Adapter to the ListView
                    lv.setAdapter((ListAdapter) listviewadapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                String str = tvv.getText().toString();

                editor.putString("temp_regiID",str);
                editor.commit();

                Intent inn = new Intent(getApplicationContext(), Std_details.class);
                startActivity(inn);
                editor.putInt("scroll_pos_search",i);
                editor.commit();
            }
        });
    }
}
