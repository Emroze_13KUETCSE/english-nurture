package com.roze.english.dynamic_search;

import android.content.Context;
import android.graphics.Typeface;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roze.english.R;

import java.util.List;

/**
 * Created by Emroze on 18-Dec-17.
 */

public class s_list_adapter extends ArrayAdapter<search_list_element_std_details> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<search_list_element_std_details> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public s_list_adapter(Context context, int resourceId,
                          List<search_list_element_std_details> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView regi;
        TextView f_name;
        TextView n_name;
        TextView school;
        TextView batch;
        TextView bg;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final com.roze.english.dynamic_search.s_list_adapter.ViewHolder holder;
        if (view == null) {
            holder = new com.roze.english.dynamic_search.s_list_adapter.ViewHolder();
            view = inflater.inflate(R.layout.s_by_class_and_batch_listview, null);
            // Locate the TextViews in listview_item.xml

            holder.regi = (TextView) view.findViewById(R.id.tv_regi_lv);
            holder.f_name = (TextView) view.findViewById(R.id.tv_full_name_lv);
            holder.n_name = (TextView) view.findViewById(R.id.tv_nick_name_lv);
            holder.school = (TextView) view.findViewById(R.id.tv_school_lv);
            holder.batch = (TextView) view.findViewById(R.id.tv_bt);
            holder.bg = (TextView) view.findViewById(R.id.tv_big_lv);


            Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");


            holder.regi.setTypeface(type);
            holder.f_name.setTypeface(type);
            holder.n_name.setTypeface(type1);
            holder.school.setTypeface(type1);
            holder.batch.setTypeface(type1);

            // Locate the ImageView in listview_item.xml
            //holder.flag = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (com.roze.english.dynamic_search.s_list_adapter.ViewHolder) view.getTag();
        }

        String str_big_latter ;
        String str_f_name = worldpopulationlist.get(position).getN_name();
        if(str_f_name.equals("")){
            str_big_latter="";
        }
        else {
            char c = str_f_name.charAt(0);
            str_big_latter = String.valueOf(c);
        }
        // Capture position and set to the TextViews
        holder.regi.setText(worldpopulationlist.get(position).getRegi());
        holder.f_name.setText(worldpopulationlist.get(position).getF_name());
        holder.n_name.setText(worldpopulationlist.get(position).getN_name());
        holder.school.setText(worldpopulationlist.get(position).getSchool());
        holder.batch.setText(worldpopulationlist.get(position).getBatch());
        holder.bg.setText(str_big_latter);

        // Capture position and set to the ImageView
        //holder.flag.setChecked(worldpopulationlist.get(position).getFlag());
        return view;
    }


    public List<search_list_element_std_details> getWorldPopulation() {
        return worldpopulationlist;
    }



    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
