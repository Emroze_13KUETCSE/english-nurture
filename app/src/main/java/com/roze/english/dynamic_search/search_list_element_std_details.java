package com.roze.english.dynamic_search;

/**
 * Created by Emroze on 18-Dec-17.
 */

public class search_list_element_std_details {
    String regi;
    String f_name;
    String n_name;
    String school;
    String batch;

    public search_list_element_std_details() {
    }

    public search_list_element_std_details(String regi, String f_name, String n_name, String school, String batch) {
        this.regi = regi;
        this.f_name = f_name;
        this.n_name = n_name;
        this.school = school;
        this.batch = batch;
    }

    public String getRegi() {
        return regi;
    }

    public void setRegi(String regi) {
        this.regi = regi;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getN_name() {
        return n_name;
    }

    public void setN_name(String n_name) {
        this.n_name = n_name;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

}
