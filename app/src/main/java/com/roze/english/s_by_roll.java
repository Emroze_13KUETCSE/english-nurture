package com.roze.english;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class s_by_roll extends AppCompatActivity {

    public static String PREFS_NAME="math";

    EditText et_roll;
    Button btn_search;
    String roll_et="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_by_roll);

        et_roll = (EditText) findViewById(R.id.et_roll);
        btn_search = (Button) findViewById(R.id.btn_search);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roll_et = et_roll.getText().toString();

                if(roll_et.equals("")){}
                else{
                    editor.putString("temp_regiID",roll_et);
                    editor.commit();
                    Intent inn = new Intent(getApplicationContext(), Std_details.class);
                    startActivity(inn);
                }

            }
        });

    }
}
