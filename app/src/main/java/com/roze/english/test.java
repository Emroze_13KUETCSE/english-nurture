package com.roze.english;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class test extends AppCompatActivity {
     TextView mTextView ;
     EditText et ;
     Button btn ;

    RequestQueue requestQ;



    private static final String URL = "http://gonitniketan.site90.com/insert.php";
    public static final String USERNAME = "regi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

         mTextView = (TextView) findViewById(R.id.tv);
         et = (EditText) findViewById(R.id.editText);
         btn = (Button) findViewById(R.id.sub);


        requestQ = Volley.newRequestQueue(getApplicationContext());

// Instantiate the RequestQueue.
        final RequestQueue queue = Volley.newRequestQueue(this);
        final String url ="http://gonitniketan.site90.com/android/insert.php";

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String regi = et.getText().toString().trim();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(response);
                                Toast.makeText(test.this,response,Toast.LENGTH_LONG).show();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(test.this,error.toString(),Toast.LENGTH_LONG).show();
                                mTextView.setText(error.toString());
                            }
                        }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();
                        params.put(USERNAME,regi);

                        return params;
                    }

                };

                queue.add(stringRequest);


            }
        });

    }
}
