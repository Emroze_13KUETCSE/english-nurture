package com.roze.english;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.roze.english.dynamic_search.d_search;
import com.roze.english.dynamic_search.d_search_name;
import com.roze.english.dynamic_search.d_search_school;

import net.cachapa.expandablelayout.ExpandableLayout;

import pl.droidsonroids.gif.GifTextView;

public class search extends AppCompatActivity {

    ExpandableLayout expandableLayout1;
    GifTextView g1;
    TextView tv_search,tv_roll,tv_class,tv_name,tv_school;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        g1 =(GifTextView) findViewById(R.id.gif1);


        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        tv_search = (TextView) findViewById(R.id.tv_search);
        tv_roll = (TextView) findViewById(R.id.tv_by_roll);
        tv_name = (TextView) findViewById(R.id.tv_by_name);
        tv_class = (TextView) findViewById(R.id.tv_by_class);
        tv_school = (TextView) findViewById(R.id.tv_by_school);


        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        tv_search.setTypeface(type1);

        tv_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(search.this,s_by_r_class_and_batch.class);
                startActivity(i);
            }
        });
        tv_roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(search.this,d_search.class);
                startActivity(i);
            }
        });
        tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(search.this,d_search_name.class);
                startActivity(i);
            }
        });

        tv_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(search.this,d_search_school.class);
                startActivity(i);
            }
        });

    }
}
