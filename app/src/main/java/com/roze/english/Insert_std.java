package com.roze.english;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Insert_std extends AppCompatActivity {

    EditText et_regi,et_fname,et_nname,et_faname,et_school,et_batchtime,et_mobile;

    RadioButton rb_7,rb_8,rb_9,rb_10,rb1,rb2,rb3,rb4,rb5,rb6,rb7,rb8,rb9,rb10,rb11,rb12;

    Button btn_submit;

    String insertUrl = "http://gonitniketan.site90.com/insert.php";

    RequestQueue requestQ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_std);

        requestQ = Volley.newRequestQueue(getApplicationContext());

        et_regi = (EditText) findViewById(R.id.et_regi);
        et_fname = (EditText) findViewById(R.id.et_fName);
        et_nname = (EditText) findViewById(R.id.et_nName);
        et_faname = (EditText) findViewById(R.id.et_faName);
        et_school = (EditText) findViewById(R.id.et_school);
        et_batchtime = (EditText) findViewById(R.id.et_btime);
        et_mobile = (EditText) findViewById(R.id.et_mobile);

        rb_7 = (RadioButton) findViewById(R.id.rb_7);
        rb_8 = (RadioButton) findViewById(R.id.rb_8);
        rb_9 = (RadioButton) findViewById(R.id.rb_9);
        rb_10 = (RadioButton) findViewById(R.id.rb_10);

        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);
        rb3 = (RadioButton) findViewById(R.id.rb3);
        rb4 = (RadioButton) findViewById(R.id.rb4);
        rb5 = (RadioButton) findViewById(R.id.rb5);
        rb6 = (RadioButton) findViewById(R.id.rb6);
        rb7 = (RadioButton) findViewById(R.id.rb7);
        rb8 = (RadioButton) findViewById(R.id.rb8);
        rb9 = (RadioButton) findViewById(R.id.rb9);
        rb10 = (RadioButton) findViewById(R.id.rb10);
        rb11 = (RadioButton) findViewById(R.id.rb11);
        rb12 = (RadioButton) findViewById(R.id.rb12);

        btn_submit = (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Showing the progress dialog
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        insertUrl, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error) {

                                //Toast.makeText(getActivity(), "Update successfully registered", Toast.LENGTH_LONG).show();

                            } else {

                                //String errorMsg = jObj.getString("error_msg");
                                //Toast.makeText(getActivity(),
                                //       errorMsg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> parameters = new HashMap<String, String>();
                        parameters.put("regi", et_regi.getText().toString());
                        parameters.put("full_name", et_fname.getText().toString());
                        parameters.put("nick_name", et_nname.getText().toString());
                        parameters.put("father_name", et_faname.getText().toString());
                        parameters.put("school", et_school.getText().toString());
                        parameters.put("batch_time", et_batchtime.getText().toString());
                        parameters.put("mobile", et_mobile.getText().toString());

                        parameters.put("jan", "1");
                        parameters.put("feb", "0");
                        parameters.put("mar", "0");
                        parameters.put("apr", "0");
                        parameters.put("may", "0");
                        parameters.put("june", "0");
                        parameters.put("july", "0");
                        parameters.put("aug", "0");
                        parameters.put("sep", "0");
                        parameters.put("oct", "0");
                        parameters.put("nov", "0");
                        parameters.put("dec", "0");


                        return parameters;
                    }
                };

                //Adding request to the queue
                requestQ.add(strReq);
            }
        });





    }
}
