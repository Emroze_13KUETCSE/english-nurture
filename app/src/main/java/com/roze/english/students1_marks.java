package com.roze.english;

/**
 * Created by emroze on 6/18/17.
 */

public class students1_marks {
    public String regi;
    public String batch_time;
    public String course;

    public students1_marks(){

    }

    public String getRegi() {
        return regi;
    }

    public void setRegi(String regi) {
        this.regi = regi;
    }

    public String getBatch_time() {
        return batch_time;
    }

    public void setBatch_time(String batch_time) {
        this.batch_time = batch_time;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
