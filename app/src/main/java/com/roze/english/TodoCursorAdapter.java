package com.roze.english;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by emroz on 4/4/2017.
 */

public class TodoCursorAdapter extends CursorAdapter {

    public TodoCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.tv_in_listview, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tvBody = (TextView) view.findViewById(R.id.tv_job_name_lv);
        TextView tvPriority = (TextView) view.findViewById(R.id.tv_batch_time_lv);
        TextView tvPriority1 = (TextView) view.findViewById(R.id.tv_course_lv);
        TextView tvPriority2 = (TextView) view.findViewById(R.id.tv_school_lv);
        TextView tvPriority3 = (TextView) view.findViewById(R.id.tv_nick_lv);


        // Extract properties from cursor
        String body = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
        String batch = cursor.getString(cursor.getColumnIndexOrThrow("batch_time"));
        String course = cursor.getString(cursor.getColumnIndexOrThrow("course"));
        String school = cursor.getString(cursor.getColumnIndexOrThrow("school"));
        String nick = cursor.getString(cursor.getColumnIndexOrThrow("nick_name"));

        // Populate fields with extracted properties
        tvBody.setText(body);
        tvPriority.setText(batch);
        tvPriority1.setText(course);
        tvPriority2.setText(school);
        tvPriority3.setText(nick);



        //tvPriority.setText(String.valueOf(priority));
    }
}
