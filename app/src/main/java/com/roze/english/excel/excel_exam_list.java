package com.roze.english.excel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.exam.choose_c_b_t2;
import com.roze.english.exam.exam_list;
import com.roze.english.helper.AppConfig;
import com.roze.english.students;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class excel_exam_list extends AppCompatActivity {

    public static String PREFS_NAME="math";
    RequestQueue requestQueue,requestQueue1,requestQueue2;
    String showUrl = "http://englishnurturebd.com/jason.php";
    String showUrl2 = "http://englishnurturebd.com/json.php";
    String showUrl3 = "http://englishnurturebd.com/json1.php";

    String BASE_URL = "http://englishnurturebd.com/";

    ProgressDialog pd;

    TextView tv1,tv2;

    ListView lv;

    Button btn;


    Cursor cursor_list;
    Cursor cursor_marks;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    String ex_name;

    Cursor cursor1;
    Cursor cursor;

    Cursor cursor_marks_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excel_exam_list);

        tv1 = (TextView) findViewById(R.id.tv_class_name);

        lv = (ListView) findViewById(R.id.sms_lv);

        btn = (Button) findViewById(R.id.btn_excel_ok);

        pd = new ProgressDialog(excel_exam_list.this);


        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv1.setTypeface(type1);

    }
    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        db_helper = new DatabaseHelper(this);

        SQLiteDatabase db = db_helper.getReadableDatabase();

        final String match_class = preference.getString("s_class","").toString();
        final String match_batch = preference.getString("s_batch","").toString();
        final String match_date = preference.getString("s_date","").toString();

        if(preference.getString("class","").equals("true")){
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = '" + match_class+"'",null);
            cursor=db.rawQuery("SELECT rowid _id, * FROM exam_list WHERE exam_date" + " = '"+match_date+"'"+" AND class = '" + match_class+ "'",null);
            cursor1=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = '" + match_class+"'",null);

        }
        else if(preference.getString("class","").equals("false")){
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = '" + match_class + "' AND batch_time" + " = '"+match_batch+"'",null);
            cursor=db.rawQuery("SELECT rowid _id, * FROM exam_list WHERE " + "class" + " = '" + match_class + "' AND batch" + " = '"+match_batch+"'"+ " AND exam_date" + " = '"+match_date+"'",null);
            cursor1=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
        }


        cursor_excel todoAdapter = new cursor_excel(this, cursor);


        lv.setAdapter(todoAdapter);



        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                TextView tvv = (TextView) view.findViewById(R.id.tv_nick_name_lv);
                TextView tvv1 = (TextView) view.findViewById(R.id.tv_full_name_lv);

                final TextView on_off = (TextView) view.findViewById(R.id.tv_on_off);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll2);

                final String date = tvv.getText().toString();
                final String name = tvv1.getText().toString();

                editor.putString("exam_date",date);
                editor.putString("ex_name",name);
                editor.commit();


                on_off.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(on_off.getText().toString().equals("On")){
                            on_off.setText("Off");
                            on_off.setBackgroundResource(R.drawable.border5);
                            on_off.setTextColor(Color.rgb(0,0,0));
                        }
                        else{
                            on_off.setText("On");
                            on_off.setBackgroundResource(R.drawable.border2);
                            on_off.setTextColor(Color.rgb(255,255,255));
                        }
                    }
                });
                l2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getApplicationContext(),choose_c_b_t2.class));

                    }
                });

                Toast.makeText(getApplicationContext(), date, Toast.LENGTH_SHORT).show();




            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(haveNetworkConnection()){
                    onScroll(lv);
                }
                else{
                    Toast.makeText(excel_exam_list.this, "No net connection", Toast.LENGTH_SHORT).show();
                }

            }
        });



    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    private void delete_from_sqlite(String ex_name){
        db_helper.delete_exam_list(ex_name);
    }
    private void delete_from_server(final String exam_name, String exam_date ){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        AppConfig.delete_exam_list api = adapter.create(AppConfig.delete_exam_list.class);

        api.delete_ex_list_f_server(
                exam_name,
                exam_date,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            delete_from_sqlite(exam_name);
                            Toast.makeText(getApplicationContext(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            pd.hide();
                            finish();
                            //Toast.makeText(display.this, result.toString(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),exam_list.class));

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.hide();
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    public void onScroll(AbsListView v)
    {

        pd.setMessage("Creating Excel...");
        pd.show();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        db_helper = new DatabaseHelper(this);
        final SQLiteDatabase db = db_helper.getReadableDatabase();

        db_helper.onUpgrade(db,1,2);

        final String match_class = preference.getString("s_class","").toString();
        final String match_batch = preference.getString("s_batch","").toString();

        //db_helper.create_result_table();

        //cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);

        cursor_list=db.rawQuery("SELECT rowid _id, * FROM exam_list",null);
        cursor_marks=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
        if(preference.getString("class","").equals("true")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class,null);
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class,null);


        }
        else if(preference.getString("class","").equals("false")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);

        }


        final Cursor finalCursor = cursor;
        final Cursor finalCursor1 = cursor;
        final Cursor finalCursor2 = cursor_marks_result;

        if(finalCursor1.moveToFirst()){
            do{
                students std_item = new students();

                std_item.setRegi(finalCursor.getString(finalCursor.getColumnIndexOrThrow("regi")));
                std_item.setFull_name(finalCursor.getString(finalCursor.getColumnIndexOrThrow("full_name")));
                std_item.setSchool(finalCursor.getString(finalCursor.getColumnIndexOrThrow("school")));
                std_item.setBatch_time(finalCursor.getString(finalCursor.getColumnIndexOrThrow("batch_time")));

                db_helper.insert_into_result(std_item);

            }while(finalCursor1.moveToNext());
        }


        final int childCount = lv.getChildCount();

        Cursor ex_result = null;

        for (int ii = 0; ii < childCount; ii++)
        {
            View v1 = lv.getChildAt(ii);
            TextView tx = (TextView) v1.findViewById(R.id.tv_on_off);
            final TextView tx_name = (TextView) v1.findViewById(R.id.tv_full_name_lv);
            final TextView tx_date = (TextView) v1.findViewById(R.id.tv_nick_name_lv);


            if(tx.getText().toString().equals("On")){
                db_helper.alter_result_table(tx_name.getText().toString());

                requestQueue1 = Volley.newRequestQueue(getApplicationContext());
                ex_result=db.rawQuery("SELECT rowid _id, * FROM result" ,null);
                final Cursor finalEx_result = ex_result;
                final int finalIi = ii;
                JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST,
                        showUrl2, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());
                        try {
                            JSONArray std = response.getJSONArray("students");
                            for (int i = 0; i < std.length(); i++) {
                                JSONObject student = std.getJSONObject(i);


                                if(finalEx_result.moveToFirst()){
                                    do{
                                        if(finalEx_result.getString(finalEx_result.getColumnIndexOrThrow("regi")).equals(student.getString("regi"))){
                                            //Toast.makeText(getApplicationContext(), student.getString(tx_date.getText().toString()), Toast.LENGTH_SHORT).show();
                                            db_helper.update_result_table(student.getString("regi"),
                                                    student.getString(tx_date.getText().toString()),
                                                    tx_name.getText().toString());
                                        }

                                    }while(finalEx_result.moveToNext());
                                }

                            }

                            if( finalIi == childCount-1) {
                                if (preference.getString("class", "").equals("true")) {
                                    SQLite2Excel(String.valueOf(finalIi)+"_"+tx_name.getText().toString() + "_" + preference.getString("s_class", ""));



                                } else if (preference.getString("class", "").equals("false")) {
                                    SQLite2Excel(String.valueOf(finalIi)+"_"+tx_name.getText().toString() + "_" + preference.getString("s_class", "")
                                            + "_" + preference.getString("s_batch", ""));



                                }

                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Please try again1", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.append(error.getMessage());

                        Toast.makeText(getApplicationContext(), "No internet cxonnection", Toast.LENGTH_SHORT).show();
                    }
                });
                requestQueue1.add(jsonObjectRequest2);


            }
            Toast.makeText(this, tx.getText().toString(), Toast.LENGTH_SHORT).show();
        }





    }

    private void SQLite2Excel(String name){
        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/GonitNiketan/Result";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        // Export SQLite DB as EXCEL FILE
        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), db_helper.DATABASE_NAME, directory_path);


        sqliteToExcel.exportSingleTable(db_helper.TABLE_NAME4, name+".xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }
            @Override
            public void onCompleted(String filePath) {
                Toast.makeText(getApplicationContext(), "File has been created", Toast.LENGTH_SHORT).show();

                pd.hide();
                finish();
            }
            @Override
            public void onError(Exception e) {

            }
        });
    }
}
