package com.roze.english.excel;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.roze.english.R;

/**
 * Created by emroz on 4/30/2017.
 */

public class cursor_excel extends CursorAdapter {

    String db_month;

    public cursor_excel(Context context, Cursor cursor) {
        super(context, cursor, 0);


    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.s_by_excel, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template

        TextView big_latter = (TextView) view.findViewById(R.id.tv_big_lv);
        TextView f_name = (TextView) view.findViewById(R.id.tv_full_name_lv);
        TextView n_name = (TextView) view.findViewById(R.id.tv_nick_name_lv);
        TextView on_off = (TextView) view.findViewById(R.id.tv_on_off);


        Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

        big_latter.setTypeface(type);
        f_name.setTypeface(type);
        n_name.setTypeface(type1);
        on_off.setTypeface(type1);


        // Extract properties from cursor
        String str_big_latter ;
        String str_f_name = cursor.getString(cursor.getColumnIndexOrThrow("exam_name"));
        String str_n_name = cursor.getString(cursor.getColumnIndexOrThrow("exam_date"));


        // String str_paid = cursor.getString(cursor.getColumnIndexOrThrow(db_month));

        // Populate fields with extracted properties

        f_name.setText(str_f_name);
        n_name.setText(str_n_name);

        //tvPriority.setText(String.valueOf(priority));
    }
}
