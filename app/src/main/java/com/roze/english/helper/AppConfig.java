package com.roze.english.helper;

/**
 * Created by emroz on 4/21/2017.
 */

import com.google.gson.JsonElement;
import com.roze.english.students1;
import com.roze.english.students1_examlist;
import com.roze.english.students1_marks;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import java.util.List;


public class AppConfig {

    public interface insert {
        @FormUrlEncoded
        @POST("/android/insertData.php")
        void insertData(
                @Field("regi") String regi,
                @Field("batch_time") String batch_time,
                @Field("s_nick") String s_nick,
                @Field("s_full") String s_full,
                @Field("father_name") String father_name,
                @Field("school") String school,
                @Field("fa_contact") String fa_contact,
                @Field("course") String course,

                Callback<Response> callback);
    }

    public interface read {
        @GET("/android/displayAll.php")
        void readData(Callback<JsonElement> callback);
    }

    public interface delete {
        @FormUrlEncoded
        @POST("/android/deleteData.php")
        void deleteData(
                @Field("regi") String regi,
                Callback<Response> callback);
    }
    public interface deleteAll {
        @FormUrlEncoded
        @POST("/android/delete_all.php")
        void deleteAllData(
                @Field("course") String course,
                Callback<Response> callback);
    }
    public interface delete_data {
        @FormUrlEncoded
        @POST("/android/deleteData.php")
        void delete_f_server(
                @Field("regi") int regi,
                Callback<Response> callback);
    }

    public interface delete_exam_list {
        @FormUrlEncoded
        @POST("/android/delete_exam_list.php")
        void delete_ex_list_f_server(
                @Field("exam_name") String exam_name,
                @Field("exam_date") String exam_date,
                Callback<Response> callback);
    }


    public interface add_exam {
        @FormUrlEncoded
        @POST("/android/alter_table.php")
        void add_exam_name(
                @Field("column") String exam_name,
                @Field("exam_date") String exam_date,
                @Field("class") String class_a,
                @Field("batch") String batch,
                Callback<Response> callback);

    }

    public interface update_marks {
        @FormUrlEncoded
        @POST("/android/update_exam_marks.php")
        void update_exam_marks(
                @Field("regi") String regi,
                @Field("marks") String marks,
                @Field("exam_name") String exam_name,
                Callback<Response> callback);
    }

    public interface update {
        @FormUrlEncoded
        @POST("/android/updateData.php")
        void updateData(
                @Field("id") String id,
                @Field("name") String name,
                @Field("age") String age,
                @Field("mobile") String mobile,
                @Field("email") String email,
                Callback<Response> callback);
    }

    public interface update_std_details {
        @FormUrlEncoded
        @POST("/android/update_std_details.php")
        void update_std(
                @Field("regi") String regi,
                @Field("pre_regi") String pre_regi,
                @Field("batch_time") String batch_time,
                @Field("s_nick") String s_nick,
                @Field("s_full") String s_full,
                @Field("father_name") String father_name,
                @Field("school") String school,
                @Field("fa_contact") String fa_contact,
                @Field("course") String course,
                Callback<Response> callback);
    }

    public interface update_payment {
        @FormUrlEncoded
        @POST("/android/update_payment.php")
        void update_pay(
                @Field("regi") String regi,
                @Field("jan") int jan,
                @Field("feb") int feb,
                @Field("mar") int mar,
                @Field("apr") int apr,
                @Field("may") int may,
                @Field("jun") int jun,
                @Field("jul") int jul,
                @Field("aug") int aug,
                @Field("sep") int sep,
                @Field("oct") int oct,
                @Field("nov") int nov,
                @Field("dec") int dec,
                Callback<Response> callback);
    }

    public interface get_students {

        @retrofit.http.GET("/json_std.php")
        public void getStudents(retrofit.Callback<List<students1>> response);
    }
    public interface get_students_marks {

        @retrofit.http.GET("/json_marks.php")
        public void getStudentsMarks(retrofit.Callback<List<students1_marks>> response);
    }
    public interface get_students_markslist {

        @retrofit.http.GET("/json_marklist.php")
        public void getStudentsMarksList(retrofit.Callback<List<students1_examlist>> response);
    }

}
