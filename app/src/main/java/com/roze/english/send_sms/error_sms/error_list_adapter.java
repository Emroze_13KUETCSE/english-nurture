package com.roze.english.send_sms.error_sms;

import android.content.Context;
import android.graphics.Typeface;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roze.english.R;

import java.util.List;

/**
 * Created by Emroze on 10-Feb-18.
 */

public class error_list_adapter extends ArrayAdapter<error_list_value> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<error_list_value> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public error_list_adapter(Context context, int resourceId,
                              List<error_list_value> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView regi;
        TextView f_name;
        TextView number;
        TextView bg;
    }

    public View getView(int position, View view, ViewGroup parent) {
        final com.roze.english.send_sms.error_sms.error_list_adapter.ViewHolder holder;
        if (view == null) {
            holder = new com.roze.english.send_sms.error_sms.error_list_adapter.ViewHolder();
            view = inflater.inflate(R.layout.sms_error_listview, null);
            // Locate the TextViews in listview_item.xml

            holder.regi = (TextView) view.findViewById(R.id.tv_regi);
            holder.f_name = (TextView) view.findViewById(R.id.tv_full_name_lv);
            holder.number = (TextView) view.findViewById(R.id.tv_number);
            holder.bg = (TextView) view.findViewById(R.id.tv_big_lv);



            Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");


            holder.regi.setTypeface(type1);
            holder.f_name.setTypeface(type);
            holder.number.setTypeface(type);



            // Locate the ImageView in listview_item.xml
            //holder.flag = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (com.roze.english.send_sms.error_sms.error_list_adapter.ViewHolder) view.getTag();
        }

        String str_big_latter ;
        String str_f_name = worldpopulationlist.get(position).getName();
        if(str_f_name.equals("")){
            str_big_latter="";
        }
        else {
            char c = str_f_name.charAt(0);
            str_big_latter = String.valueOf(c);
        }
        // Capture position and set to the TextViews
        holder.regi.setText(worldpopulationlist.get(position).getRegi());
        holder.f_name.setText(worldpopulationlist.get(position).getName());
        holder.number.setText(worldpopulationlist.get(position).getNum());

        holder.bg.setText(str_big_latter);


        // Capture position and set to the ImageView
        //holder.flag.setChecked(worldpopulationlist.get(position).getFlag());
        return view;
    }


    public List<error_list_value> getWorldPopulation() {
        return worldpopulationlist;
    }



    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
