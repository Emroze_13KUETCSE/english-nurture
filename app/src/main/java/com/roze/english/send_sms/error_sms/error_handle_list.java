package com.roze.english.send_sms.error_sms;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.send_sms.sms_display;

import java.util.ArrayList;
import java.util.List;

public class error_handle_list extends AppCompatActivity {
    public static String PREFS_NAME="math";

    ListView lv;
    DatabaseHelper db_helper = new DatabaseHelper(this);

    public String[] name;
    public String [] error_list_regi;
    public String [] error_list_num;
    public int len=0;


    int control = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_handle_list);

        lv = (ListView) findViewById(R.id.error_list);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final String match_class = preference.getString("s_class","").toString();
        String match_batch = preference.getString("s_batch","").toString();

        Intent intent = getIntent();
        len = intent.getIntExtra("error_list_no",0);


        name = new String[len];
        error_list_regi = new String[len];
        error_list_num = new String[len];

        error_list_regi = intent.getStringArrayExtra("error_list_regi");
        error_list_num = intent.getStringArrayExtra("error_list_num");
        name = intent.getStringArrayExtra("error_list_name");


    }

    @Override
    protected void onResume() {
        super.onResume();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if(control == 1){
            control = 0;
            String mm = preference.getString("update_mobile_no","");
            error_list_num[preference.getInt("scroll_indx",0)] = mm;
            editor.remove("update_mobile_no");
            editor.commit();
            //s.buffer[preference.getInt("scroll_indx",0)] = mm;
            //Toast.makeText(getApplicationContext(), s.buffer[preference.getInt("scroll_indx",0)], Toast.LENGTH_SHORT).show();
        }

        error_list_adapter listviewadapter;
        List<error_list_value> list_data = new ArrayList<error_list_value>();

        for (int j = 0; j < len; j++) {
            error_list_value worldpopulation = new error_list_value(name[j],error_list_regi[j],error_list_num[j]);
            list_data.add(worldpopulation);
        }

        listviewadapter = new error_list_adapter(getApplicationContext(), R.layout.sms_error_listview,
                list_data);

        // Binds the Adapter to the ListView
        lv.setAdapter((ListAdapter) listviewadapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //list.smoothScrollToPosition(indexx);
                lv.setSelection(preference.getInt("scroll_indx",0));
            }
        }, 10);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tvv = (TextView) view.findViewById(R.id.tv_regi);
                String str = tvv.getText().toString();

                editor.putString("temp_regi",str);
                editor.putInt("scroll_indx",i);
                editor.commit();

                Intent inn = new Intent(getApplicationContext(), edit_for_sms.class);
                startActivity(inn);
                control=1;
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent i = new Intent(getApplicationContext(),sms_display.class);
        startActivity(i);
    }
}
