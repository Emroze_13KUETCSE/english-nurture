package com.roze.english.send_sms.error_sms;

/**
 * Created by Emroze on 10-Feb-18.
 */

public class error_list_value {
    String name;
    String regi;
    String num;

    public error_list_value(String name, String regi, String num) {
        this.name = name;
        this.regi = regi;
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegi() {
        return regi;
    }

    public void setRegi(String regi) {
        this.regi = regi;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
