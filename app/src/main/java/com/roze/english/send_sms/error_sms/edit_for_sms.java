package com.roze.english.send_sms.error_sms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.helper.AppConfig;
import com.roze.english.students;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import pl.droidsonroids.gif.GifTextView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class edit_for_sms extends AppCompatActivity {
    public static String PREFS_NAME="math";

    String BASE_URL = "http://gonitniketan.com/";

    EditText et_r,et_nn,et_fn,et_fan,et_sc,et_m;
    TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8,tv0;
    TextView tv_class,tv_batch,tv_7,tv_8,tv_9,tv_10,tv_7b,tv_8b,tv_9b,tv_12b,tv_3b,tv_4b,tv_5b,tv_730b,tv_time,tv_sat,tv_sun;
    ExpandableLayout expandableLayout1,expandableLayout0,expandableLayout2;
    GifTextView g1,g2,g3;

    String search_class="" , search_batch="", sat_sun="", batch_time ="";

    Button submit,pre;

    LinearLayout ll;

    ProgressDialog pd;

    String regi_ID_temp="";

    DatabaseHelper db_helper = new DatabaseHelper(this);

/*    TextView tv9;
    EditText et_pay;*/


    ExpandableLayout ex100;
    Button pick;
    TimePicker t1;
    String hours , min,AM_PM_s;
    TextView tv_custom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_for_sms);

        pd = new ProgressDialog(edit_for_sms.this);

        et_r = (EditText) findViewById(R.id.et_i_roll);
        et_nn = (EditText) findViewById(R.id.et_i_nname);
        et_fn = (EditText) findViewById(R.id.et_i_fname);
        et_fan = (EditText) findViewById(R.id.et_i_faname);
        et_sc = (EditText) findViewById(R.id.et_i_school);
        et_m = (EditText) findViewById(R.id.et_i_mobile);
        //et_pay = (EditText) findViewById(R.id.et_i_payment);


        submit = (Button) findViewById(R.id.btn_submit);
        //pre = (Button) findViewById(R.id.preview);


        tv_class = (TextView) findViewById(R.id.tv_class);
        tv_batch = (TextView) findViewById(R.id.tv_batch);
        tv_7 = (TextView) findViewById(R.id.tv_7);
        tv_8 = (TextView) findViewById(R.id.tv_8);
        tv_9 = (TextView) findViewById(R.id.tv_9);
        tv_10 = (TextView) findViewById(R.id.tv_10);
        tv_7b = (TextView) findViewById(R.id.tv_7b);
        tv_8b = (TextView) findViewById(R.id.tv_8b);
        tv_9b = (TextView) findViewById(R.id.tv_9b);
        tv_12b = (TextView) findViewById(R.id.tv_12b);
        tv_3b = (TextView) findViewById(R.id.tv_3b);
        tv_4b = (TextView) findViewById(R.id.tv_4b);
        tv_5b = (TextView) findViewById(R.id.tv_5b);
        tv_730b = (TextView) findViewById(R.id.tv_730b);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_sat = (TextView) findViewById(R.id.tv_sat);
        tv_sun = (TextView) findViewById(R.id.tv_sun);
        //tv9 = (TextView) findViewById(R.id.tv9);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv0 = (TextView) findViewById(R.id.tv0);

        tv_custom = (TextView) findViewById(R.id.tv_custom);
        ex100  = (ExpandableLayout) findViewById(R.id.eeeeee);
        pick = (Button) findViewById(R.id.btn_pick_time);
        t1 = (TimePicker) findViewById(R.id.timePicker1);

        g1 = (GifTextView) findViewById(R.id.btn_gif1);
        g2 = (GifTextView) findViewById(R.id.btn_gif2);
        g3 = (GifTextView) findViewById(R.id.btn_gif3);



        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        tv1.setTypeface(type);
        tv2.setTypeface(type);
        tv3.setTypeface(type);
        tv4.setTypeface(type);
        tv5.setTypeface(type);
        tv6.setTypeface(type);
        tv7.setTypeface(type);
        tv8.setTypeface(type);
        tv0.setTypeface(type);
        //tv9.setTypeface(type);


        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv_batch.setTypeface(type1);
        tv_class.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_7.setTypeface(type1);
        tv_8.setTypeface(type1);
        tv_9.setTypeface(type1);
        tv_10.setTypeface(type1);
        tv_7b.setTypeface(type1);
        tv_8b.setTypeface(type1);
        tv_9b.setTypeface(type1);
        tv_12b.setTypeface(type1);
        tv_3b.setTypeface(type1);
        tv_4b.setTypeface(type1);
        tv_5b.setTypeface(type1);
        tv_730b.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_sat.setTypeface(type1);
        tv_sun.setTypeface(type1);
        et_r.setTypeface(type1);
        et_nn.setTypeface(type1);
        et_fn.setTypeface(type1);
        et_fan.setTypeface(type1);
        et_sc.setTypeface(type1);
        et_m.setTypeface(type1);
        submit.setTypeface(type1);
        //et_pay.setTypeface(type1);
        tv_custom.setTypeface(type1);

        //pre.setTypeface(type1);



        //for expand
        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);




        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        g2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        tv_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });


        ll = (LinearLayout) findViewById(R.id.ll);



        //select class
        tv_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "7";
                tv_class.setText("Class 7");
                expandableLayout0.collapse();
            }
        });
        tv_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "8";
                tv_class.setText("Class 8");
                expandableLayout0.collapse();
            }
        });
        tv_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "9";
                tv_class.setText("Class 9");
                expandableLayout0.collapse();
            }
        });
        tv_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "1";
                tv_class.setText("Class 10");
                expandableLayout0.collapse();
            }
        });



        //select batch
        tv_7b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0700AM";
                tv_batch.setText("7 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
                ex100.collapse();
            }
        });
        tv_8b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0800AM";
                tv_batch.setText("8 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);

                ex100.collapse();

            }
        });
        tv_9b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0900AM";
                tv_batch.setText("9 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
                ex100.collapse();
            }
        });
        tv_12b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "1200PM";
                tv_batch.setText("12 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
                ex100.collapse();
            }
        });
        tv_3b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0300PM";
                tv_batch.setText("3 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
                ex100.collapse();
            }
        });
        tv_4b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0400PM";
                tv_batch.setText("4 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
                ex100.collapse();
            }
        });
        tv_5b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0500PM";
                tv_batch.setText("5 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
                ex100.collapse();
            }
        });
        tv_730b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0730PM";
                tv_batch.setText("7 30 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
                ex100.collapse();
            }
        });


        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });
        g3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });


        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "SAT";
                tv_time.setText("SAT");
                expandableLayout2.collapse();
            }
        });

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "SUN";
                tv_time.setText("SUN");
                expandableLayout2.collapse();
            }
        });

        tv_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayout1.collapse();
                if (ex100.isExpanded()) {
                    ex100.collapse();
                }
                else {
                    ex100.expand();
                }

            }
        });

        pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex100.isExpanded()) {
                    ex100.collapse();
                }
                else {
                    ex100.expand();
                }
                ll.setVisibility(View.VISIBLE);

                tv_batch.setText(search_batch);

            }
        });

        t1.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                if(hourOfDay < 12) {
                    hours = String.valueOf(hourOfDay);
                    min = String.valueOf(minute);
                    AM_PM_s = "AM";
                    if(hours.length() == 1){
                        hours = "0"+hours;
                    }
                    if(min.length() == 1){
                        min = "0"+min;
                    }
                    search_batch = hours+min+AM_PM_s;

                }else {
                    hours = String.valueOf(hourOfDay-12);
                    min = String.valueOf(minute);

                    AM_PM_s = "PM";
                    if(hours.length() == 1){
                        hours = "0"+hours;
                    }
                    if(min.length() == 1){
                        min = "0"+min;
                    }
                    search_batch = hours+min+AM_PM_s;
                }

            }
        });


        set_text();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if(et_r.getText().toString().equals("") ||
                        et_nn.getText().toString().equals("") ||
                        et_fn.getText().toString().equals("") ||
                        et_fan.getText().toString().equals("") ||
                        et_m.getText().toString().equals("") ||
                        sat_sun.equals("") ||
                        search_class.equals("") ||
                        search_batch.equals("")
                        ){

                    Toast.makeText(getApplicationContext(), "Please fill the form totally", Toast.LENGTH_SHORT).show();

                }
                else{
                    pd.setMessage("Please wait...");
                    pd.show();

                    insert_data();

                }

            }
        });

    }

    private void set_text(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        regi_ID_temp = preference.getString("temp_regi","");

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor;

        cursor=db.rawQuery(" SELECT rowid _id, regi, full_name, nick_name, father_name,school, batch_time , mobile,course FROM students",null);

        //cursor=db.rawQuery("SELECT rowid _id, * FROM students",null);

        String[] column = null;

        String e_class="",e_batch="",e_time="",root="";
        if(cursor.moveToFirst()){

            do{

                if(regi_ID_temp.equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){
                    et_r.setText(cursor.getString(cursor.getColumnIndexOrThrow("regi")));
                    et_fn.setText(cursor.getString(cursor.getColumnIndexOrThrow("full_name")));
                    et_nn.setText(cursor.getString(cursor.getColumnIndexOrThrow("nick_name")));
                    et_fan.setText(cursor.getString(cursor.getColumnIndexOrThrow("father_name")));
                    root=cursor.getString(cursor.getColumnIndexOrThrow("batch_time"));
                    et_sc.setText(cursor.getString(cursor.getColumnIndexOrThrow("school")));
                    et_m.setText(cursor.getString(cursor.getColumnIndexOrThrow("mobile")));
                    tv_class.setText(cursor.getString(cursor.getColumnIndexOrThrow("course")));

                    search_class = cursor.getString(cursor.getColumnIndexOrThrow("course"));
                    if(!root.equals("")){
                        char[] c_batch=new char[6];
                        for(int i = 0; i < root.length();i++){
                            if(Character.isSpaceChar(root.charAt(i))){
                                break;
                            }
                            c_batch[i]=root.charAt(i);
                        }
                        e_batch=String.valueOf(c_batch);

                        search_batch = e_batch;
                        char[] c_time = new char[3];
                        c_time[0]=root.charAt(7);
                        c_time[1]=root.charAt(8);
                        c_time[2]=root.charAt(9);

                        e_time=String.valueOf(c_time);

                    }
                    else {
                        e_time = "No Time";
                        search_batch = "No Batch";
                    }

                    sat_sun = e_time;
                    tv_batch.setText(search_batch);
                    tv_time.setText(sat_sun);
                    batch_time = search_batch+" "+sat_sun;


                    Toast.makeText(this, batch_time, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            while(cursor.moveToNext());
        }
    }


    public void insert_in_sqlite(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        regi_ID_temp = preference.getString("temp_regi","");
        DatabaseHelper db_helper = new DatabaseHelper(this);

        students std_item = new students();

        batch_time = search_batch+" "+sat_sun;

        std_item.setRegi(et_r.getText().toString());
        std_item.setFull_name(et_fn.getText().toString());
        std_item.setNick_name(et_nn.getText().toString());
        std_item.setFa_name(et_fan.getText().toString());
        std_item.setSchool(et_sc.getText().toString());
        std_item.setBatch_time(batch_time);
        std_item.setMobile(et_m.getText().toString());
        std_item.setCourse(search_class);


        db_helper.update_std_details(std_item,regi_ID_temp);

        Toast.makeText(getApplicationContext(), "Successfully registered", Toast.LENGTH_SHORT).show();

        editor.putString("temp_regi",et_r.getText().toString());
        editor.putString("update_mobile_no",et_m.getText().toString());
        editor.commit();
        pd.hide();
        finish();
    }
    public void insert_data() {

        batch_time = search_batch+" "+sat_sun;
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.update_std_details api = adapter.create(AppConfig.update_std_details.class);

        api.update_std(
                et_r.getText().toString(),
                regi_ID_temp,
                batch_time,
                et_nn.getText().toString(),
                et_fn.getText().toString(),
                et_fan.getText().toString(),
                et_sc.getText().toString(),
                et_m.getText().toString(),
                search_class,
                //et_pay.getText().toString(),
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("success", "" + resp);

                            insert_in_sqlite();




                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();

                        } /*catch (JSONException e) {
                            Log.d("JsonException", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        }*/
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        pd.hide();
                        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
