package com.roze.english.send_sms;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roze.english.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifTextView;

public class choose_sms_send extends AppCompatActivity {
    public static String PREFS_NAME="math";

    GifTextView g1,g2,g3;

    TextView tv_5,tv_6,tv_len_sms,tv1,tv_class,tv_batch,tv_7,tv_8,tv_9,tv_10,tv_7b,tv_8b,tv_9b,tv_12b,tv_3b,tv_4b,tv_5b,tv_730b,tv_time,tv_sat,tv_sun;

    Button btn_search;

    EditText et_sms;

    ExpandableLayout expandableLayout1,expandableLayout0,expandableLayout2;

    String search_class = "" , search_batch="", sat_sun="", batch_time ="";

    LinearLayout ll,ll_d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_sms_send);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        g1 = (GifTextView) findViewById(R.id.btn_gif1);
        g2 = (GifTextView) findViewById(R.id.btn_gif2);
        g3 = (GifTextView) findViewById(R.id.btn_gif3);


        tv_class = (TextView) findViewById(R.id.tv_class);
        tv_batch = (TextView) findViewById(R.id.tv_batch);
        tv_5 = (TextView) findViewById(R.id.tv_5);
        tv_6 = (TextView) findViewById(R.id.tv_6);
        tv_7 = (TextView) findViewById(R.id.tv_7);
        tv_8 = (TextView) findViewById(R.id.tv_8);
        tv_9 = (TextView) findViewById(R.id.tv_9);
        tv_10 = (TextView) findViewById(R.id.tv_10);
        tv_7b = (TextView) findViewById(R.id.tv_7b);
        tv_8b = (TextView) findViewById(R.id.tv_8b);
        tv_9b = (TextView) findViewById(R.id.tv_9b);
        tv_12b = (TextView) findViewById(R.id.tv_12b);
        tv_3b = (TextView) findViewById(R.id.tv_3b);
        tv_4b = (TextView) findViewById(R.id.tv_4b);
        tv_5b = (TextView) findViewById(R.id.tv_5b);
        tv_730b = (TextView) findViewById(R.id.tv_730b);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_sat = (TextView) findViewById(R.id.tv_sat);
        tv_sun = (TextView) findViewById(R.id.tv_sun);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv_len_sms = (TextView) findViewById(R.id.tv_num_of_sms);

        et_sms =(EditText) findViewById(R.id.et_sms);

        btn_search = (Button) findViewById(R.id.btn_search_by_class);


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        tv1.setTypeface(type);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv_batch.setTypeface(type1);
        tv_class.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_5.setTypeface(type1);
        tv_6.setTypeface(type1);
        tv_7.setTypeface(type1);
        tv_8.setTypeface(type1);
        tv_9.setTypeface(type1);
        tv_10.setTypeface(type1);
        tv_7b.setTypeface(type1);
        tv_8b.setTypeface(type1);
        tv_9b.setTypeface(type1);
        tv_12b.setTypeface(type1);
        tv_3b.setTypeface(type1);
        tv_4b.setTypeface(type1);
        tv_5b.setTypeface(type1);
        tv_730b.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_sat.setTypeface(type1);
        tv_sun.setTypeface(type1);
        et_sms.setTypeface(type1);

        btn_search.setTypeface(type1);

        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);

        ll = (LinearLayout) findViewById(R.id.ll);

        ll.setVisibility(View.INVISIBLE);

        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        g2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        tv_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        //select class
        tv_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "7";
                tv_class.setText("Class 7");
                expandableLayout0.collapse();
            }
        });
        tv_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "8";
                tv_class.setText("Class 8");
                expandableLayout0.collapse();
            }
        });
        tv_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "9";
                tv_class.setText("Class 9");
                expandableLayout0.collapse();
            }
        });
        tv_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "1";
                tv_class.setText("Class 10");
                expandableLayout0.collapse();
            }
        });
        tv_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "5";
                tv_class.setText("Class 5");
                expandableLayout0.collapse();
            }
        });
        tv_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "6";
                tv_class.setText("Class 6");
                expandableLayout0.collapse();
            }
        });



        //select batch
        tv_7b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0700AM";
                tv_batch.setText("7 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_8b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0800AM";
                tv_batch.setText("8 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_9b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0900AM";
                tv_batch.setText("9 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_12b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "1200PM";
                tv_batch.setText("12 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_3b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0300PM";
                tv_batch.setText("3 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_4b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0400PM";
                tv_batch.setText("4 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_5b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0500PM";
                tv_batch.setText("5 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_730b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0730PM";
                tv_batch.setText("7 30 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });


        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });


        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "SAT";
                tv_time.setText("SAT");
                expandableLayout2.collapse();
            }
        });

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "SUN";
                tv_time.setText("SUN");
                expandableLayout2.collapse();
            }
        });


        et_sms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String sms = String.valueOf(charSequence);
                int ll = sms.length();

                SmsManager sm = SmsManager.getDefault();
                ArrayList<String> parts =sm.divideMessage(sms+"\nEnglish Nurture, Abul Kalam Azad.");
                int numParts = parts.size();
                tv_len_sms.setText(String.valueOf(ll)+" ("+String.valueOf(numParts)+")" );
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                batch_time = search_batch+" "+sat_sun;

                if(!search_batch.equals("") && !sat_sun.equals("")){
                    editor.putString("s_class",search_class);
                    editor.putString("s_batch",batch_time);
                    editor.putString("sms",et_sms.getText().toString()+"\nEnglish Nurture, Abul Kalam Azad.");
                    editor.putString("class","false");
                    editor.commit();


                    Intent i = new Intent(getApplicationContext(),sms_display.class);
                    startActivity(i);
                }
                else if(!search_class.equals("")) {

                    editor.putString("s_class",search_class);
                    editor.putString("sms",et_sms.getText().toString()+"\nEnglish Nurture, Abul Kalam Azad.");
                    editor.putString("class","true");
                    editor.commit();

                    Intent i = new Intent(getApplicationContext(),sms_display.class);
                    startActivity(i);



                }



            }
        });
    }
}
