package com.roze.english.send_sms;


import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.*;
import android.widget.*;


import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.send_sms.error_sms.error_handle_list;
import com.roze.english.service.MyService;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class sms_display extends AppCompatActivity {
    public static String PREFS_NAME="math";

    String BASE_URL = "http://gonitniketan.com/";

    ProgressDialog pd;

    TextView tv1,tv2;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    String str="";

    Button btn_send;

    String[] regi_in,regi_out;

    String[] buffer ;
    int[] regi_buffer;
    int[] regi_buffer_not_send;


    int array_size = 0;

    int in,out,match_in,match_out=0;

    int count_roll = 0;
    int count=0,len;
    int check = 0;

    int ii = -1;

    TextView roll_list;
    Boolean buffer_roll_check = false;
    int not_zero=0;
    int g = 0;





    CheckBox cb;

    int error_no=0;
    String [] error_list_regi;
    String [] error_list_num;
    String [] error_list_name;
    String [] list_num;


    int control = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_display);

        tv1 = (TextView) findViewById(R.id.tv_class_name);
        tv2 = (TextView) findViewById(R.id.tv_batch_time);

        roll_list  = (TextView) findViewById(R.id.tv_roll_list);

        cb = (CheckBox) findViewById(R.id.checkBox);

        lv = (ListView) findViewById(R.id.sms_lv);
        pd = new ProgressDialog(sms_display.this);

        btn_send = (Button) findViewById(R.id.btn_send);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        tv1.setTypeface(type1);
        tv2.setTypeface(type1);
        roll_list.setTypeface(type);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();




        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor = null;

        final String match_class = preference.getString("s_class","").toString();
        String match_batch = preference.getString("s_batch","").toString();

        System.out.println(match_class);

        if(preference.getString("class","").equals("true")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class,null);
            tv1.setText("Class "+match_class);
            tv2.setText("No batch selected");

        }
        else if(preference.getString("class","").equals("false")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            tv1.setText("Class "+match_class);
            tv2.setText(match_batch);
        }
        //lv.setDivider(null);
        //lv.setDividerHeight(0);


        cursor_sms todoAdapter = new cursor_sms(this, cursor);

        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lv.setAdapter(todoAdapter);

        regi_buffer = new int[cursor.getCount()];




        array_size = cursor.getCount();
        regi_in = new String[array_size];

        count = 1;


        //Toast.makeText(this, "Total "+String.valueOf(cursor.getCount()), Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, "Total1 "+String.valueOf(buffer.length), Toast.LENGTH_SHORT).show();


        final TextView[] tv_sss = new TextView[cursor.getCount()];

        /*lv.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                final int checkedCount = lv.getCheckedItemCount();
                // Set the CAB title according to total checked items
                mode.setTitle(checkedCount + " Selected");
                Toast.makeText(sms_display.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
            }
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                Toast.makeText(sms_display.this, String.valueOf(item.getItemId()), Toast.LENGTH_SHORT).show();
                return true;
            }
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }



            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });*/

        final int[] ary = new int[lv.getCount()];
        for(int i = 0; i < lv.getCount(); i++){
            ary[i] = -1;
        }
        final int[] Count = {0};
        final String[] aaa = {""};



        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

                LinearLayout l_d = (LinearLayout) view.findViewById(R.id.ll_delete);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll11);
                LinearLayout l3 = (LinearLayout) view.findViewById(R.id.ll22);
                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                //tv_sss[i] = (TextView) view.findViewById(R.id.tv_send);


                String rrr = tvv.getText().toString();

                if(ary[i] == -1){
                    ary[i] = i;
                }
                else {
                    ary[i] = -1;
                }


                int r = Integer.valueOf(rrr);


                for(int a = 0; a < regi_buffer.length; a++){

                    if(regi_buffer[a] == r){
                        regi_buffer[a] = 0;
                        buffer_roll_check = true;
                    }
                }

                if(buffer_roll_check == false){
                    regi_buffer[i] = r;
                    count_roll++;
                }
                String r_list = "";
                int zero=0;
                for(int c = 0; c < regi_buffer.length; c++){
                    if(regi_buffer[c] != 0){
                        zero++;
                    }
                }
                if(zero == regi_buffer.length){
                    roll_list.setText("");
                }

                roll_list.setText(String.valueOf(zero));
                buffer_roll_check = false;

                // Toast.makeText(getApplicationContext(), String.valueOf(i)+" "+tvv.getText().toString()+" "+tvv.getText().toString(), Toast.LENGTH_SHORT).show();


                editor.putString("temp_regiID",str);
                editor.commit();
            }
        });
        final TextView[] number = new TextView[array_size];
        final TextView[] send_tv = new TextView[array_size];

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Toast.makeText(MainActivity.this, String.valueOf(list.getFirstVisiblePosition()), Toast.LENGTH_SHORT).show();
                                //Toast.makeText(MainActivity.this, aaa[0], Toast.LENGTH_SHORT).show();
                                for(int a = lv.getFirstVisiblePosition(); a < lv.getFirstVisiblePosition()+lv.getChildCount(); a++ ){

                                    if(ary[a] == -1){
                                        //lv.getChildAt(a-lv.getFirstVisiblePosition()).setBackgroundColor(Color.WHITE);
                                        View vv = lv.getChildAt(a-lv.getFirstVisiblePosition());
                                        TextView tv = (TextView) vv.findViewById(R.id.tv_on_off);
                                        tv.setTextColor(Color.BLACK);
                                        tv.setBackgroundResource(R.drawable.border_off);
                                        tv.setText("Off");
                                    }
                                    else if(ary[a] == a){
                                        //lv.getChildAt(a-lv.getFirstVisiblePosition()).setBackgroundColor(Color.rgb(85,238,163));
                                        View vv = lv.getChildAt(a-lv.getFirstVisiblePosition());
                                        TextView tv = (TextView) vv.findViewById(R.id.tv_on_off);
                                        tv.setTextColor(Color.WHITE);
                                        tv.setBackgroundResource(R.drawable.border2);
                                        tv.setText("On");
                                    }

                                }

                                //Toast.makeText(MainActivity.this, String.valueOf(list.getFirstVisiblePosition()+list.getChildCount()), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

        final Cursor finalCursor1 = cursor;

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    int bb=0;
                    if(finalCursor1.moveToFirst()){
                        do{

                            if(match_class.equals(finalCursor1.getString(finalCursor1.getColumnIndexOrThrow("course")))){
                                int re = Integer.valueOf(finalCursor1.getString(finalCursor1.getColumnIndexOrThrow("regi")));
                                regi_buffer[bb] = re;
                                ary[bb]=bb;
                                bb++;
                            }
                        }
                        while(finalCursor1.moveToNext());
                    }
                    roll_list.setText(String.valueOf(array_size));
                }
                else {
                    for(int i = 0; i < array_size;i++){
                        ary[i] = -1;
                        regi_buffer[i] = 0;
                    }
                    roll_list.setText(String.valueOf(0));

                }
            }
        });

        /*Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(ii != -1){
                                    View v1 = lv.getChildAt(ii);
                                    final TextView send = (TextView) v1.findViewById(R.id.tv_send);
                                    if(send.getText().toString().equals("ON")){
                                        send.setText("OFF");
                                        send.setBackgroundResource(R.drawable.border5);
                                        send.setTextColor(Color.rgb(0,0,0));
                                    }
                                    else{
                                        send.setText("ON");
                                        send.setBackgroundResource(R.drawable.border2);
                                        send.setTextColor(Color.rgb(255,255,255));
                                    }
                                    ii=-1;
                                }
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();*/


        final Cursor finalCursor = cursor;
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for(int c = 0; c < regi_buffer.length; c++){
                    if(regi_buffer[c] != 0){
                        not_zero++;
                    }
                }

                regi_buffer_not_send = new int[not_zero];

                //len = finalCursor.getCount() - not_zero;
                //buffer = new String[finalCursor.getCount() - not_zero];

                len = not_zero;

                Toast.makeText(sms_display.this, String.valueOf(len), Toast.LENGTH_SHORT).show();
                buffer = new String[not_zero];
                list_num = new String[not_zero];
                int e =0;
                for(int c = 0; c < regi_buffer.length; c++){
                    if(regi_buffer[c] != 0){
                        regi_buffer_not_send[e] = regi_buffer[c];
                        e++;
                    }
                }

                int buf = 0;
                error_no=0;
                if(finalCursor.moveToFirst()){

                    do{

                        if(match_class.equals(finalCursor.getString(finalCursor.getColumnIndexOrThrow("course")))){
                            int re = Integer.valueOf(finalCursor.getString(finalCursor.getColumnIndexOrThrow("regi")));
                            int f = 0;
                            for (int d = 0 ; d < regi_buffer_not_send.length; d++){
                                if(re == regi_buffer_not_send[d]){
                                    String numm = finalCursor.getString(finalCursor.getColumnIndexOrThrow("mobile"));
                                    String fname = finalCursor.getString(finalCursor.getColumnIndexOrThrow("full_name"));
                                    if(!numm.equals("")
                                            ||numm != null
                                            ||numm.charAt(0) == '1'
                                            || numm.charAt(0) == '0'
                                            || numm.contains("+880")
                                            ){
                                        if(numm.charAt(0) == '1'){
                                            String sss_n = "0";
                                            sss_n = sss_n.concat(numm);
                                            buffer[buf] = sss_n;

                                        }
                                        else if(numm.charAt(0) == '0'){
                                            buffer[buf] = numm;

                                        }

                                        //Toast.makeText(sms_display.this,  String.valueOf(buffer[buf].length()), Toast.LENGTH_SHORT).show();
                                        /*if(!String.valueOf(buffer[buf].length()).equals("11") ){
                                            error_no++;
                                        }*/
                                    }else {
                                        buffer[buf] = "0";
                                        //error_no++;
                                    }
                                    list_num[buf]=fname;
                                    buf++;
                                    break;
                                }
                                /*else {
                                    f++;
                                }*/
                            }
                        }
                    }
                    while(finalCursor.moveToNext());
                }

                for(int i =0; i < not_zero;i++){

                    try{
                        if(buffer[i].length() != 11){
                            error_no++;
                        }
                    }catch (NullPointerException en){
                        error_no++;
                        Toast.makeText(sms_display.this, String.valueOf(i), Toast.LENGTH_SHORT).show();
                    }
                    /*
                    */
                }
                error_list_num = new String[error_no];
                error_list_regi = new String[error_no];
                error_list_name = new String[error_no];


                int j =0;
                for(int i = 0; i < not_zero;i++){

                    try{

                        if(buffer[i].length() != 11 ){
                            error_list_regi[j] = String.valueOf(regi_buffer_not_send[i]);
                            error_list_num[j] = buffer[i];
                            error_list_name[j] = list_num[i];
                            j++;
                        }
                    }catch (NullPointerException ee){
                        error_list_regi[j] = String.valueOf(regi_buffer_not_send[i]);
                        error_list_num[j] = "0";
                        error_list_name[j] = list_num[i];
                        j++;
                    }
                }

                new SweetAlertDialog(sms_display.this, SweetAlertDialog.WARNING_TYPE)

                        .setTitleText("Send SMS?")
                        .setCancelText("Error("+error_no+")")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent intent=new Intent(sms_display.this,error_handle_list.class);
                                intent.putExtra("error_list_no",error_no);
                                intent.putExtra("error_list_regi",error_list_regi);
                                intent.putExtra("error_list_num",error_list_num);
                                intent.putExtra("error_list_name",error_list_name);
                                startActivity(intent);
                                control = 1;
                                finish();
                            }
                        })
                        .setContentText(not_zero+" SMS will be sent")
                        .setConfirmText("Yes,send it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();



                                len = buffer.length;
                                Intent intent=new Intent(sms_display.this,MyService.class);
                                Bundle b=new Bundle();
                                b.putStringArray("number_array",buffer);
                                b.putIntArray("regi_array",regi_buffer_not_send);
                                intent.putExtras(b);
                                startService(intent);

                                Thread t = new Thread() {

                                    @Override
                                    public void run() {
                                        try {
                                            while (!isInterrupted()) {
                                                Thread.sleep(50);
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {

                                                        count = preference.getInt("sms_count",0);
                                                        //Toast.makeText(sms_display.this, String.valueOf(len), Toast.LENGTH_SHORT).show();
                                                        if(count <= len){


                                                            pd.setCanceledOnTouchOutside(false);
                                                            pd.setMessage("Sending SMS..."+String.valueOf(count+1)+"/"+
                                                                    String.valueOf(len)+"\nRegi: "+regi_buffer_not_send[count]);
                                                            pd.show();


                                                        }


                                                    }
                                                });

                                            }
                                        } catch (InterruptedException e) {
                                        }
                                    }
                                };
                                t.start();



                                // Toast.makeText(MainActivity.this, "After "+preference.getString("net",""), Toast.LENGTH_SHORT).show();



                            }
                        })
                        .show();



                not_zero=0;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

   /* // for sms delivery report
    BroadcastReceiver sendBroadcastReceiver = new sms_display.SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new sms_display.DeliverReceiver();

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        SmsManager sm = SmsManager.getDefault();
        ArrayList<String> parts =sm.divideMessage(message);
        int numParts = parts.size();

        if(numParts == 1){
            PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                    SENT), 0);

            PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                    new Intent(DELIVERED), 0);

            registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

            registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
            Toast.makeText(this, String.valueOf(numParts), Toast.LENGTH_SHORT).show();

        }
        else {
            ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
            ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

            registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

            registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));

            for (int i = 0; i < numParts; i++) {
                sentIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(SENT), 0));
                deliveryIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(DELIVERED), 0));
            }

            sm.sendMultipartTextMessage(phoneNumber,null, parts, sentIntents, deliveryIntents);

        }
    }

    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Toast.makeText(getBaseContext(), "SMS delivered",
                        //    Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    //Toast.makeText(getBaseContext(), "sms not delivered",
                        //    Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {



            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(context, "OK "+String.valueOf(count), Toast.LENGTH_SHORT).show();
                    count++;
                    g++;
                    check=0;


                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(context, "Generic", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(context, "No service", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(context, "NULL PDU", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(context, "Radio OFF", Toast.LENGTH_SHORT).show();

                    break;
            }

        }
    }

    public static int[] removeElement(int[] original, int element){
        int[] n = new int[original.length - 1];
        System.arraycopy(original, 0, n, 0, element );
        System.arraycopy(original, element+1, n, element, original.length - element-1);
        return n;
    }*/

}
