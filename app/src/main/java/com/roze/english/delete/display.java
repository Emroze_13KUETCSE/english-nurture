package com.roze.english.delete;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.Std_details;
import com.roze.english.helper.AppConfig;
import com.roze.english.students;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class display extends AppCompatActivity {
    public static String PREFS_NAME="math";

    String BASE_URL = "http://englishnurturebd.com/";

    ProgressDialog pd;

    TextView tv1,tv2,tv_delete_all;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    String str="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);


        tv1 = (TextView) findViewById(R.id.tv_class_name);
        tv2 = (TextView) findViewById(R.id.tv_batch_time);
        tv_delete_all = (TextView) findViewById(R.id.tv_delete_all);


        lv = (ListView) findViewById(R.id.s_by_class_batch_lv);
        pd = new ProgressDialog(display.this);

        tv_delete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Deleting All...");
                pd.show();
                delete_all();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor todoCursor = db.rawQuery("SELECT rowid _id, full_name, batch_time, course, school, nick_name FROM students", null);

        Cursor cursor = null;

        String match_class = preference.getString("s_class","").toString();
        String match_batch = preference.getString("s_batch","").toString();


        if(preference.getString("class","").equals("true")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class,null);
            tv1.setText("Class "+match_class);
            tv2.setVisibility(View.INVISIBLE);

        }
        else if(preference.getString("class","").equals("false")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            tv1.setText("Class "+match_class);
            tv2.setText(match_batch);
        }

        lv.setDivider(null);
        lv.setDividerHeight(0);


        cursor_adapter todoAdapter = new cursor_adapter(this, cursor);

        lv.setAdapter(todoAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                LinearLayout l_d = (LinearLayout) view.findViewById(R.id.ll_delete);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll2);
                LinearLayout l3 = (LinearLayout) view.findViewById(R.id.ll3);
                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                TextView delete = (TextView) view.findViewById(R.id.tv_delete);

                str = tvv.getText().toString();

                l_d.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(display.this, str, Toast.LENGTH_SHORT).show();
                    }
                });

                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        new AlertDialog.Builder(display.this)
                                .setTitle("Delete entry")
                                .setMessage("Are you sure you want to delete this entry?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        Toast.makeText(display.this, str, Toast.LENGTH_SHORT).show();

                                        pd.setMessage("Deleting...");
                                        pd.show();

                                        //delete(str);
                                        delete_from_server();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(R.drawable.alert)
                                .show();
                    }
                });
                l2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent inn = new Intent(getApplicationContext(), Std_details.class);
                        startActivity(inn);


                    }
                });
                l3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent inn = new Intent(getApplicationContext(), Std_details.class);
                        startActivity(inn);


                    }
                });


                editor.putString("temp_regiID",str);
                editor.commit();



            }
        });
    }

    private void delete_all(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        AppConfig.deleteAll api = adapter.create(AppConfig.deleteAll.class);

        api.deleteAllData(
                preference.getString("s_class",""),
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            db_helper.delete_All(preference.getString("s_class",""));
                            Toast.makeText(display.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            pd.hide();
                            finish();
                            //Toast.makeText(display.this, result.toString(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),display.class));


                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            pd.hide();
                            Toast.makeText(display.this, "Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.hide();
                        Toast.makeText(display.this, error.toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(display.this, "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
    private void delete_from_sqlite(){
        students std_item = new students();

        std_item.setRegi(str);

        db_helper.delete_std(std_item);
    }
    private void delete_from_server(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        AppConfig.delete_data api = adapter.create(AppConfig.delete_data.class);
        int regi_id = Integer.valueOf(str);

        api.delete_f_server(
                regi_id,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            delete_from_sqlite();
                            Toast.makeText(display.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            pd.hide();
                            finish();
                            //Toast.makeText(display.this, result.toString(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),display.class));


                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            pd.hide();
                            Toast.makeText(display.this, "Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.hide();
                        Toast.makeText(display.this, error.toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(display.this, "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }


}
