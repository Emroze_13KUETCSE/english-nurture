package com.roze.english.delete;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.roze.english.R;

/**
 * Created by emroz on 4/22/2017.
 */

public class cursor_adapter extends CursorAdapter {

    String db_month;

    public cursor_adapter(Context context, Cursor cursor) {
        super(context, cursor, 0);


    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.s_by_delete_list, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template

        TextView big_latter = (TextView) view.findViewById(R.id.tv_big_lv);
        TextView f_name = (TextView) view.findViewById(R.id.tv_full_name_lv);
        TextView n_name = (TextView) view.findViewById(R.id.tv_nick_name_lv);
        TextView regi_ID = (TextView) view.findViewById(R.id.tv_regi_lv);
        TextView school = (TextView) view.findViewById(R.id.tv_school_lv);
        TextView delete = (TextView) view.findViewById(R.id.tv_delete);
        TextView bt = (TextView) view.findViewById(R.id.tv_bt);

        Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

        big_latter.setTypeface(type);
        f_name.setTypeface(type);
        n_name.setTypeface(type1);
        regi_ID.setTypeface(type1);
        school.setTypeface(type1);
        delete.setTypeface(type1);
        bt.setTypeface(type);

        // Extract properties from cursor
        String str_big_latter = "";
        String str_f_name = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
        String str_n_name = cursor.getString(cursor.getColumnIndexOrThrow("nick_name"));
        String str_regi = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
        String str_school = cursor.getString(cursor.getColumnIndexOrThrow("school"));
        String str_bt = cursor.getString(cursor.getColumnIndexOrThrow("batch_time"));

        // String str_paid = cursor.getString(cursor.getColumnIndexOrThrow(db_month));
        try{
            if(str_f_name.equals("")){
                str_big_latter="";
            }
            else {
                char c = str_f_name.charAt(0);
                str_big_latter = String.valueOf(c);
            } 
        }
        catch (NullPointerException e){
            
        }


        // Populate fields with extracted properties
        big_latter.setText(str_big_latter);
        f_name.setText(str_f_name);
        n_name.setText(str_n_name);
        regi_ID.setText(str_regi);
        school.setText(str_school);
        bt.setText(str_bt);




        //tvPriority.setText(String.valueOf(priority));
    }
}
