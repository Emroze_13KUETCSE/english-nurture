package com.roze.english;

/**
 * Created by emroze on 6/18/17.
 */

public class students1_examlist {
    public String exam_name;
    public String exam_date;
    public String exam_class;
    public String exam_batch;

    public students1_examlist() {
    }

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }

    public String getExam_date() {
        return exam_date;
    }

    public void setExam_date(String exam_date) {
        this.exam_date = exam_date;
    }

    public String getExam_class() {
        return exam_class;
    }

    public void setExam_class(String exam_class) {
        this.exam_class = exam_class;
    }

    public String getExam_batch() {
        return exam_batch;
    }

    public void setExam_batch(String exam_batch) {
        this.exam_batch = exam_batch;
    }
}
