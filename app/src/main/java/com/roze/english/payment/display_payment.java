package com.roze.english.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.Std_details;

public class display_payment extends AppCompatActivity {

    public static String PREFS_NAME="math";

    TextView tv1,tv2,tv3;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_payment);


        tv1 = (TextView) findViewById(R.id.tv_class_name);
        tv2 = (TextView) findViewById(R.id.tv_batch_time);
        tv3 = (TextView) findViewById(R.id.tv_month);


        lv = (ListView) findViewById(R.id.s_by_class_batch_lv);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv3.setTypeface(type1);
        tv2.setTypeface(type1);
        tv1.setTypeface(type1);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        editor.putInt("scroll_pos_payment",0);
        editor.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor todoCursor = db.rawQuery("SELECT rowid _id, full_name, batch_time, course, school, nick_name FROM students", null);

        Cursor cursor = null;

        String match_class = preference.getString("s_class","").toString();
        String match_batch = preference.getString("s_batch","").toString();


        if(preference.getString("class","").equals("true")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class,null);
            tv1.setText("Class "+match_class);
            tv2.setVisibility(View.INVISIBLE);

        }
        else if(preference.getString("class","").equals("false")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            tv1.setText("Class "+match_class);
            tv2.setText(match_batch);
        }
        
       
        tv3.setText(preference.getString("month",""));

        
        /*cursor=db.rawQuery("SELECT * FROM students WHERE "
                + "course" + " = " + match_class + " AND " + "batch_time" +
                " LIKE  '"+search.getText()+"%'");*/
// Setup cursor adapter using cursor from last step
        cursor_payment todoAdapter = new cursor_payment(this, cursor);
// Attach cursor adapter to the ListView
        lv.setAdapter(todoAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //list.smoothScrollToPosition(indexx);
                lv.setSelection(preference.getInt("scroll_pos_payment",0));
            }
        }, 10);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                String str = tvv.getText().toString();

                editor.putString("temp_regiID",str);
                editor.commit();

                Intent inn = new Intent(getApplicationContext(), Std_details.class);
                startActivity(inn);

                editor.putInt("scroll_pos_payment",i);
                editor.commit();

                Toast.makeText(getApplicationContext(), str,
                        Toast.LENGTH_SHORT).show();

            }
        });
    }
}
