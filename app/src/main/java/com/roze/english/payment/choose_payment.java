package com.roze.english.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roze.english.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import pl.droidsonroids.gif.GifTextView;

public class choose_payment extends AppCompatActivity {
    public static String PREFS_NAME="math";

    GifTextView g1,g2,g3,g4;

    TextView tv_class,tv_batch,tv_7,tv_8,tv_9,tv_10,tv_7b,tv_8b,tv_9b,tv_12b,tv_3b,tv_4b,tv_5b,tv_730b,tv_time,tv_sat,tv_sun;

    TextView jan,feb,mar,apr,may,june,july,aug,sep,oct,nov,dec,tv_mp;

    Button btn_search;

    ExpandableLayout expandableLayout1,expandableLayout0,expandableLayout2,ex4;

    String search_class , search_batch, sat_sun, batch_time,month ="";

    LinearLayout ll,ll_d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_payment);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        g1 = (GifTextView) findViewById(R.id.btn_gif1);
        g2 = (GifTextView) findViewById(R.id.btn_gif2);
        g3 = (GifTextView) findViewById(R.id.btn_gif3);
        g4 = (GifTextView) findViewById(R.id.btn_gif4);


        tv_class = (TextView) findViewById(R.id.tv_class);
        tv_batch = (TextView) findViewById(R.id.tv_batch);
        tv_7 = (TextView) findViewById(R.id.tv_7);
        tv_8 = (TextView) findViewById(R.id.tv_8);
        tv_9 = (TextView) findViewById(R.id.tv_9);
        tv_10 = (TextView) findViewById(R.id.tv_10);
        tv_7b = (TextView) findViewById(R.id.tv_7b);
        tv_8b = (TextView) findViewById(R.id.tv_8b);
        tv_9b = (TextView) findViewById(R.id.tv_9b);
        tv_12b = (TextView) findViewById(R.id.tv_12b);
        tv_3b = (TextView) findViewById(R.id.tv_3b);
        tv_4b = (TextView) findViewById(R.id.tv_4b);
        tv_5b = (TextView) findViewById(R.id.tv_5b);
        tv_730b = (TextView) findViewById(R.id.tv_730b);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_sat = (TextView) findViewById(R.id.tv_sat);
        tv_sun = (TextView) findViewById(R.id.tv_sun);

        jan = (TextView) findViewById(R.id.jan);
        feb = (TextView) findViewById(R.id.feb);
        mar = (TextView) findViewById(R.id.mar);
        apr = (TextView) findViewById(R.id.apr);
        may = (TextView) findViewById(R.id.may);
        june = (TextView) findViewById(R.id.jun);
        july = (TextView) findViewById(R.id.jul);
        aug = (TextView) findViewById(R.id.aug);
        sep = (TextView) findViewById(R.id.sep);
        oct = (TextView) findViewById(R.id.oct);
        nov = (TextView) findViewById(R.id.nov);
        dec = (TextView) findViewById(R.id.dec);
        tv_mp = (TextView) findViewById(R.id.tv_mp);


        btn_search = (Button) findViewById(R.id.btn_search_by_class);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv_batch.setTypeface(type1);
        tv_class.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_7.setTypeface(type1);
        tv_8.setTypeface(type1);
        tv_9.setTypeface(type1);
        tv_10.setTypeface(type1);
        tv_7b.setTypeface(type1);
        tv_8b.setTypeface(type1);
        tv_9b.setTypeface(type1);
        tv_12b.setTypeface(type1);
        tv_3b.setTypeface(type1);
        tv_4b.setTypeface(type1);
        tv_5b.setTypeface(type1);
        tv_730b.setTypeface(type1);
        tv_time.setTypeface(type1);
        tv_sat.setTypeface(type1);
        tv_sun.setTypeface(type1);

        jan .setTypeface(type1);
        feb.setTypeface(type1);
        mar.setTypeface(type1);
        apr.setTypeface(type1);
        may.setTypeface(type1);
        june.setTypeface(type1);
        july.setTypeface(type1);
        aug.setTypeface(type1);
        sep.setTypeface(type1);
        oct.setTypeface(type1);
        nov.setTypeface(type1);
        dec.setTypeface(type1);
        tv_mp.setTypeface(type1);

        btn_search.setTypeface(type1);

        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);
        ex4 = (ExpandableLayout) findViewById(R.id.expandable_layout_4);



        ll = (LinearLayout) findViewById(R.id.ll);

        ll.setVisibility(View.INVISIBLE);


        tv_click();
        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        g2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });
        g4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
            }
        });

        tv_mp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
            }
        });

        tv_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        //select class
        tv_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "7";
                tv_class.setText("Class 7");
                expandableLayout0.collapse();
            }
        });
        tv_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "8";
                tv_class.setText("Class 8");
                expandableLayout0.collapse();
            }
        });
        tv_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "9";
                tv_class.setText("Class 9");
                expandableLayout0.collapse();
            }
        });
        tv_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_class = "1";
                tv_class.setText("Class 10");
                expandableLayout0.collapse();
            }
        });



        //select batch
        tv_7b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0700am";
                tv_batch.setText("7 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_8b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0800am";
                tv_batch.setText("8 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_9b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0900am";
                tv_batch.setText("9 AM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_12b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "1200pm";
                tv_batch.setText("12 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_3b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0300pm";
                tv_batch.setText("3 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_4b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0400pm";
                tv_batch.setText("4 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_5b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0500pm";
                tv_batch.setText("5 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });
        tv_730b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_batch = "0730pm";
                tv_batch.setText("7 30 PM");
                expandableLayout1.collapse();
                ll.setVisibility(View.VISIBLE);
            }
        });


        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });


        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "sat";
                tv_time.setText("SAT");
                expandableLayout2.collapse();
            }
        });

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sat_sun = "sun";
                tv_time.setText("SUN");
                expandableLayout2.collapse();
            }
        });


        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                batch_time = search_batch+" "+sat_sun;

                if(search_batch == ""  || sat_sun == ""){
                    editor.putString("s_class",search_class);
                    editor.putString("s_batch",batch_time);
                    editor.putString("class","false");
                    editor.putString("month",month);
                    editor.commit();

                    //Toast.makeText(s_by_r_class_and_batch.this, batch_time +" "+search_class, Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(getApplicationContext(),display_payment.class);
                    startActivity(i);
                }
                else if(search_class != "") {

                    editor.putString("s_class",search_class);
                    editor.putString("class","true");
                    editor.putString("month",month);
                    editor.commit();

                    Intent i = new Intent(getApplicationContext(),display_payment.class);
                    startActivity(i);

                    //Toast.makeText(s_by_r_class_and_batch.this, search_class, Toast.LENGTH_SHORT).show();


                }



            }
        });
    }
    private void tv_click(){
        jan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }

                tv_mp.setText("January");
                month="jan";

            }
        });
        feb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }

                tv_mp.setText("February");
                month="feb";
            }
        });
        mar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }

                tv_mp.setText("March");
                month="mar";
            }
        });
        apr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="apr";
                tv_mp.setText("April");
            }
        });
        may.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="may";
                tv_mp.setText("May");
            }
        });
        june.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="june";
                tv_mp.setText("June");
            }
        });
        july.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="july";
                tv_mp.setText("July");
            }
        });
        aug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="aug";
                tv_mp.setText("August");
            }
        });
        sep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="sep";
                tv_mp.setText("September");
            }
        });
        oct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="oct";
                tv_mp.setText("October");
            }
        });
        nov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="nov";
                tv_mp.setText("November");
            }
        });
        dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ex4.isExpanded()) {
                    ex4.collapse();
                }
                else {
                    ex4.expand();
                }
                month="dec";
                tv_mp.setText("December");
            }
        });

    }

}
