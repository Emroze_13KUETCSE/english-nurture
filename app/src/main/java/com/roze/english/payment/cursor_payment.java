package com.roze.english.payment;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.roze.english.R;

/**
 * Created by emroz on 4/28/2017.
 */

public class cursor_payment extends CursorAdapter {

    String db_month;

    public static String PREFS_NAME="math";

    public cursor_payment(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.s_by_class_and_batch_listview, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView big_latter = (TextView) view.findViewById(R.id.tv_big_lv);
        TextView f_name = (TextView) view.findViewById(R.id.tv_full_name_lv);
        TextView n_name = (TextView) view.findViewById(R.id.tv_nick_name_lv);
        TextView regi_ID = (TextView) view.findViewById(R.id.tv_regi_lv);
        TextView school = (TextView) view.findViewById(R.id.tv_school_lv);
        TextView paid = (TextView) view.findViewById(R.id.tv_paid_lv);
        TextView bt = (TextView) view.findViewById(R.id.tv_bt);


        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

        big_latter.setTypeface(type);
        f_name.setTypeface(type);
        n_name.setTypeface(type1);
        regi_ID.setTypeface(type1);
        school.setTypeface(type1);
        paid.setTypeface(type);
        bt.setTypeface(type);



        db_month = preference.getString("month","");
        // Extract properties from cursor
        String str_big_latter ;
        String str_f_name = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
        String str_n_name = cursor.getString(cursor.getColumnIndexOrThrow("nick_name"));
        String str_regi = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
        String str_school = cursor.getString(cursor.getColumnIndexOrThrow("school"));
        String str_paid = cursor.getString(cursor.getColumnIndexOrThrow(db_month));
        String str_bt = cursor.getString(cursor.getColumnIndexOrThrow("batch_time"));

        if(str_paid.equals("1")){
            str_paid = "Paid";
        }
        else {
            str_paid = "";
        }

        if(str_f_name.equals("")){
            str_big_latter="";
        }
        else {
            char c = str_f_name.charAt(0);
            str_big_latter = String.valueOf(c);
        }



        // Populate fields with extracted properties
        big_latter.setText(str_big_latter);
        f_name.setText(str_f_name);
        n_name.setText(str_n_name);
        regi_ID.setText(str_regi);
        school.setText(str_school);
        paid.setText(str_paid);
        bt.setText(str_bt);



        //tvPriority.setText(String.valueOf(priority));
    }
}
