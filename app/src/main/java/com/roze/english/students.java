package com.roze.english;

/**
 * Created by emroz on 4/4/2017.
 */

public class students {
    public String regi;
    public String full_name;
    public String nick_name;
    public String fa_name;
    public String school;
    public String batch_time;
    public String mobile;
    public String course;

    public String date;
    public String fa_ocu;
    public String address;
    public String ma_name;
    public String ma_ocu;
    public String ma_con;

    public String id;


    public String jan;
    public String feb;
    public String mar;
    public String apr;
    public String may;
    public String june;
    public String july;
    public String aug;
    public String sep;
    public String oct;
    public String nov;
    public String dec;

    public students() {
    }

    /*
    id	"258"
    regi	"178001"
    batch_time	"12/31/99"
    date	""
    s_nick	"Nobel"
    s_full	"NOKIBUR RAHMAN"
    father_name	"Md. Saifur Rahaman"
    fa_ocu	""
    mother_name	""
    ma_ocu	""
    address	""
    school	"LAB"
    course	"8"
    fa_contact	"01718319219"
    ma_contact	""
    */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getRegi() {
        return regi;
    }

    public void setRegi(String regi) {
        this.regi = regi;
    }


    public String getBatch_time() {
        return batch_time;
    }

    public void setBatch_time(String batch_time) {
        this.batch_time = batch_time;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }


    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }



    public String getFa_name() {
        return fa_name;
    }

    public void setFa_name(String fa_name) {
        this.fa_name = fa_name;
    }



    public String getFa_ocu() {
        return fa_ocu;
    }

    public void setFa_ocu(String fa_ocu) {
        this.fa_ocu = fa_ocu;
    }


    public String getMa_name() {
        return ma_name;
    }

    public void setMa_name(String ma_name) {
        this.ma_name = ma_name;
    }


    public String getMa_ocu() {
        return ma_ocu;
    }

    public void setMa_ocu(String ma_ocu) {
        this.ma_ocu = ma_ocu;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }




    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }


    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }



    public String getMa_con() {
        return ma_con;
    }

    public void setMa_con(String ma_con) {
        this.ma_con = ma_con;
    }




    public String getJan() {
        return jan;
    }

    public void setJan(String jan) {
        this.jan = jan;
    }

    public String getFeb() {
        return feb;
    }

    public void setFeb(String feb) {
        this.feb = feb;
    }

    public String getMar() {
        return mar;
    }

    public void setMar(String mar) {
        this.mar = mar;
    }

    public String getApr() {
        return apr;
    }

    public void setApr(String apr) {
        this.apr = apr;
    }

    public String getMay() {
        return may;
    }

    public void setMay(String may) {
        this.may = may;
    }

    public String getJune() {
        return june;
    }

    public void setJune(String june) {
        this.june = june;
    }

    public String getJuly() {
        return july;
    }

    public void setJuly(String july) {
        this.july = july;
    }

    public String getAug() {
        return aug;
    }

    public void setAug(String aug) {
        this.aug = aug;
    }

    public String getSep() {
        return sep;
    }

    public void setSep(String sep) {
        this.sep = sep;
    }

    public String getOct() {
        return oct;
    }

    public void setOct(String oct) {
        this.oct = oct;
    }

    public String getNov() {
        return nov;
    }

    public void setNov(String nov) {
        this.nov = nov;
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }


}
