package com.roze.english;

/**
 * Created by emroze on 6/17/17.
 */

public class students1 {
    public String id;
    public String regi;
    public String batch_time;
    public String date;
    public String s_nick;
    public String s_full;
    public String father_name;
    public String fa_ocu;
    public String mother_name;
    public String ma_ocu;
    public String address;
    public String school;
    public String course;
    public String fa_contact;
    public String ma_contact;


    public String jan;
    public String feb;
    public String mar;
    public String apr;
    public String may;
    public String june;
    public String july;
    public String aug;
    public String sep;
    public String oct;
    public String nov;
    public String dec;

    public students1() {
    }

    /*
    id	"258"
    regi	"178001"
    batch_time	"12/31/99"
    date	""
    s_nick	"Nobel"
    s_full	"NOKIBUR RAHMAN"
    father_name	"Md. Saifur Rahaman"
    fa_ocu	""
    mother_name	""
    ma_ocu	""
    address	""
    school	"LAB"
    course	"8"
    fa_contact	"01718319219"
    ma_contact	""
    */



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getRegi() {
        return regi;
    }

    public void setRegi(String regi) {
        this.regi = regi;
    }


    public String getBatch_time() {
        return batch_time;
    }

    public void setBatch_time(String batch_time) {
        this.batch_time = batch_time;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getS_nick() {
        return s_nick;
    }

    public void setS_nick(String s_nick) {
        this.s_nick = s_nick;
    }


    public String getS_full() {
        return s_full;
    }

    public void setS_full(String s_full) {
        this.s_full = s_full;
    }



    public String getFa_ocu() {
        return fa_ocu;
    }

    public void setFa_ocu(String fa_ocu) {
        this.fa_ocu = fa_ocu;
    }




    public String getMa_ocu() {
        return ma_ocu;
    }

    public void setMa_ocu(String ma_ocu) {
        this.ma_ocu = ma_ocu;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }




    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }


    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }


    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

    public String getFa_contact() {
        return fa_contact;
    }

    public void setFa_contact(String fa_contact) {
        this.fa_contact = fa_contact;
    }

    public String getMa_contact() {
        return ma_contact;
    }

    public void setMa_contact(String ma_contact) {
        this.ma_contact = ma_contact;
    }

    public String getJan() {
        return jan;
    }

    public void setJan(String jan) {
        this.jan = jan;
    }

    public String getFeb() {
        return feb;
    }

    public void setFeb(String feb) {
        this.feb = feb;
    }

    public String getMar() {
        return mar;
    }

    public void setMar(String mar) {
        this.mar = mar;
    }

    public String getApr() {
        return apr;
    }

    public void setApr(String apr) {
        this.apr = apr;
    }

    public String getMay() {
        return may;
    }

    public void setMay(String may) {
        this.may = may;
    }

    public String getJune() {
        return june;
    }

    public void setJune(String june) {
        this.june = june;
    }

    public String getJuly() {
        return july;
    }

    public void setJuly(String july) {
        this.july = july;
    }

    public String getAug() {
        return aug;
    }

    public void setAug(String aug) {
        this.aug = aug;
    }

    public String getSep() {
        return sep;
    }

    public void setSep(String sep) {
        this.sep = sep;
    }

    public String getOct() {
        return oct;
    }

    public void setOct(String oct) {
        this.oct = oct;
    }

    public String getNov() {
        return nov;
    }

    public void setNov(String nov) {
        this.nov = nov;
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }

}
