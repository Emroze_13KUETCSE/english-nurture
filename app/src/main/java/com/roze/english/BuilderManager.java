package com.roze.english;

/**
 * Created by emroz on 4/1/2017.
 */


import android.graphics.Color;

import com.nightonke.boommenu.BoomButtons.HamButton;

/**
 * Created by Weiping Huang at 23:44 on 16/11/21
 * For Personal Open Source
 * Contact me at 2584541288@qq.com or nightonke@outlook.com
 * For more projects: https://github.com/Nightonke
 */
public class BuilderManager {

    private static int[] color = new int[]{
            R.color.color1,
            R.color.color2,
            R.color.color3,
            R.color.color4,
            R.color.color5,
            R.color.color6,
    };



    //track
    private static int[] imageResourcestrack = new int[]{
            R.drawable.setting1,
            R.drawable.setting1,
            R.drawable.setting1,
            R.drawable.setting1,
    };
    private static int[] txtResourcestrack = new int[]{
            R.string.one1,
            R.string.two1,
            R.string.three1,
            R.string.four1,
    };
    private static int[] txtResourcesSubtrack = new int[]{
            R.string.sub_one1,
            R.string.sub_two1,
            R.string.sub_three1,
            R.string.sub_four1,
    };






    private static int color_index = 0;


    //track
    private static int imageResourceIndextrack = 0;
    private static int txtResourceIndexsubtrack = 0;
    private static int txtResourceIndextrack = 0;



    static int getcolor() {
        if (color_index >= color.length) color_index = 0;
        return color[color_index++];
    }

    //track
    static int getImageResourcetrack() {
        if (imageResourceIndextrack >= imageResourcestrack.length) imageResourceIndextrack = 0;
        return imageResourcestrack[imageResourceIndextrack++];
    }
    static int getTxtResourcetrack() {
        if (txtResourceIndextrack >= txtResourcestrack.length) txtResourceIndextrack = 0;
        return txtResourcestrack[txtResourceIndextrack++];
    }
    static int getTxtResourceSubtrack() {
        if (txtResourceIndexsubtrack >= txtResourcesSubtrack.length) txtResourceIndexsubtrack = 0;
        return txtResourcesSubtrack[txtResourceIndexsubtrack++];
    }









    //track
    static HamButton.Builder getHamButtonBuilder0() {
        return new HamButton.Builder()
                .normalImageRes(getImageResourcetrack())
                .normalTextRes(getTxtResourcetrack())
                .rippleEffect(true)
                .textSize(18)
                .subTextSize(13)
                .normalTextColor(Color.parseColor("#86e3f4"))
                .shadowColor(Color.parseColor("#F200306E"))
                .pieceColor(Color.parseColor("#6A1B9A"))
                .normalColor(Color.parseColor("#310D5D"))
                .subNormalTextRes(getTxtResourceSubtrack())
                .pieceColor(Color.WHITE);
    }


    private static BuilderManager ourInstance = new BuilderManager();

    public static BuilderManager getInstance() {
        return ourInstance;
    }

    private BuilderManager() {
    }
}
