package com.roze.english;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import net.cachapa.expandablelayout.ExpandableLayout;

import pl.droidsonroids.gif.GifTextView;

public class s_by_name extends AppCompatActivity {

    public static String PREFS_NAME="math";

    ExpandableLayout expandableLayout0;

    TextView tv_nn,tv_fn;

    EditText et_name;
    Button btn_search;
    String name="";

    GifTextView g1;

    String fn,nn = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_by_name);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        editor.putString("f_n_name","nick_name");
        editor.commit();

        et_name = (EditText) findViewById(R.id.et_name);
        btn_search = (Button) findViewById(R.id.btn_search);
        tv_nn = (TextView) findViewById(R.id.tv_nn);
        tv_fn = (TextView) findViewById(R.id.tv_fn);
        g1 = (GifTextView) findViewById(R.id.gif1);

        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);

        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_fn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayout0.collapse();
                Toast.makeText(s_by_name.this, "Full Name Selected", Toast.LENGTH_SHORT).show();
                editor.putString("f_n_name","full_name");
                editor.commit();
            }
        });

        tv_nn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayout0.collapse();
                Toast.makeText(s_by_name.this, "Nick Name Selected", Toast.LENGTH_SHORT).show();
                editor.putString("f_n_name","nick_name");
                editor.commit();
            }
        });


        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = et_name.getText().toString();

                if(name.equals("")){}
                else{
                    editor.putString("temp_name",name);
                    editor.commit();
                    Intent inn = new Intent(getApplicationContext(), Display_s_by_name.class);
                    startActivity(inn);
                }

            }
        });

    }
}
