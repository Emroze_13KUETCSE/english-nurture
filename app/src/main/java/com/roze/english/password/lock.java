package com.roze.english.password;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.R;
import com.roze.english.insert.choose;
import com.roze.english.more_options.more_option;
import com.roze.english.search;
import com.roze.english.send_sms.choose_sms_send;

public class lock extends AppCompatActivity {
    public static String PREFS_NAME="math";

    Button btn_ok;
    EditText et;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);

        btn_ok = (Button) findViewById(R.id.btn_ok);
        et = (EditText) findViewById(R.id.editText);
        tv = (TextView) findViewById(R.id.tv_pass);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        btn_ok.setTypeface(type1);
        tv.setTypeface(type1);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(lock.this, "1", Toast.LENGTH_SHORT).show();
                if(et.getText().toString() == ""){
                    Toast.makeText(lock.this, "Empty", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(preference.getString("pass","").equals(et.getText().toString())){

                        if(preference.getString("lock","").equals("1")){

                            Intent i=new Intent(getApplicationContext(),search.class);
                            startActivity(i);
                        }
                        else if(preference.getString("lock","").equals("2")){

                            Intent i=new Intent(getApplicationContext(),choose_sms_send.class);
                            startActivity(i);
                        }
                        else if(preference.getString("lock","").equals("3")){

                            Intent i=new Intent(getApplicationContext(),more_option.class);
                            startActivity(i);
                        }
                        else if(preference.getString("lock","").equals("4")){

                            Intent i=new Intent(getApplicationContext(),choose.class);
                            startActivity(i);
                        }
                        else if(preference.getString("lock","").equals("5")){

                            Intent i=new Intent(getApplicationContext(),pass_setti.class);
                            startActivity(i);
                        }

                        finish();
                    }
                    else{
                        Toast.makeText(lock.this, "Incorrect", Toast.LENGTH_SHORT).show();
                        et.setText("");
                    }
                }
            }
        });

    }
}
