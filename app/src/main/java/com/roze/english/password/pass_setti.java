package com.roze.english.password;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roze.english.R;
import com.roze.english.insert.change_pass;

public class pass_setti extends AppCompatActivity {
    public static String PREFS_NAME="math";

    TextView tv1,tv2,tv3,tv4,tv5,tv6;
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_setti);

        tv1 = (TextView) findViewById(R.id.tv_on_off2);
        tv2 = (TextView) findViewById(R.id.tv_on_off3);
        tv3 = (TextView) findViewById(R.id.tv_on_off4);
        tv4 = (TextView) findViewById(R.id.tv_on_off5);
        tv5 = (TextView) findViewById(R.id.tv_full_name_lv1);
        tv6 = (TextView) findViewById(R.id.tv_nick_name_lv1);

        ll = (LinearLayout) findViewById(R.id.ll_delete1);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("search_on","").equals("true")) {

            tv1.setText("On");
            tv1.setBackgroundResource(R.drawable.border2);
            tv1.setTextColor(Color.rgb(255,255,255));
        }
        else{
            tv1.setText("Off");
            tv1.setBackgroundResource(R.drawable.border5);
            tv1.setTextColor(Color.rgb(0,0,0));
        }
        if(preference.getString("sms_on","").equals("true")) {
            tv2.setText("On");
            tv2.setBackgroundResource(R.drawable.border2);
            tv2.setTextColor(Color.rgb(255,255,255));
        }
        else{
            tv2.setText("Off");
            tv2.setBackgroundResource(R.drawable.border5);
            tv2.setTextColor(Color.rgb(0,0,0));
        }
        if(preference.getString("setti_on","").equals("true")) {
            tv3.setText("On");
            tv3.setBackgroundResource(R.drawable.border2);
            tv3.setTextColor(Color.rgb(255,255,255));
        }
        else{
            tv3.setText("Off");
            tv3.setBackgroundResource(R.drawable.border5);
            tv3.setTextColor(Color.rgb(0,0,0));
        }
        if(preference.getString("regi_on","").equals("true")) {
            tv4.setText("On");
            tv4.setBackgroundResource(R.drawable.border2);
            tv4.setTextColor(Color.rgb(255,255,255));
        }
        else{
            tv4.setText("Off");
            tv4.setBackgroundResource(R.drawable.border5);
            tv4.setTextColor(Color.rgb(0,0,0));
        }

        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),change_pass.class);
                startActivity(i);
            }
        });
        tv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),change_pass.class);
                startActivity(i);
            }
        });
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),change_pass.class);
                startActivity(i);
            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tv1.getText().toString().equals("On")){
                    tv1.setText("Off");
                    tv1.setBackgroundResource(R.drawable.border5);
                    tv1.setTextColor(Color.rgb(0,0,0));
                    editor.putString("search_on", "f");
                    editor.commit();
                }
                else{
                    tv1.setText("On");
                    tv1.setBackgroundResource(R.drawable.border2);
                    tv1.setTextColor(Color.rgb(255,255,255));
                    editor.putString("search_on", "true");
                    editor.commit();
                }
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tv2.getText().toString().equals("On")){
                    tv2.setText("Off");
                    tv2.setBackgroundResource(R.drawable.border5);
                    tv2.setTextColor(Color.rgb(0,0,0));
                    editor.putString("sms_on", "f");
                    editor.commit();
                }
                else{
                    tv2.setText("On");
                    tv2.setBackgroundResource(R.drawable.border2);
                    tv2.setTextColor(Color.rgb(255,255,255));
                    editor.putString("sms_on", "true");
                    editor.commit();
                }
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tv3.getText().toString().equals("On")){
                    tv3.setText("Off");
                    tv3.setBackgroundResource(R.drawable.border5);
                    tv3.setTextColor(Color.rgb(0,0,0));
                    editor.putString("setti_on", "f");
                    editor.commit();
                }
                else{
                    tv3.setText("On");
                    tv3.setBackgroundResource(R.drawable.border2);
                    tv3.setTextColor(Color.rgb(255,255,255));
                    editor.putString("setti_on", "true");
                    editor.commit();
                }
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tv4.getText().toString().equals("On")){
                    tv4.setText("Off");
                    tv4.setBackgroundResource(R.drawable.border5);
                    tv4.setTextColor(Color.rgb(0,0,0));
                    editor.putString("regi_on", "f");
                    editor.commit();
                }
                else{
                    tv4.setText("On");
                    tv4.setBackgroundResource(R.drawable.border2);
                    tv4.setTextColor(Color.rgb(255,255,255));
                    editor.putString("regi_on", "true");
                    editor.commit();
                }
            }
        });
    }
}
