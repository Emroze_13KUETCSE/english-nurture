package com.roze.english.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.gsm.SmsManager;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.MainActivity;
import com.roze.english.R;
import com.roze.english.send_sms.sms_display;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MyService extends Service {
    public static String PREFS_NAME="math";

    TextView tv;
    ProgressBar pb;

    int STATUS_BAR_NOTIFICATION = 1;
    private Context context;
    private NotificationManager nm;
    private Notification noti;

    int count=0,len;
    int check = 0;
    int g=0;
    String[] buffer ;
    String sms="";
    int [] regi;

    Thread t;
    RemoteViews remoteViews;
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();

        STATUS_BAR_NOTIFICATION=1;

        nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        CharSequence tickerText = "hello";
        long when = System.currentTimeMillis();
        noti = new Notification(R.drawable.logo, tickerText, when);
        context = MyService.this;
        Intent notiIntent = new Intent(context, MyService.class);
        PendingIntent pi = PendingIntent.getService(context, 0, notiIntent, 0);
        noti.flags = Notification.FLAG_SHOW_LIGHTS;
        noti.ledARGB = 0xff00ff00;
        noti.ledOnMS = 3000;
        noti.ledOffMS = 5000;


        CharSequence title = "English Nurture...";
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.noti);
        contentView.setImageViewResource(R.id.status_icon, R.drawable.logo);
        contentView.setTextViewText(R.id.status_text, title);
        contentView.setProgressBar(R.id.status_progress, 100, 0, false);
        noti.contentView = contentView;
        noti.contentIntent = pi;
        nm.notify(STATUS_BAR_NOTIFICATION, noti);


        Bundle b=intent.getExtras();
        buffer = b.getStringArray("number_array");
        regi = b.getIntArray("regi_array");

        len = buffer.length;

        Toast.makeText(context, String.valueOf(startId), Toast.LENGTH_SHORT).show();

        sms = preference.getString("sms","");

        final Handler handler = new Handler();
        final Timer timer = new Timer();


/*        t = new Thread(){
            @Override
            public void run() {
                try {


                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        t.start();*/
        final TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(check == 0 && count < len){
                            check = 1;


                            editor.putInt("sms_count",count);
                            editor.commit();
                            CharSequence title = "Sending: " + (count*100)/len + "%"+"("+count+"/"+len+")"+" Regi : "+String.valueOf(regi[g]);
                            noti.contentView.setTextViewText(R.id.status_text, title);
                            noti.contentView.setProgressBar(R.id.status_progress, 100,(count*100)/len, false);
                            nm.notify(STATUS_BAR_NOTIFICATION, noti);


                            if(g < len){
                                sendSMS(buffer[g],sms);
                            }



                        }
                        if(count == len){

                            CharSequence title1 = "Sending Successful. 100%";
                            noti.contentView.setTextViewText(R.id.status_text, title1);
                            noti.contentView.setProgressBar(R.id.status_progress, 100,(count*100)/len, false);
                            nm.notify(STATUS_BAR_NOTIFICATION, noti);

                            noti.flags |= Notification.FLAG_AUTO_CANCEL;
                            count+=100;
                            editor.putInt("sms_count",count);
                            editor.commit();


                            Toast.makeText(MyService.this, "Successfully send all SMS", Toast.LENGTH_SHORT).show();

                            stopSelf();
                                /*new SweetAlertDialog(MyService.this, SweetAlertDialog.SUCCESS_TYPE)

                                        .setTitleText("Successfully send all SMS")
                                        .setContentText(len+" SMS has been sent")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(final SweetAlertDialog sDialog) {

                                                nm.cancel(STATUS_BAR_NOTIFICATION);
                                                nm.cancelAll();
                                                //
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();*/


                        }

                        //Toast.makeText(context, "here", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        timer.schedule(timerTask, 0, 500);


        return START_STICKY;
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(context, "Destroy", Toast.LENGTH_SHORT).show();
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor = preference.edit();

        editor.putInt("sms_count",0);
        editor.commit();
        System.exit(0);
        /*try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }

    // for sms delivery report
    BroadcastReceiver sendBroadcastReceiver = new MyService.SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new MyService.DeliverReceiver();


    private void sendSMS(String phoneNumber, String message) {
        try{
            if(phoneNumber.length() == 11){
                String SENT = "SMS_SENT";
                String DELIVERED = "SMS_DELIVERED";

                SmsManager sm = SmsManager.getDefault();
                ArrayList<String> parts =sm.divideMessage(message);
                int numParts = parts.size();

                if(numParts == 1){
                    PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                            SENT), 0);

                    PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                            new Intent(DELIVERED), 0);

                    registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

                    registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
                    //Toast.makeText(this, String.valueOf(numParts), Toast.LENGTH_SHORT).show();

                }
                else {
                    ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
                    ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

                    registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

                    registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));

                    for (int i = 0; i < numParts; i++) {
                        sentIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(SENT), 0));
                        deliveryIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(DELIVERED), 0));
                    }

                    sm.sendMultipartTextMessage(phoneNumber,null, parts, sentIntents, deliveryIntents);

                }
            }
            else {
                count++;
                g++;
                check=0;
            }
        }catch (NullPointerException ee){
            count++;
            g++;
            check=0;
        }
    }

    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Toast.makeText(getBaseContext(), "SMS delivered",
                    //    Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    //Toast.makeText(getBaseContext(), "sms not delivered",
                    //    Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {



            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Toast.makeText(context, "OK "+String.valueOf(count), Toast.LENGTH_SHORT).show();
                    count++;
                    g++;
                    check=0;


                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(context, "Generic", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(context, "No service", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(context, "NULL PDU", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(context, "Radio OFF", Toast.LENGTH_SHORT).show();

                    break;
            }

        }
    }

    public static int[] removeElement(int[] original, int element){
        int[] n = new int[original.length - 1];
        System.arraycopy(original, 0, n, 0, element );
        System.arraycopy(original, element+1, n, element, original.length - element-1);
        return n;
    }
}
