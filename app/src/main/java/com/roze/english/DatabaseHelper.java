package com.roze.english;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by emroz on 4/4/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VIRSION = 1;
    public static final String DATABASE_NAME = "englishNuture.db";

    public  String TABLE_NAME = "students";
    public  String TABLE_NAME2 = "exam_marks";
    public  String TABLE_NAME3 = "exam_list";
    public  String TABLE_NAME4 = "result";


    public  String COLUMN_regi = "regi";
    public  String COLUMN_fname = "full_name";
    public  String COLUMN_nic_name = "nick_name";
    public  String COLUMN_fa_name = "father_name";
    public  String COLUMN_school = "school";
    public  String COLUMN_batch_time = "batch_time";
    public  String COLUMN_mobile = "mobile";
    public  String COLUMN_course = "course";



    public String COLUMN_jan = "jan";
    public String COLUMN_feb = "feb";
    public String COLUMN_mar = "mar";
    public String COLUMN_apr = "apr";
    public String COLUMN_may = "may";
    public String COLUMN_june = "june";
    public String COLUMN_july = "july";
    public String COLUMN_aug = "aug";
    public String COLUMN_sep = "sep";
    public String COLUMN_oct = "oct";
    public String COLUMN_nov = "nov";
    public String COLUMN_dec = "dec";


    private static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS students ( _id INTEGER PRIMARY KEY AUTOINCREMENT, regi VARCHAR,full_name VARCHAR, nick_name VARCHAR, father_name VARCHAR, school VARCHAR, batch_time VARCHAR, mobile VARCHAR, course VARCHAR, jan VARCHAR, feb VARCHAR, mar VARCHAR, apr VARCHAR, may VARCHAR, june VARCHAR, july VARCHAR, aug VARCHAR, sep VARCHAR, oct VARCHAR, nov VARCHAR, dec VARCHAR);";
    private static final String TABLE_CREATE2 = "CREATE TABLE IF NOT EXISTS exam_marks ( _id INTEGER PRIMARY KEY AUTOINCREMENT, regi VARCHAR, batch_time VARCHAR, course VARCHAR);";
    private static final String TABLE_CREATE3 = "CREATE TABLE IF NOT EXISTS exam_list ( _id INTEGER PRIMARY KEY AUTOINCREMENT, exam_name VARCHAR, exam_date VARCHAR, class VARCHAR, batch VARCHAR);";
    private static final String TABLE_CREATE4 = "CREATE TABLE IF NOT EXISTS result ( _id INTEGER PRIMARY KEY AUTOINCREMENT, regi VARCHAR, full_name VARCHAR, school VARCHAR, batch_time VARCHAR);";

    public SQLiteDatabase db;

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME , null , DATABASE_VIRSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "DROP TABLE IF EXISTS "+TABLE_NAME;
        String query2 = "DROP TABLE IF EXISTS "+TABLE_NAME2;
        String query3 = "DROP TABLE IF EXISTS "+TABLE_NAME3;
        db.execSQL(query);
        db.execSQL(query2);
        db.execSQL(query3);
        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_CREATE2);
        db.execSQL(TABLE_CREATE3);

        this.db = db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String query4 = "DROP TABLE IF EXISTS "+TABLE_NAME4;
        db.execSQL(query4);
        db.execSQL(TABLE_CREATE4);

        //this.onCreate(db);
    }

    public void delete_result_table(){
        db = this.getWritableDatabase();

        String alter_query = "DELETE FROM '"
                +TABLE_NAME4+"'";
        db.execSQL(alter_query);



    }
    public void drop_column_from_result(String column){
        db = this.getWritableDatabase();

        String alter_query = "ALTER TABLE '"
                + TABLE_NAME4+"'"+" DROP COLUMN '"+column+"'";
        db.execSQL(alter_query);


    }

    public void insert_into_result(students student){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();



            values.put(COLUMN_regi, student.getRegi());
            values.put(COLUMN_fname,student.getFull_name());
            values.put(COLUMN_school,student.getSchool());
            values.put(COLUMN_batch_time,student.getBatch_time());


            db.insert(TABLE_NAME4,null,values);


    }
    public void alter_result_table(String column){
        db = this.getWritableDatabase();

        String alter_query = "ALTER TABLE '"
                + TABLE_NAME4 + "' ADD COLUMN '" + column + "' VARCHAR;";
        db.execSQL(alter_query);

    }
    public void update_result_table(String regi,String mark,String exam_name){
        db = this.getWritableDatabase();
        Cursor cursor;
        cursor=db.rawQuery(" SELECT rowid _id,* FROM result",null);
        if(cursor.moveToFirst()){

            do{

                if(regi.equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){


                    String alter_query = "UPDATE "
                            + TABLE_NAME4 + " SET '" + exam_name + "' = '"+mark+"' WHERE _id = '"+cursor.getString(cursor.getColumnIndexOrThrow("_id"))+"';";
                    db.execSQL(alter_query);

                    break;
                }
            }
            while(cursor.moveToNext());
        }
    }

    public void update_exam_marks(String regi,String mark,String exam_name){
        db = this.getWritableDatabase();
        Cursor cursor;
        cursor=db.rawQuery(" SELECT rowid _id,* FROM exam_marks",null);
        if(cursor.moveToFirst()){

            do{

                if(regi.equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){


                    String alter_query = "UPDATE "
                            + TABLE_NAME2 + " SET '" + exam_name + "' = '"+mark+"' WHERE _id = '"+cursor.getString(cursor.getColumnIndexOrThrow("_id"))+"';";
                    db.execSQL(alter_query);

                    break;
                }
            }
            while(cursor.moveToNext());
        }
    }


    public void update_std_details(students student,String pre_regi){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor;

        cursor=db.rawQuery(" SELECT rowid _id, regi, full_name, nick_name, father_name,school, batch_time , mobile, course FROM students",null);

        if(cursor.moveToFirst()){

            do{

                if(pre_regi.equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){

                    values.put(COLUMN_regi, student.getRegi());
                    values.put(COLUMN_fname,student.getFull_name());
                    values.put(COLUMN_nic_name,student.getNick_name());
                    values.put(COLUMN_fa_name,student.getFa_name());
                    values.put(COLUMN_school,student.getSchool());
                    values.put(COLUMN_batch_time,student.getBatch_time());
                    values.put(COLUMN_mobile,student.getMobile());
                    values.put(COLUMN_course,student.getCourse());

                    db.update(TABLE_NAME, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                    //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            while(cursor.moveToNext());
        }
    }
    public void update_marks_table(String regi,String mark,String ex_name){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor;

        cursor=db.rawQuery(" SELECT rowid _id, regi,* FROM exam_marks",null);

        if(cursor.moveToFirst()){

            do{

                if(regi.equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){


                    String alter_query = "UPDATE "
                            + TABLE_NAME2 + " SET '" + ex_name + "' = '"+mark+"' WHERE _id = '"+cursor.getString(cursor.getColumnIndexOrThrow("_id"))+"';";
                    db.execSQL(alter_query);
                    db.close();

                    break;
                }
            }
            while(cursor.moveToNext());
        }
    }

    public void update_month_payment(students student){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor;

        cursor=db.rawQuery(" SELECT rowid _id, regi, full_name, nick_name, father_name,school, batch_time , mobile,jan, feb, mar, apr, may, june, july, aug, sep,oct, nov, dec FROM students",null);

        if(cursor.moveToFirst()){

            do{

                if(student.getRegi().equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){

                    values.put(COLUMN_jan,student.getJan());
                    values.put(COLUMN_feb,student.getFeb());
                    values.put(COLUMN_mar,student.getMar());
                    values.put(COLUMN_apr,student.getApr());
                    values.put(COLUMN_may,student.getMay());
                    values.put(COLUMN_june,student.getJune());
                    values.put(COLUMN_july,student.getJuly());
                    values.put(COLUMN_aug,student.getAug());
                    values.put(COLUMN_sep,student.getSep());
                    values.put(COLUMN_oct,student.getOct());
                    values.put(COLUMN_nov,student.getNov());
                    values.put(COLUMN_dec,student.getDec());

                    db.update(TABLE_NAME, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                    //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            while(cursor.moveToNext());
        }
    }

    public void delete_std(students student){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        db.delete(TABLE_NAME,"regi="+student.getRegi(),null);
        db.delete(TABLE_NAME2,"regi="+student.getRegi(),null);
        db.close();
    }
    public void delete_All(String course){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        db.delete(TABLE_NAME,"course="+course,null);
        db.delete(TABLE_NAME2,"course="+course,null);
        db.close();
    }

    public void delete_exam_list(String exam_name){
        db = this.getWritableDatabase();

        db.delete(TABLE_NAME3,"exam_name='"+exam_name+"'",null);

        db.close();
    }

    public void delete_exam_marks_column(String exam_name){
        db = this.getWritableDatabase();

        Cursor cursor;
        cursor=db.rawQuery("SELECT rowid _id,* FROM exam_list",null);


        String[] mark_list = new String[cursor.getCount()-1];
        int i =0;
        if(cursor.moveToFirst()){

            do{


                if(mark_list.length!=0 && !exam_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("exam_name")))){
                    mark_list[i] = cursor.getString(cursor.getColumnIndexOrThrow("exam_name"));
                    i++;
                }
            }
            while(cursor.moveToNext());
        }


        Cursor cursor1;
        cursor1 = db.rawQuery("SELECT rowid _id,* FROM exam_marks",null);
        String[][] ex_marks = new String[cursor1.getCount()][cursor1.getColumnCount()-2];

        int a = 0;
        int b = 0;
        if(cursor1.moveToFirst()){
            do{
                ex_marks[a][0] = cursor1.getString(cursor1.getColumnIndexOrThrow("regi"));
                ex_marks[a][1] = cursor1.getString(cursor1.getColumnIndexOrThrow("batch_time"));
                ex_marks[a][2] = cursor1.getString(cursor1.getColumnIndexOrThrow("course"));

                for(int ii =0; ii < mark_list.length; ii++){
                    ex_marks[a][ii+3]= cursor1.getString(cursor1.getColumnIndexOrThrow(mark_list[ii]));
                }
                a++;
            }while(cursor1.moveToNext());
        }

        String query2 = "DROP TABLE IF EXISTS "+TABLE_NAME2;
        db.execSQL(query2);
        db.execSQL(TABLE_CREATE2);

        ContentValues values = new ContentValues();

        for(int aa = 0; aa < cursor1.getCount(); aa++){
            values.put(COLUMN_regi, ex_marks[aa][0]);
            values.put(COLUMN_batch_time,ex_marks[aa][1]);
            values.put(COLUMN_course,ex_marks[aa][2]);

            db.insert(TABLE_NAME2,null,values);
        }

        for(int k = 0; k < mark_list.length;k++){
            alter_marks_table(mark_list[k]);
        }

        for(int jj = 0 ; jj < cursor1.getCount();jj++){
            for(int kk = 0; kk < mark_list.length;kk++){
                update_marks_table(ex_marks[jj][0],ex_marks[jj][kk+3],mark_list[kk]);
            }
        }


        delete_exam_list(exam_name);
    }

    public boolean insertStudents_check(students student){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = db.rawQuery("SELECT rowid _id, regi FROM students", null);

        int i = 0;
        if(cursor.moveToFirst()){
            do{
                String db_regi = cursor.getString(1);
                if(student.getRegi().equals(db_regi)){
                    i=0;
                    break;
                }
                i++;
            }
            while(cursor.moveToNext());
        }

        boolean bbbb = false;
        if(i == 0){
            bbbb = false;
        }else if(i == cursor.getCount()){
            bbbb = true;
        }

        db.close();

        return bbbb;
        
    }
    public void alter_marks_table(String column){
        db = this.getWritableDatabase();

            String alter_query = "ALTER TABLE '"
                    + TABLE_NAME2 + "' ADD COLUMN '" + column + "' VARCHAR;";
            db.execSQL(alter_query);


    }
    public boolean check_alter_marks_table(String column){
        db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT rowid _id, exam_name FROM exam_list", null);

        boolean b =false;
        int i = 0;
        if(cursor.moveToFirst()){
            do{
                String db_regi = cursor.getString(1);
                if(column.equals(db_regi)){
                    i=0;
                    break;
                }
                i++;
            }
            while(cursor.moveToNext());
        }

        int row = cursor.getCount();

        if(i == cursor.getCount()){

            b = true;

        }
        return b;

    }

    public void insert_into_marks_list(String ex_name,String ex_date,String class_a,String batch){

        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

            values.put("exam_name", ex_name);
            values.put("exam_date",ex_date);
            values.put("class",class_a);
            values.put("batch",batch);

            db.insert(TABLE_NAME3,null,values);


        db.close();
    }

    public void insert_marks_table(students student){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = db.rawQuery("SELECT rowid _id, regi FROM exam_marks", null);
        int i = 0;

        if(cursor.moveToFirst()){
            do{
                String db_regi = cursor.getString(1);
                if(student.getRegi().equals(db_regi)){
                    i=0;
                    break;
                }
                i++;
            }
            while(cursor.moveToNext());
        }

        int row = cursor.getCount();

        if(i == cursor.getCount()){
            values.put(COLUMN_regi, student.getRegi());
            values.put(COLUMN_batch_time,student.getBatch_time());
            values.put(COLUMN_course,student.getCourse());

            db.insert(TABLE_NAME2,null,values);
        }

        db.close();
    }

    public void insert_into_marks_table(students student,String[] column,String[] ary,int size){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = db.rawQuery("SELECT rowid _id, regi FROM exam_marks", null);

        int i = 0;
        if(cursor.moveToFirst()){
            do{
                String db_regi = cursor.getString(1);
                if(student.getRegi().equals(db_regi)){
                    i=0;
                    break;
                }
                i++;
            }
            while(cursor.moveToNext());
        }

        int row = cursor.getCount();

        if(i == cursor.getCount()){
            values.put(COLUMN_regi, student.getRegi());
            values.put(COLUMN_batch_time,student.getBatch_time());
            values.put(COLUMN_course,student.getCourse());
            for(int j = 0; j < size;j++){
                values.put(column[j],ary[j]);
            }

            db.insert(TABLE_NAME2,null,values);
        }

        db.close();
    }
    public void insert_into_marks_table2(students student,String[] column,String[] ary,int size){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = db.rawQuery("SELECT rowid _id, regi FROM exam_marks", null);

        int i = 0;
        if(cursor.moveToFirst()){
            do{
                String db_regi = cursor.getString(1);
                if(student.getRegi().equals(db_regi)){
                    i=0;
                    break;
                }
                i++;
            }
            while(cursor.moveToNext());
        }

        int row = cursor.getCount();

        if(i == cursor.getCount()){
            values.put(COLUMN_regi, student.getRegi());
            values.put(COLUMN_batch_time,student.getBatch_time());
            values.put(COLUMN_course,student.getCourse());

            for(int j = 0; j < size;j++){
                values.put(column[j],ary[j]);
            }
            db.insert(TABLE_NAME2,null,values);
        }

        db.close();
    }
    public void insertStudents(students student){
        //db.execSQL(TABLE_CREATE3);
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        /*String query = "select * from job";
        Cursor cursor = db.rawQuery(query,null);
        int count = cursor.getCount();


        values.put(COLUMN_ID,count);*/

        Cursor cursor = db.rawQuery("SELECT rowid _id, regi FROM students", null);

        int i = 0;
        if(cursor.moveToFirst()){
            do{
                String db_regi = cursor.getString(1);
                if(student.getRegi().equals(db_regi)){
                    i=0;
                    break;
                }
                i++;
            }
            while(cursor.moveToNext());
        }

        int row = cursor.getCount();

        if(i == cursor.getCount()){
            values.put(COLUMN_regi, student.getRegi());
            values.put(COLUMN_fname,student.getFull_name());
            values.put(COLUMN_nic_name,student.getNick_name());
            values.put(COLUMN_fa_name,student.getFa_name());
            values.put(COLUMN_school,student.getSchool());
            values.put(COLUMN_batch_time,student.getBatch_time());
            values.put(COLUMN_mobile,student.getMobile());
            values.put(COLUMN_course,student.getCourse());
            values.put(COLUMN_jan,student.getJan());
            values.put(COLUMN_feb,student.getFeb());
            values.put(COLUMN_mar,student.getMar());
            values.put(COLUMN_apr,student.getApr());
            values.put(COLUMN_may,student.getMay());
            values.put(COLUMN_june,student.getJune());
            values.put(COLUMN_july,student.getJuly());
            values.put(COLUMN_aug,student.getAug());
            values.put(COLUMN_sep,student.getSep());
            values.put(COLUMN_oct,student.getOct());
            values.put(COLUMN_nov,student.getNov());
            values.put(COLUMN_dec,student.getDec());

            db.insert(TABLE_NAME,null,values);
        }

        db.close();


    }
}
