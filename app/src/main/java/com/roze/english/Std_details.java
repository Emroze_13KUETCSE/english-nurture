package com.roze.english;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.edit.edit_display;
import com.roze.english.helper.AppConfig;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import pl.droidsonroids.gif.GifTextView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Std_details extends AppCompatActivity {

    String BASE_URL = "http://englishnurturebd.com/";


    public static String PREFS_NAME="math";

    TextView tv_r,tv_batch,tv_fn,tv_nn,tv_fan,tv_sc,tv_mn,tv_mp,tv_s,jan,feb,mar,apr,may,june,july,aug,sep,oct,nov,dec;
    TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8,tv0;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    ExpandableLayout expandableLayout1,expandableLayout0,expandableLayout2;
    GifTextView g1,g2,g3;

    int ijan,ifeb,imar,iapr,imay,ijun,ijul,iaug,isep,ioct,inov,idec = 0;

    String regi_ID_temp="";

    Button update,edit,call;

    ProgressDialog pd;

    String number;

    public static final int MAKE_CALL_PREMISSION_REQ = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_std_details);

        pd = new ProgressDialog(Std_details.this);

        tv_r = (TextView) findViewById(R.id.tv_r);
        tv_batch = (TextView) findViewById(R.id.tv_bt);
        tv_fn = (TextView) findViewById(R.id.tv_fn);
        tv_nn = (TextView) findViewById(R.id.tv_nn);
        tv_fan = (TextView) findViewById(R.id.tv_fan);
        tv_sc = (TextView) findViewById(R.id.tv_sc);
        tv_mn = (TextView) findViewById(R.id.tv_m);
        tv_mp = (TextView) findViewById(R.id.tv_mp);
        tv_s = (TextView) findViewById(R.id.tv_short);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        //tv8 = (TextView) findViewById(R.id.tv8);
        tv0 = (TextView) findViewById(R.id.tv0);

        jan = (TextView) findViewById(R.id.jan);
        feb = (TextView) findViewById(R.id.feb);
        mar = (TextView) findViewById(R.id.mar);
        apr = (TextView) findViewById(R.id.apr);
        may = (TextView) findViewById(R.id.may);
        june = (TextView) findViewById(R.id.jun);
        july = (TextView) findViewById(R.id.jul);
        aug = (TextView) findViewById(R.id.aug);
        sep = (TextView) findViewById(R.id.sep);
        oct = (TextView) findViewById(R.id.oct);
        nov = (TextView) findViewById(R.id.nov);
        dec = (TextView) findViewById(R.id.dec);


        update = (Button) findViewById(R.id.update);
        edit = (Button) findViewById(R.id.edit);
        call = (Button) findViewById(R.id.btn_call);


        g1 = (GifTextView) findViewById(R.id.btn_gif1);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        tv1.setTypeface(type);
        tv2.setTypeface(type);
        tv3.setTypeface(type);
        tv4.setTypeface(type);
        tv5.setTypeface(type);
        tv6.setTypeface(type);
        tv7.setTypeface(type);
        tv_s.setTypeface(type);
        tv0.setTypeface(type);


        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv_batch.setTypeface(type1);
        jan .setTypeface(type1);
        feb.setTypeface(type1);
        mar.setTypeface(type1);
        apr.setTypeface(type1);
        may.setTypeface(type1);
        june.setTypeface(type1);
        july.setTypeface(type1);
        aug.setTypeface(type1);
        sep.setTypeface(type1);
        oct.setTypeface(type1);
        nov.setTypeface(type1);
        dec.setTypeface(type1);
        tv_r.setTypeface(type1);
        tv_batch.setTypeface(type1);
        tv_fn.setTypeface(type1);
        tv_nn.setTypeface(type1);
        tv_fan.setTypeface(type1);
        tv_sc.setTypeface(type1);
        tv_mn.setTypeface(type1);
        tv_mp.setTypeface(type1);
        update.setTypeface(type1);



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),edit_display.class));
            }
        });
        g1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_mp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });






    }


    @Override
    protected void onResume() {
        super.onResume();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        regi_ID_temp = preference.getString("temp_regiID","");

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor;

        cursor=db.rawQuery(" SELECT rowid _id, regi, full_name, nick_name, father_name,school, batch_time , mobile,jan, feb, mar, apr, may, june, july, aug, sep,oct, nov, dec FROM students",null);

        //cursor=db.rawQuery("SELECT rowid _id, * FROM students",null);

        String[] column = null;

        if(cursor.moveToFirst()){

            do{

                if(regi_ID_temp.equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){
                    tv_r.setText(cursor.getString(cursor.getColumnIndexOrThrow("regi")));
                    tv_fn.setText(cursor.getString(cursor.getColumnIndexOrThrow("full_name")));
                    tv_nn.setText(cursor.getString(cursor.getColumnIndexOrThrow("nick_name")));
                    tv_fan.setText(cursor.getString(cursor.getColumnIndexOrThrow("father_name")));
                    tv_batch.setText(cursor.getString(cursor.getColumnIndexOrThrow("batch_time")));
                    tv_sc.setText(cursor.getString(cursor.getColumnIndexOrThrow("school")));
                    tv_mn.setText(cursor.getString(cursor.getColumnIndexOrThrow("mobile")));

                    number = cursor.getString(cursor.getColumnIndexOrThrow("mobile"));

                    String str_f_name = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
                    String str_big_latter;

                    if(str_f_name.equals("")){
                        str_big_latter="";
                    }
                    else {
                        char c = str_f_name.charAt(0);
                        str_big_latter = String.valueOf(c);
                        tv_s.setText(str_big_latter);
                    }

                    if(cursor.getString(cursor.getColumnIndexOrThrow("jan")).equals("1")){
                        jan.setBackgroundResource(R.drawable.border2);
                        jan.setTextColor(Color.rgb(255,255,255));
                        ijan=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("feb")).equals("1")){
                        feb.setBackgroundResource(R.drawable.border2);
                        feb.setTextColor(Color.rgb(255,255,255));
                        ifeb=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("mar")).equals("1")){
                        mar.setBackgroundResource(R.drawable.border2);
                        mar.setTextColor(Color.rgb(255,255,255));
                        imar=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("apr")).equals("1")){
                        apr.setBackgroundResource(R.drawable.border2);
                        apr.setTextColor(Color.rgb(255,255,255));
                        iapr=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("may")).equals("1")){
                        may.setBackgroundResource(R.drawable.border2);
                        may.setTextColor(Color.rgb(255,255,255));
                        imay=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("june")).equals("1")){
                        june.setBackgroundResource(R.drawable.border2);
                        june.setTextColor(Color.rgb(255,255,255));
                        ijun=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("july")).equals("1")){
                        july.setBackgroundResource(R.drawable.border2);
                        july.setTextColor(Color.rgb(255,255,255));
                        ijul=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("aug")).equals("1")){
                        aug.setBackgroundResource(R.drawable.border2);
                        aug.setTextColor(Color.rgb(255,255,255));
                        iaug=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("sep")).equals("1")){
                        sep.setBackgroundResource(R.drawable.border2);
                        sep.setTextColor(Color.rgb(255,255,255));
                        isep=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("oct")).equals("1")){
                        oct.setBackgroundResource(R.drawable.border2);
                        oct.setTextColor(Color.rgb(255,255,255));
                        ioct = 1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("nov")).equals("1")){
                        nov.setBackgroundResource(R.drawable.border2);
                        nov.setTextColor(Color.rgb(255,255,255));
                        inov=1;
                    }
                    if(cursor.getString(cursor.getColumnIndexOrThrow("dec")).equals("1")){
                        dec.setBackgroundResource(R.drawable.border2);
                        dec.setTextColor(Color.rgb(255,255,255));
                        idec = 1;
                    }


                    //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            while(cursor.moveToNext());
        }


        tv_click();



        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pd.setMessage("Updating...");
                pd.show();
                update_db();

            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details

                    ActivityCompat.requestPermissions(Std_details.this, new String[]{Manifest.permission.CALL_PHONE},MAKE_CALL_PREMISSION_REQ);

                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    if(number.charAt(0) == '0'){
                        intent.setData(Uri.parse("tel:" + number));
                    }
                    else {
                        intent.setData(Uri.parse("tel:" + "0" + number));
                    }

                    startActivity(intent);
                }

            }
        });

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MAKE_CALL_PREMISSION_REQ:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    if(number.charAt(0) == '0'){
                        intent.setData(Uri.parse("tel:" + number));
                    }
                    else {
                        intent.setData(Uri.parse("tel:" + "0" + number));
                    }

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                }
                return;
        }
    }
    private void tv_click(){
        jan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ijan == 0){
                    update.setVisibility(View.VISIBLE);
                    jan.setBackgroundResource(R.drawable.border2);
                    jan.setTextColor(Color.rgb(255,255,255));
                    ijan=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    jan.setBackgroundResource(R.drawable.border5);
                    jan.setTextColor(Color.rgb(0,0,0));
                    ijan=0;
                }
            }
        });
        feb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ifeb == 0){
                    update.setVisibility(View.VISIBLE);
                    feb.setBackgroundResource(R.drawable.border2);
                    feb.setTextColor(Color.rgb(255,255,255));
                    ifeb=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    feb.setBackgroundResource(R.drawable.border5);
                    feb.setTextColor(Color.rgb(0,0,0));
                    ifeb=0;
                }
            }
        });
        mar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imar == 0){
                    update.setVisibility(View.VISIBLE);
                    mar.setBackgroundResource(R.drawable.border2);
                    mar.setTextColor(Color.rgb(255,255,255));
                    imar=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    mar.setBackgroundResource(R.drawable.border5);
                    mar.setTextColor(Color.rgb(0,0,0));
                    imar=0;
                }
            }
        });
        apr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(iapr == 0){
                    update.setVisibility(View.VISIBLE);
                    apr.setBackgroundResource(R.drawable.border2);
                    apr.setTextColor(Color.rgb(255,255,255));
                    iapr=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    apr.setBackgroundResource(R.drawable.border5);
                    apr.setTextColor(Color.rgb(0,0,0));
                    iapr=0;
                }
            }
        });
        may.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imay == 0){
                    update.setVisibility(View.VISIBLE);
                    may.setBackgroundResource(R.drawable.border2);
                    may.setTextColor(Color.rgb(255,255,255));
                    imay=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    may.setBackgroundResource(R.drawable.border5);
                    may.setTextColor(Color.rgb(0,0,0));
                    imay=0;
                }
            }
        });
        june.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ijun == 0){
                    update.setVisibility(View.VISIBLE);
                    june.setBackgroundResource(R.drawable.border2);
                    june.setTextColor(Color.rgb(255,255,255));
                    ijun=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    june.setBackgroundResource(R.drawable.border5);
                    june.setTextColor(Color.rgb(0,0,0));
                    ijun=0;
                }
            }
        });
        july.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ijul == 0){
                    update.setVisibility(View.VISIBLE);
                    july.setBackgroundResource(R.drawable.border2);
                    july.setTextColor(Color.rgb(255,255,255));
                    ijul=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    july.setBackgroundResource(R.drawable.border5);
                    july.setTextColor(Color.rgb(0,0,0));
                    ijul=0;
                }
            }
        });
        aug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(iaug == 0){
                    update.setVisibility(View.VISIBLE);
                    aug.setBackgroundResource(R.drawable.border2);
                    aug.setTextColor(Color.rgb(255,255,255));
                    iaug=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    aug.setBackgroundResource(R.drawable.border5);
                    aug.setTextColor(Color.rgb(0,0,0));
                    iaug=0;
                }
            }
        });
        sep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isep == 0){
                    update.setVisibility(View.VISIBLE);
                    sep.setBackgroundResource(R.drawable.border2);
                    sep.setTextColor(Color.rgb(255,255,255));
                    isep=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    sep.setBackgroundResource(R.drawable.border5);
                    sep.setTextColor(Color.rgb(0,0,0));
                    isep=0;
                }
            }
        });
        oct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ioct == 0){
                    update.setVisibility(View.VISIBLE);
                    oct.setBackgroundResource(R.drawable.border2);
                    oct.setTextColor(Color.rgb(255,255,255));
                    ioct=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    oct.setBackgroundResource(R.drawable.border5);
                    oct.setTextColor(Color.rgb(0,0,0));
                    ioct=0;
                }
            }
        });
        nov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inov == 0){
                    update.setVisibility(View.VISIBLE);
                    nov.setBackgroundResource(R.drawable.border2);
                    nov.setTextColor(Color.rgb(255,255,255));
                    inov=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    nov.setBackgroundResource(R.drawable.border5);
                    nov.setTextColor(Color.rgb(0,0,0));
                    inov=0;
                }
            }
        });
        dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(idec == 0){
                    update.setVisibility(View.VISIBLE);
                    dec.setBackgroundResource(R.drawable.border2);
                    dec.setTextColor(Color.rgb(255,255,255));
                    idec=1;
                }
                else{
                    update.setVisibility(View.VISIBLE);
                    dec.setBackgroundResource(R.drawable.border5);
                    dec.setTextColor(Color.rgb(0,0,0));
                    idec=0;
                }
            }
        });

    }

    private void update_SQLite(){

        students std_item = new students();

        std_item.setRegi(regi_ID_temp);
        std_item.setJan(String.valueOf(ijan));
        std_item.setFeb(String.valueOf(ifeb));
        std_item.setMar(String.valueOf(imar));
        std_item.setApr(String.valueOf(iapr));
        std_item.setMay(String.valueOf(imay));
        std_item.setJune(String.valueOf(ijun));
        std_item.setJuly(String.valueOf(ijul));
        std_item.setAug(String.valueOf(iaug));
        std_item.setSep(String.valueOf(isep));
        std_item.setOct(String.valueOf(ioct));
        std_item.setNov(String.valueOf(inov));
        std_item.setDec(String.valueOf(idec));

        db_helper.update_month_payment(std_item);
    }

    private void update_db(){


        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.update_payment api = adapter.create(AppConfig.update_payment.class);

        api.update_pay(
                regi_ID_temp,
                ijan,
                ifeb,
                imar,
                iapr,
                imay,
                ijun,
                ijul,
                iaug,
                isep,
                ioct,
                inov,
                idec,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            update_SQLite();
                            Toast.makeText(Std_details.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(getApplicationContext(),Std_details.class));
                            /*resp = reader.readLine();
                            Log.d("success", "" + resp);

                            JSONObject jObj = new JSONObject(resp);
                            int success = jObj.getInt("success");
*/
                            /*if (success == 1) {
                                Toast.makeText(getApplicationContext(), "Successfully updated", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(),DisplayData.class));
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                            }*/

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            pd.hide();
                            Toast.makeText(Std_details.this, "Please try again", Toast.LENGTH_SHORT).show();
                        } /*catch (JSONException e) {
                            Log.d("JsonException", e.toString());
                        }*/
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.hide();
                        Toast.makeText(Std_details.this, error.toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(Std_details.this, "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
