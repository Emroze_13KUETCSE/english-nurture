package com.roze.english;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;
import com.roze.english.delete.cursor_adapter;
import com.roze.english.helper.AppConfig;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class Dummy extends AppCompatActivity {

    public static String PREFS_NAME="math";

    ResideMenu resideMenu;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    ListView lv;

    String str;

    ProgressDialog pd;

    String BASE_URL = "http://englishnurturebd.com/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy);



        //menu sliding
        pd = new ProgressDialog(Dummy.this);

        // attach to current activity;

        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.back3);
        resideMenu.attachToActivity(this);



        // create menu items;
        String titles[] = { "Home", "Profile", "Security", "Settings" };
        int icon[] = { R.drawable.home, R.drawable.profile, R.drawable.security, R.drawable.setting };

        for (int i = 0; i < titles.length; i++){
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            //item.setOnClickListener((View.OnClickListener) this);

            if(i == 0) {
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        finish();

                    }
                });
            }
            if(i == 2){
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        finish();
                        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                        startActivity(intent);
                    }
                });
            }
            resideMenu.addMenuItem(item,  ResideMenu.DIRECTION_LEFT); // or  ResideMenu.DIRECTION_RIGHT
        }

        //////////////////////////////////////////

        lv=(ListView) findViewById(R.id.list_item);



    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT rowid _id, * FROM students", null);

        cursor_adapter todoAdapter = new cursor_adapter(this, cursor);

        lv.setDivider(null);
        lv.setDividerHeight(0);

        lv.setAdapter(todoAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                LinearLayout l_d = (LinearLayout) view.findViewById(R.id.ll_delete);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll2);
                LinearLayout l3 = (LinearLayout) view.findViewById(R.id.ll3);
                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                TextView delete = (TextView) view.findViewById(R.id.tv_delete);






                str = tvv.getText().toString();

                l_d.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(display.this, str, Toast.LENGTH_SHORT).show();
                    }
                });

                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        new AlertDialog.Builder(Dummy.this)
                                .setTitle("Delete entry")
                                .setMessage("Are you sure you want to delete this entry?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        Toast.makeText(Dummy.this, str, Toast.LENGTH_SHORT).show();

                                        pd.setMessage("Deleting...");
                                        pd.show();

                                        //delete(str);
                                        delete_from_server();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(R.drawable.alert)
                                .show();
                    }
                });
                l2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent inn = new Intent(getApplicationContext(), Std_details.class);
                        startActivity(inn);


                    }
                });
                l3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent inn = new Intent(getApplicationContext(), Std_details.class);
                        startActivity(inn);


                    }
                });


                editor.putString("temp_regiID",str);
                editor.commit();



            }
        });

        SQLite2Excel();


    }

    private void delete_from_sqlite(){
        students std_item = new students();

        std_item.setRegi(str);

        db_helper.delete_std(std_item);
    }
    private void delete_from_server(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        AppConfig.delete_data api = adapter.create(AppConfig.delete_data.class);
        int regi_id = Integer.valueOf(str);

        api.delete_f_server(
                regi_id,
                new Callback<retrofit.client.Response>() {
                    @Override
                    public void success(retrofit.client.Response result, retrofit.client.Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            delete_from_sqlite();
                            Toast.makeText(Dummy.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            pd.hide();
                            finish();
                            //Toast.makeText(display.this, result.toString(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),Dummy.class));


                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            pd.hide();
                            Toast.makeText(Dummy.this, "Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.hide();
                        Toast.makeText(Dummy.this, error.toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(Dummy.this, "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    private void SQLite2Excel(){
        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/GonitNiketan/";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        // Export SQLite DB as EXCEL FILE
        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), db_helper.DATABASE_NAME, directory_path);
        sqliteToExcel.exportAllTables("users.xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onCompleted(String filePath) {
                //Utils.showSnackBar(view, "Successfully Exported");
                Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Exception e) {

            }
        });

      /*  sqliteToExcel.exportSingleTable(db_helper.TABLE_NAME2, "exam.xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }
            @Override
            public void onCompleted(String filePath) {

            }
            @Override
            public void onError(Exception e) {

            }
        });*/
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }
}
