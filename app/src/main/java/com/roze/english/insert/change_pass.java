package com.roze.english.insert;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.R;

public class change_pass extends AppCompatActivity {
    public static String PREFS_NAME="math";

    TextView tv1;
    Button btn;
    EditText et1,et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        tv1 = (TextView) findViewById(R.id.tv1);
        btn = (Button) findViewById(R.id.btn_ok);
        et1 = (EditText) findViewById(R.id.et_oldpass);
        et2 = (EditText) findViewById(R.id.et_newpass);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et1.getText().toString() == "" || et2.getText().toString() == ""){

                }
                else{
                    if(preference.getString("pass","").equals(et1.getText().toString())){
                        editor.putString("pass",et2.getText().toString());
                        editor.commit();
                        Toast.makeText(change_pass.this, "Password changed successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else{
                        Toast.makeText(change_pass.this, "Password doesn't match", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
