package com.roze.english.insert;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.roze.english.R;

public class choose extends AppCompatActivity {
    public static String PREFS_NAME="math";

    TextView tv1,tv2,tv11,tv22,tv00;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        tv1 = (TextView) findViewById(R.id.tv_sr);
        tv2 = (TextView) findViewById(R.id.tv_br);
        tv00 = (TextView) findViewById(R.id.tv1);
        tv11 = (TextView) findViewById(R.id.tv2);
        tv22= (TextView) findViewById(R.id.tv3);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");

        tv11.setTypeface(type);
        tv22.setTypeface(type);
        tv00.setTypeface(type);

        tv1.setTypeface(type1);
        tv2.setTypeface(type1);


        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),insert_std.class));
                editor.putString("batch_wise","false");
                editor.commit();
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),insert_std.class));
                editor.putString("batch_wise","true");
                editor.commit();
            }
        });
    }
}
