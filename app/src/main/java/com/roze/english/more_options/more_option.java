package com.roze.english.more_options;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.roze.english.Dummy;
import com.roze.english.R;
import com.roze.english.delete.delete_data;
import com.roze.english.exam.choose_exam_options;
import com.roze.english.payment.choose_payment;

public class more_option extends AppCompatActivity {

    TextView tv_total,tv_edit,tv_delete,tv_payment,tv0,tv1,tv2,tv3,tv4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_option);

        tv_total = (TextView) findViewById(R.id.tv_t);
        tv_edit = (TextView) findViewById(R.id.tv_e);
        tv_delete = (TextView) findViewById(R.id.tv_d);
        tv_payment = (TextView) findViewById(R.id.tv_p);
        tv0 = (TextView) findViewById(R.id.tv0);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");


        tv0.setTypeface(type);
        tv1.setTypeface(type);
        tv2.setTypeface(type);
        tv3.setTypeface(type);
        tv4.setTypeface(type);


        tv_total.setTypeface(type1);
        tv_edit.setTypeface(type1);
        tv_delete.setTypeface(type1);
        tv_payment.setTypeface(type1);


        tv_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Dummy.class));
            }
        });
        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),choose_exam_options.class));
            }
        });
        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),delete_data.class));
            }
        });
        tv_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),choose_payment.class));
            }
        });

    }
}
