package com.roze.english;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Display_s_by_class_and_batch_listView extends AppCompatActivity {

    public static String PREFS_NAME="math";

    TextView tv1,tv2;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);
    TextView tv_count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_s_by_class_and_batch_list_view);


        tv1 = (TextView) findViewById(R.id.tv_class_name);
        tv2 = (TextView) findViewById(R.id.tv_batch_time);

        lv = (ListView) findViewById(R.id.s_by_class_batch_lv);
        tv_count = (TextView) findViewById(R.id.tv_count);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        editor.putInt("scroll_pos_search",0);
        editor.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor todoCursor = db.rawQuery("SELECT rowid _id, full_name, batch_time, course, school, nick_name FROM students", null);

        Cursor cursor = null;

        String match_class = preference.getString("s_class","").toString();
        String match_batch = preference.getString("s_batch","").toString();




        if(preference.getString("class","").equals("true")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class,null);
            tv1.setText("Class "+match_class);
            tv2.setVisibility(View.INVISIBLE);

        }
        else if(preference.getString("class","").equals("false")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            tv1.setText("Class "+match_class);
            tv2.setText(match_batch);
        }

        tv_count.setText(String.valueOf(cursor.getCount()));

        /*cursor=db.rawQuery("SELECT * FROM students WHERE "
                + "course" + " = " + match_class + " AND " + "batch_time" +
                " LIKE  '"+search.getText()+"%'");*/
// Setup cursor adapter using cursor from last step
        CursorAdaptar_s_by_r_class_and_batch todoAdapter = new CursorAdaptar_s_by_r_class_and_batch(this, cursor);
// Attach cursor adapter to the ListView
        lv.setAdapter(todoAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //list.smoothScrollToPosition(indexx);
                lv.setSelection(preference.getInt("scroll_pos_search",0));
            }
        }, 10);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                String str = tvv.getText().toString();

                editor.putString("temp_regiID",str);
                editor.commit();

                Intent inn = new Intent(getApplicationContext(), Std_details.class);
                startActivity(inn);
                editor.putInt("scroll_pos_search",i);
                editor.commit();
                Toast.makeText(getApplicationContext(), str,
                        Toast.LENGTH_SHORT).show();

            }
        });
    }
}
