package com.roze.english;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;

import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.gigamole.library.ArcProgressStackView;
import com.google.gson.JsonElement;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.roze.english.helper.AppConfig;
import com.roze.english.insert.choose;
import com.roze.english.more_options.more_option;
import com.roze.english.password.lock;
import com.roze.english.send_sms.choose_sms_send;
import com.roze.english.thread.MyWorkerThread;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import pl.droidsonroids.gif.GifTextView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class MainActivity extends AppCompatActivity {
    RequestQueue requestQueue,requestQueue1;

    String BASE_URL = "http://englishnurturebd.com/";
    String showUrl = "http://englishnurturebd.com/jason1.php";
    String showUrl2 = "http://englishnurturebd.com/json.php";
    String showUrl3 = "http://englishnurturebd.com/json1.php";

    public static String PREFS_NAME="math";

    ResideMenu resideMenu;

    public final static int MODEL_COUNT = 4;

    Button up;

    String[] column;

    // Parsed colors
    private int[] mStartColors = new int[MODEL_COUNT];
    private int[] mEndColors = new int[MODEL_COUNT];



    GifTextView gif;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    ProgressDialog pd;

    private static final int REQUEST_CODE_PERMISSION_P = 3;
    private static final int REQUEST_CODE_PERMISSION_SMS = 4;
    private static final int REQUEST_CODE_PERMISSION_STORAGE = 5;
    public final static int REQUEST_CODE = 10101;

    int length=0;

    private List<students1> std;
    private List<students1_marks> std2;
    private List<students1_examlist> std3;

    private String[][] std_table;
    private Integer[][] sort_table;
    int len;

    Thread thread;
    Handler handler;

    ImageView iv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        //menu sliding

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.back);
        //resideMenu.setUse3D(false);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        resideMenu.attachToActivity(this);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        // create menu items;
        String titles[] = { "Home", "Profile", "Security", "Settings" };
        int icon[] = { R.drawable.home, R.drawable.profile, R.drawable.security, R.drawable.setting };


        for (int i = 0; i < titles.length; i++){
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            //item.setOnClickListener((View.OnClickListener) this);

            if(i == 0) {
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        resideMenu.closeMenu();
                    }
                });
            }
            if(i == 2){
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        resideMenu.closeMenu();
                        editor.putString("lock","5");
                        editor.commit();
                        Intent intent = new Intent(getApplicationContext(),lock.class);
                        startActivity(intent);
                    }
                });
            }
            resideMenu.addMenuItem(item,  ResideMenu.DIRECTION_LEFT); // or  ResideMenu.DIRECTION_RIGHT
        }

        iv = (ImageView) findViewById(R.id.imageView) ;
        //////////////////////////////////////////
        // for text design

        TextView text = (TextView)findViewById(R.id.tv_gonit);
        TextView text2 = (TextView)findViewById(R.id.tv_detail);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/font.ttf");

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        Typeface type2 = Typeface.createFromAsset(getAssets(),"fonts/tyrone.ttf");

        text.setTypeface(type2);
        text2.setTypeface(type);

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i=new Intent(MainActivity.this,Dummy.class);
                startActivity(i);*/
            }
        });
        ///////////////////////////////////////////



        //////////////

        // for boom button


        final BoomMenuButton bmb0 = (BoomMenuButton) findViewById(R.id.bmb);
        assert bmb0 != null;
        bmb0.setButtonEnum(ButtonEnum.Ham);
        bmb0.setPiecePlaceEnum(PiecePlaceEnum.HAM_4);
        bmb0.setButtonPlaceEnum(ButtonPlaceEnum.HAM_4);
        for (int i = 0; i < bmb0.getPiecePlaceEnum().pieceNumber(); i++){
            bmb0.addBuilder(BuilderManager.getHamButtonBuilder0());

        }

        bmb0.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });

        bmb0.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {

                if(index == 0){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if(preference.getString("search_on","").equals("true")){
                                editor.putString("lock","1");
                                editor.commit();
                                Intent i=new Intent(MainActivity.this,lock.class);
                                startActivity(i);
                            }
                            else{

                                Intent i=new Intent(MainActivity.this,search.class);
                                startActivity(i);
                            }



                        }
                    }, 950);

                }
                else if(index == 1){

                    //
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(preference.getString("sms_on","").equals("true")) {
                                editor.putString("lock", "2");
                                editor.commit();
                                Intent i = new Intent(MainActivity.this, lock.class);
                                startActivity(i);
                            }
                            else{
                                Intent i=new Intent(MainActivity.this,choose_sms_send.class);
                                startActivity(i);
                            }

                        }
                    }, 950);

                }
                else if(index == 2){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(preference.getString("setti_on","").equals("true")) {
                                editor.putString("lock", "3");
                                editor.commit();
                                Intent i = new Intent(MainActivity.this, lock.class);
                                startActivity(i);
                            }
                            else{
                                Intent i=new Intent(MainActivity.this,more_option.class);
                                startActivity(i);
                            }
                        }
                    }, 950);

                }
                else if(index == 3){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(preference.getString("regi_on","").equals("true")) {
                                editor.putString("lock", "4");
                                editor.commit();
                                Intent i = new Intent(MainActivity.this, lock.class);
                                startActivity(i);
                            }
                            else{
                                Intent i=new Intent(MainActivity.this,choose.class);
                                startActivity(i);
                            }
                        }
                    }, 950);

                }

            }

            @Override
            public void onBackgroundClick() {


            }

            @Override
            public void onBoomWillHide() {

            }

            @Override
            public void onBoomDidHide() {
                //iv1.setVisibility(View.VISIBLE);
                iv.setVisibility(View.VISIBLE);

            }

            @Override
            public void onBoomWillShow() {

                //iv1.setVisibility(View.INVISIBLE);
                iv.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onBoomDidShow() {

            }
        });

        // for gif
        gif = (GifTextView) findViewById(R.id.gif1);

        gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });


        ////////////////////////////////////////////

        // for jason parsing
        //result = (TextView) findViewById(R.id.textView);




        Calendar cal= Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("M");
        String month_name = month_date.format(cal.getTime());

        //Toast.makeText(MainActivity.this, month_name, Toast.LENGTH_SHORT).show();






        if (Build.VERSION.SDK_INT >= 23) {
            String mPermission_SMS = Manifest.permission.SEND_SMS;
            String mPermission_p = Manifest.permission.CALL_PHONE;
            String mP = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            boolean s=false;

            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS},REQUEST_CODE_PERMISSION_P);

            }
            /*try {


                if (ActivityCompat.checkSelfPermission(this, mPermission_p)
                        != MockPackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{mPermission_p}, REQUEST_CODE_PERMISSION_P);

                    // If any permission above not allowed by user, this condition will execute every time, else your else part will work
                }
                if (ActivityCompat.checkSelfPermission(this, mPermission_SMS)
                        != MockPackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{mPermission_SMS}, REQUEST_CODE_PERMISSION_SMS);

                    // If any permission above not allowed by user, this condition will execute every time, else your else part will work
                }
                if (ActivityCompat.checkSelfPermission(this, mP)
                        != MockPackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{mP}, REQUEST_CODE_PERMISSION_STORAGE);

                    // If any permission above not allowed by user, this condition will execute every time, else your else part will work
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }

        pd = new ProgressDialog(MainActivity.this);
        pd.setCanceledOnTouchOutside(false);

        thread = new Thread(new WokerThread());

        up = (Button)findViewById(R.id.btn_update);
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //up1();
                //up3();
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Updating...");
                pd.show();
                thread.start();

            }
        });



    }


    class WokerThread implements Runnable{

        @Override
        public void run() {
            get_data();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        final String[] bgColors = getResources().getStringArray(R.array.medical_express);

        // Set models

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("pass",null) == null){
            editor.putString("pass","azad");
            editor.putString("search_on","true");
            editor.putString("sms_on","true");
            editor.putString("setti_on","true");
            editor.putString("regi_on","true ");
            editor.putString("changed_index","0");

            editor.commit();
        }
        if(haveNetworkConnection()){
            if(preference.getString("first",null)==null) {
                //up1();
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Updating...");
                pd.show();
                get_data();
                editor.putString("first","1");
                editor.commit();


            }
            else{

            }
        }
        else{

        }


    }


    private void up1(){

        pd.setMessage("Updating 2...");
        pd.show();
        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        //Creating an object of our api interface
        AppConfig.get_students_markslist api = adapter.create(AppConfig.get_students_markslist.class);

        //Defining the method
        api.getStudentsMarksList(new Callback<List<students1_examlist>>() {

            @Override
            public void success(List<students1_examlist> studentses, retrofit.client.Response response) {

                std3 = studentses;

                int len = std3.size();

                for(int i =0; i < len ; i++){
                    Toast.makeText(MainActivity.this,std3.get(i).getExam_date(), Toast.LENGTH_SHORT).show();
                    db_helper.insert_into_marks_list(std3.get(i).getExam_name(),std3.get(i).getExam_date(),std3.get(i).getExam_class(),std3.get(i).getExam_batch());
                }

                //pd.hide();
                Toast.makeText(MainActivity.this, "Updated Marklist table "+len, Toast.LENGTH_SHORT).show();
                up2();
            }

            @Override
            public void failure(RetrofitError error) {
                //you can handle the errors here
                //pd.hide();
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                Log.e("here1",error.toString());
            }
        });


        /*requestQueue1 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest1 = new JsonObjectRequest(Request.Method.POST,
                showUrl3, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i < std.length(); i++) {
                        JSONObject student = std.getJSONObject(i);

                        db_helper.insert_into_marks_list(student.getString("exam_name"),student.getString("exam_date"));

                        //result.append(regi + " " + batch_time + " " + s_nick +" "+school+" \n");
                    }
                    //result.append("===\n");
                    String std_lenght = String.valueOf(std.length());
                    length = std.length();

                    //result.append(std+" \n");

                    Toast.makeText(MainActivity.this, "Updated Marklist table "+std_lenght, Toast.LENGTH_SHORT).show();

                    up2();
                } catch (JSONException e) {

                    pd.hide();
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Please try again1", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());

                pd.hide();
                Toast.makeText(MainActivity.this, "No internet cxonnection", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue1.add(jsonObjectRequest1);*/


    }

    private void up2(){

        for(int i =0; i < len ; i++){
            students std_item = new students();

            std_item.setRegi(std_table[i][0]);
            std_item.setBatch_time(std_table[i][1]);
            std_item.setCourse(std_table[i][6]);

            db_helper.insert_marks_table(std_item);

            //Toast.makeText(MainActivity.this, std_table[i][0], Toast.LENGTH_SHORT).show();
        }

        /*//Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        //Creating an object of our api interface
        AppConfig.get_students_marks api = adapter.create(AppConfig.get_students_marks.class);

        //Defining the method
        api.getStudentsMarks(new Callback<List<students1_marks>>() {

            @Override
            public void success(List<students1_marks> studentses, retrofit.client.Response response) {

                std2 = studentses;

                int len = std2.size();

                for(int i =0; i < len ; i++){
                    students std_item2 = new students();

                    std_item2.setRegi(std2.get(i).getRegi());
                    std_item2.setBatch_time(std2.get(i).getBatch_time());
                    std_item2.setCourse(std2.get(i).getCourse());

                    db_helper.insert_marks_table(std_item2);
                }

                pd.hide();
            }

            @Override
            public void failure(RetrofitError error) {
                //you can handle the errors here
                pd.hide();
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                Log.e("here1",error.toString());
            }
        });*/


        /*requestQueue2 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest1 = new JsonObjectRequest(Request.Method.POST,
                showUrl2, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i < std.length(); i++) {
                        JSONObject student = std.getJSONObject(i);

                        students std_item = new students();

                        std_item.setRegi(student.getString("regi"));
                        std_item.setBatch_time(student.getString("batch_time"));
                        std_item.setCourse(student.getString("course"));

                        db_helper.insert_marks_table(std_item);

                    }

                } catch (JSONException e) {
                    pd.hide();
                    e.printStackTrace();
                    Log.e("error1 ",e.toString());
                    Toast.makeText(MainActivity.this, "Please try again1", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(MainActivity.this, "No internet cxonnection", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue1.add(jsonObjectRequest1);*/

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        SQLiteDatabase db = db_helper.getWritableDatabase();

        final Cursor cursor;

        cursor=db.rawQuery("SELECT rowid _id, * FROM exam_list",null);

        //Toast.makeText(this,String.valueOf(cursor.getCount()), Toast.LENGTH_SHORT).show();

        column = new String[cursor.getCount()];
        int ii =0;
        if(preference.getString("first11",null)==null) {
            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        db_helper.alter_marks_table(cursor.getString(cursor.getColumnIndexOrThrow("exam_name")));
                        column[ii] = cursor.getString(cursor.getColumnIndexOrThrow("exam_name"));
                        ii++;
                        //Toast.makeText(this, cursor.getString(cursor.getColumnIndexOrThrow("exam_date")), Toast.LENGTH_SHORT).show();
                    } while (cursor.moveToNext());
                }
            }
            editor.putString("first11","a");
            editor.commit();

        }
        else{
            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        if(db_helper.check_alter_marks_table(cursor.getString(cursor.getColumnIndexOrThrow("exam_name")))) {
                            db_helper.alter_marks_table(cursor.getString(cursor.getColumnIndexOrThrow("exam_name")));
                            column[ii] = cursor.getString(cursor.getColumnIndexOrThrow("exam_name"));
                            ii++;
                            //Toast.makeText(this, cursor.getString(cursor.getColumnIndexOrThrow("exam_date")), Toast.LENGTH_SHORT).show();
                        }
                    } while (cursor.moveToNext());
                }
            }
        }

        //Creating a rest adapter
        RestAdapter adapter1 = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        //Creating an object of our api interface
        AppConfig.get_students_marks api1 = adapter1.create(AppConfig.get_students_marks.class);

        //Defining the method
        api1.getStudentsMarks(new Callback<List<students1_marks>>() {

            @Override
            public void success(List<students1_marks> studentses, retrofit.client.Response response) {

                std2 = studentses;

                int len = std2.size();

                for(int i =0; i < len ; i++){
                    students std_item2 = new students();

                    std_item2.setRegi(std2.get(i).getRegi());
                    /*if (cursor.getCount() != 0) {
                        for (int j = 0; j < cursor.getCount(); j++) {
                            *//*try{
                                db_helper.update_marks_table(student.getString("regi"),student.getString(column[j]),column[j]);
                            }
                            catch (NullPointerException e){
                                Toast.makeText(MainActivity.this, "null "+String.valueOf(j), Toast.LENGTH_SHORT).show();
                            }*//*
                            //Toast.makeText(MainActivity.this, column[], Toast.LENGTH_SHORT).show();
                            db_helper.update_marks_table(std_item2.setRegi(std2.get(i).getRegi()), student.getString(column[j]), column[j]);
                        }
                    }*/
                }

            }

            @Override
            public void failure(RetrofitError error) {
                //you can handle the errors here
                //pd.hide();
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                Log.e("here1",error.toString());
            }
        });

        requestQueue1 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST,
                showUrl2, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i < std.length(); i++) {
                        JSONObject student = std.getJSONObject(i);

                        students std_item = new students();

                        if (cursor.getCount() != 0) {
                            for (int j = 0; j < cursor.getCount(); j++) {

                                db_helper.update_marks_table(student.getString("regi"), student.getString(column[j]), column[j]);
                            }
                        }
                        pd.hide();
                    }

                } catch (JSONException e) {
                    pd.hide();
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Please try again1", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(MainActivity.this, "No internet cxonnection", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue1.add(jsonObjectRequest2);

        //  SQLite2Excel();


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == REQUEST_CODE_PERMISSION_SMS) {

        }

        if (requestCode == REQUEST_CODE_PERMISSION_P) {

        }
        if (requestCode == REQUEST_CODE) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(this)) {

                }
            }
        }

    }


    private void SQLite2Excel(){
        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/GonitNiketan/";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        // Export SQLite DB as EXCEL FILE
        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), db_helper.DATABASE_NAME, directory_path);

        sqliteToExcel.exportAllTables("users.xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onCompleted(String filePath) {
                //Utils.showSnackBar(view, "Successfully Exported");
                Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Exception e) {

            }
        });

      /*  sqliteToExcel.exportSingleTable(db_helper.TABLE_NAME2, "exam.xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }
            @Override
            public void onCompleted(String filePath) {

            }
            @Override
            public void onError(Exception e) {

            }
        });*/
    }
    /*@Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }*/

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void up3(){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                showUrl, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i < std.length(); i++) {
                        JSONObject student = std.getJSONObject(i);

                        students std_item = new students();

                        std_item.setRegi(student.getString("regi"));
                        std_item.setFull_name(student.getString("s_full"));
                        std_item.setNick_name(student.getString("s_nick"));
                        std_item.setFa_name(student.getString("father_name"));
                        std_item.setSchool(student.getString("school"));
                        std_item.setBatch_time(student.getString("batch_time"));
                        std_item.setMobile(student.getString("fa_contact"));
                        std_item.setCourse(student.getString("course"));
                        std_item.setJan(student.getString("jan"));
                        std_item.setFeb(student.getString("feb"));
                        std_item.setMar(student.getString("mar"));
                        std_item.setApr(student.getString("apr"));
                        std_item.setMay(student.getString("may"));
                        std_item.setJune(student.getString("june"));
                        std_item.setJuly(student.getString("july"));
                        std_item.setAug(student.getString("aug"));
                        std_item.setSep(student.getString("sep"));
                        std_item.setOct(student.getString("oct"));
                        std_item.setNov(student.getString("nov"));
                        std_item.setDec(student.getString("dec"));

                        //db_helper.insertStudents(std_item);

                        //result.append(regi + " " + batch_time + " " + s_nick +" "+school+" \n");
                    }
                    //result.append("===\n");
                    String std_lenght = String.valueOf(std.length());
                    //result.append(std+" \n");
                    pd.hide();
                    Toast.makeText(MainActivity.this, "Module 1 Updated students "+std_lenght, Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    pd.hide();

                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(MainActivity.this, "No internet cxonnection", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);

    }


    public void get_data(){


        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        //Creating an object of our api interface
        AppConfig.get_students api = adapter.create(AppConfig.get_students.class);

        //Defining the method
        api.getStudents(new Callback<List<students1>>() {

            @Override
            public void success(List<students1> studentses, retrofit.client.Response response) {

                std = studentses;

                len = std.size();

                std_table = new String[len][20];

                for(int i =0; i < len ; i++){

                    std_table[i][0] = std.get(i).getRegi();
                    std_table[i][1] = std.get(i).getBatch_time();
                    std_table[i][2] = std.get(i).getS_nick();
                    std_table[i][3] = std.get(i).getS_full();
                    std_table[i][4] = std.get(i).getFather_name();
                    std_table[i][5] = std.get(i).getSchool();
                    std_table[i][6] = std.get(i).getCourse();
                    std_table[i][7] = std.get(i).getFa_contact();
                    std_table[i][8] = std.get(i).getJan();
                    std_table[i][9] = std.get(i).getFeb();
                    std_table[i][10] = std.get(i).getMar();
                    std_table[i][11] = std.get(i).getApr();
                    std_table[i][12] = std.get(i).getMay();
                    std_table[i][13] = std.get(i).getJune();
                    std_table[i][14] = std.get(i).getJuly();
                    std_table[i][15] = std.get(i).getAug();
                    std_table[i][16] = std.get(i).getSep();
                    std_table[i][17] = std.get(i).getOct();
                    std_table[i][18] = std.get(i).getNov();
                    std_table[i][19] = std.get(i).getDec();


                    //Toast.makeText(MainActivity.this, std_table[i][0], Toast.LENGTH_SHORT).show();


                }


                SQLiteDatabase db = db_helper.getWritableDatabase();

                final Cursor cursor;

                cursor=db.rawQuery("SELECT rowid _id, * FROM students",null);

                if(cursor.getCount() < std_table.length){
                    //array sorting
                    Arrays.sort(std_table, new Comparator<String[]>() {
                        @Override
                        public int compare(final String[] entry1, final String[] entry2) {
                            final String time1 = entry1[0];
                            final String time2 = entry2[0];
                            return time1.compareTo(time2);
                        }
                    });

                    for(int i =0; i < len ; i++){
                        students std_item = new students();

                        std_item.setRegi(std_table[i][0]);
                        std_item.setBatch_time(std_table[i][1]);
                        std_item.setNick_name(std_table[i][2]);
                        std_item.setFull_name(std_table[i][3]);
                        std_item.setFa_name(std_table[i][4]);
                        std_item.setSchool(std_table[i][5]);
                        std_item.setCourse(std_table[i][6]);
                        std_item.setMobile(std_table[i][7]);
                        std_item.setJan(std_table[i][8]);
                        std_item.setFeb(std_table[i][9]);
                        std_item.setMar(std_table[i][10]);
                        std_item.setApr(std_table[i][11]);
                        std_item.setMay(std_table[i][12]);
                        std_item.setJune(std_table[i][13]);
                        std_item.setJuly(std_table[i][14]);
                        std_item.setAug(std_table[i][15]);
                        std_item.setSep(std_table[i][16]);
                        std_item.setOct(std_table[i][17]);
                        std_item.setNov(std_table[i][18]);
                        std_item.setDec(std_table[i][19]);

                        db_helper.insertStudents(std_item);
                        System.out.println(std_table[i][0]);

                        //Toast.makeText(MainActivity.this, std_table[i][0], Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(MainActivity.this, std_table[i][0], Toast.LENGTH_SHORT).show();
                }

                //Toast.makeText(MainActivity.this, "Updated Students "+len, Toast.LENGTH_SHORT).show();
                //up1();
                pd.hide();
                thread.interrupt();

            }

            @Override
            public void failure(RetrofitError error) {
                //you can handle the errors here
                pd.hide();
                //Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                Log.e("here1",error.toString());
            }
        });


    }



    public void displayData() {

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.read api = adapter.create(AppConfig.read.class);

        api.readData(new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, retrofit.client.Response response) {
                String myResponse = jsonElement.toString();

                try {
                    JSONObject jObj = new JSONObject(myResponse);

                    int success = jObj.getInt("success");

                    /*if (success == 1) {*/

                        JSONArray jsonArray = jObj.getJSONArray("students");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject student = jsonArray.getJSONObject(i);

                            students std_item = new students();

                            std_item.setRegi(student.getString("regi"));
                            std_item.setFull_name(student.getString("s_full"));
                            std_item.setNick_name(student.getString("s_nick"));
                            std_item.setFa_name(student.getString("father_name"));
                            std_item.setSchool(student.getString("school"));
                            std_item.setBatch_time(student.getString("batch_time"));
                            std_item.setMobile(student.getString("fa_contact"));
                            std_item.setCourse(student.getString("course"));
                            std_item.setJan(student.getString("jan"));
                            std_item.setFeb(student.getString("feb"));
                            std_item.setMar(student.getString("mar"));
                            std_item.setApr(student.getString("apr"));
                            std_item.setMay(student.getString("may"));
                            std_item.setJune(student.getString("june"));
                            std_item.setJuly(student.getString("july"));
                            std_item.setAug(student.getString("aug"));
                            std_item.setSep(student.getString("sep"));
                            std_item.setOct(student.getString("oct"));
                            std_item.setNov(student.getString("nov"));
                            std_item.setDec(student.getString("dec"));

                            //db_helper.insertStudents(std_item);

                        }

/*

                    } else {
                        Toast.makeText(getApplicationContext(), "No Details Found", Toast.LENGTH_SHORT).show();
                    }*/
                } catch (JSONException error) {
                    Log.e("YOUR_APP_LOG_TAG", " ", error);
                    //Log.e("exception", e.toString());
                }

                Toast.makeText(MainActivity.this, "Module 3 Updated students "+length, Toast.LENGTH_SHORT).show();
                pd.hide();
            }
            @Override
            public void failure(RetrofitError error) {
                Log.e("YOUR_APP_LOG_TAG", " ", error);
                //Log.e("Failure", error.toString());
                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        );
    }
}
