package com.roze.english.exam.exam_mark_list;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.roze.english.R;
import com.roze.english.exam.preview.ListViewAdapter;
import com.roze.english.exam.preview.total_data;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class display_marks extends AppCompatActivity {

    TextView tv_ex_name,tv_ex_class,tv_ex_batch,tv_ex_date,tv_highest_marks,tv_ex_h_regi;

    private String ex_name;
    private String ex_class;
    private String ex_batch;
    private String ex_date;

    public static String PREFS_NAME="math";
    ListView list;
    ListViewAdapter listviewadapter;
    List<total_data> worldpopulationlist = new ArrayList<total_data>();
    String[] name;
    String[] regi;
    String[] marks;
    String[] temp_regi;
    String[] temp_marks;
    int total=0;
    int non_zero = 0;
    String[] str_big_latter;

    int change_index=0;
    int indexx = 0;

    Button btn_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_marks);

        tv_ex_batch = (TextView) findViewById(R.id.tv_ex_batch);
        tv_ex_name = (TextView) findViewById(R.id.tv_ex_name);
        tv_ex_class = (TextView) findViewById(R.id.tv_ex_class);
        tv_ex_date = (TextView) findViewById(R.id.tv_ex_date);
        tv_highest_marks = (TextView) findViewById(R.id.tv_highest_marks);
        tv_ex_h_regi = (TextView) findViewById(R.id.tv_ex_h_regi);
        btn_edit = (Button) findViewById(R.id.btn_edit);


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");

        tv_ex_batch.setTypeface(type1);
        tv_ex_h_regi.setTypeface(type1);
        tv_ex_date.setTypeface(type1);
        tv_ex_name.setTypeface(type);
        tv_ex_class.setTypeface(type1);
        tv_highest_marks.setTypeface(type);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        ex_class = preference.getString("s_class","").toString();
        ex_batch = preference.getString("s_batch","").toString();
        ex_date = preference.getString("s_date","").toString();
        ex_name = preference.getString("ex_name_a","").toString();
        change_index = Integer.valueOf(preference.getString("changed_index",""));

        String temp_ex_name="";
        for(int i = 0; i < ex_name.length();i++){
            if(ex_name.charAt(i) == 's' && ex_name.charAt(i+1) == 'k'){
                break;
            }
            temp_ex_name = temp_ex_name.concat(String.valueOf(ex_name.charAt(i)));
        }

        if(ex_class.equals("1")){
            tv_ex_class.setText("Class: 10");
        }
        else {
            tv_ex_class.setText("Class: "+ex_class);
        }
        tv_ex_batch.setText("Batch: "+ex_batch);
        tv_ex_name.setText("Exam Number: "+temp_ex_name);
        tv_ex_date.setText("Exam Date: "+ex_date);

        Intent intent = getIntent();
        total = intent.getIntExtra("total",0);
        name = new String[total];
        temp_regi = new String[total];
        temp_marks = new String[total];
        str_big_latter = new String[total];
        name = intent.getStringArrayExtra("name");
        temp_regi = intent.getStringArrayExtra("regi");
        temp_marks = intent.getStringArrayExtra("marks");
        indexx = intent.getIntExtra("scroll_index",0);


        //Toast.makeText(this, String.valueOf(marrrks[i]), Toast.LENGTH_SHORT).show();
        for(int j = 0; j < temp_regi.length;j++){
            if(!temp_regi.equals("-1")){
                non_zero++;
            }
        }

        for(int k = 0; k < name.length; k++){
            if(name[k].equals("")){
                str_big_latter[k]="";
            }
            else {
                char c = name[k].charAt(0);
                str_big_latter[k] = String.valueOf(c);
            }
        }

        regi = new String[temp_regi.length];
        marks = new String[temp_regi.length];
        int b =0;
        int c = 0;

        for(int a = 0 ; a < temp_marks.length; a++){
            try{
                if(temp_marks[a].equals("-1") || temp_marks[a] == null){
                    marks[b] = "";
                    b++;
                }
                else {
                    marks[b] = temp_marks[a];
                    b++;
                }
            }
            catch (NullPointerException e){
                marks[b] = "";
                b++;
            }

            if(!temp_regi[a].equals("-1")){
                regi[c] = temp_regi[a];
                c++;
            }

        }


        float[] marrrks = new float[temp_marks.length];
        for(int i = 0; i < temp_marks.length; i++){
            try{
                if(!temp_marks[i].equals("-1") || !temp_marks[i].equals("")){
                    try{
                        marrrks[i] = Float.valueOf(temp_marks[i]);
                        //Toast.makeText(this, temp_marks[i], Toast.LENGTH_SHORT).show();
                    }
                    catch (NumberFormatException e){
                        marrrks[i] = -1;
                        //Toast.makeText(this, "rr"+temp_marks[i], Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    marrrks[i] = -1;
                    //Toast.makeText(this, "rrr"+temp_marks[i], Toast.LENGTH_SHORT).show();
                }
            }
            catch (NullPointerException e){
                marrrks[i] = -1;
            }

        }


        int[] indeex = new int[marrrks.length];
        for(int i =0; i < marrrks.length;i++){
            indeex[i] = -1;

        }

        float temp = marrrks[0];
        int jj = 0;
        for(int i = 0; i < marrrks.length; i++){
            if(marrrks[i] >= temp){
                if(marrrks[i] == temp){
                    indeex[jj] = i;
                    jj++;
                }
                else if(marrrks[i] > temp){
                    for(int ii =0; ii < marrrks.length;ii++){
                        indeex[ii] = -1;
                    }
                    jj=0;
                    temp = marrrks[i];
                    indeex[jj] = i;
                    jj++;
                }
            }

        }

        String indii="";

        for(int i = 0; i < indeex.length;i++){
            if(indeex[i] != -1){
                indii=indii.concat(String.valueOf(regi[indeex[i]])+" ");
            }
        }


        if(indii.length() > 50){
            String temppp="";
            for(int i =0; i < 50; i++){
                temppp = temppp.concat(String.valueOf(indii.charAt(i)));
            }
            temppp = temppp+"...";
            tv_ex_h_regi.setText(temppp);

        }else{
            tv_ex_h_regi.setText(indii);
        }


        final String finalIndii = indii;
        tv_highest_marks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(display_marks.this, SweetAlertDialog.NORMAL_TYPE)

                        .setTitleText("Highest Marks Regi :")
                        .setContentText(finalIndii)
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        tv_ex_h_regi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(display_marks.this, SweetAlertDialog.NORMAL_TYPE)

                        .setTitleText("Highest Marks Regi :")
                        .setContentText(finalIndii)
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });

        tv_highest_marks.setText("Highest Marks: "+String.valueOf(temp));


        for (int i = 0; i < regi.length; i++) {
            total_data worldpopulation = new total_data(str_big_latter[i],
                    name[i], regi[i], marks[i]);
            worldpopulationlist.add(worldpopulation);
        }

        // Locate the ListView in listview_main.xml
        list = (ListView) findViewById(R.id.listview);



        // Pass results to ListViewAdapter Class
        listviewadapter = new ListViewAdapter(this, R.layout.s_by_temp_preview_list,
                worldpopulationlist);

        // Binds the Adapter to the ListView
        list.setAdapter((ListAdapter) listviewadapter);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //list.smoothScrollToPosition(indexx);
                list.setSelection(indexx);
            }
        }, 10);


        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(),edit_marks.class);
                intent.putExtra("marks",marks);
                startActivity(intent);
                finish();
            }
        });

        final String finalTemp_ex_name = temp_ex_name;
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv_name = (TextView) view.findViewById(R.id.textView_std_name);
                TextView regii = (TextView) view.findViewById(R.id.textView_regi);
                TextView markss = (TextView) view.findViewById(R.id.tv_marks);

                String t_name = tv_name.getText().toString();
                String t_regi = regii.getText().toString();
                String t_marks = markss.getText().toString();

                Intent intent = new Intent(getApplicationContext(),marks_update.class);
                intent.putExtra("name", t_name);
                intent.putExtra("regi",t_regi);
                intent.putExtra("marks",t_marks);
                intent.putExtra("full_ex_name", ex_name);
                intent.putExtra("ex_name", finalTemp_ex_name);
                intent.putExtra("class",ex_class);
                intent.putExtra("date",ex_date);
                intent.putExtra("batch",ex_batch);
                intent.putExtra("total",total);
                intent.putExtra("name_array", name);
                intent.putExtra("regi_array",regi);
                intent.putExtra("marks_array",marks);
                intent.putExtra("total",total);
                intent.putExtra("index",i);

                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}
