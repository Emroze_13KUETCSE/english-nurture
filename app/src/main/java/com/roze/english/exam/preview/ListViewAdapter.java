package com.roze.english.exam.preview;

/*
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.roze.math.R;

import java.util.List;

*/
/**
 * Created by emroz on 8/29/2017.
 *//*


public class ListViewAdapter {
    Context context;
    LayoutInflater inflater;
    List<total_data> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapter(Context context, int resourceId,
                           List<total_data> worldpopulationlist) {
        super();
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView name;
        TextView regi;
        TextView marks;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.s_by_temp_preview_list, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.textView_std_name);
            holder.regi = (TextView) view.findViewById(R.id.textView_regi);
            holder.marks = (TextView) view.findViewById(R.id.tv_marks);
            // Locate the ImageView in listview_item.xml
            //holder.flag = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Capture position and set to the TextViews
        holder.name.setText(worldpopulationlist.get(position).getName());
        holder.regi.setText(worldpopulationlist.get(position).getRegi());
        holder.marks.setText(worldpopulationlist.get(position)
                .getMarks());
        // Capture position and set to the ImageView
        //holder.flag.setChecked(worldpopulationlist.get(position).getFlag());
        return view;
    }


    public List<total_data> getWorldPopulation() {
        return worldpopulationlist;
    }



    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
*/
import android.graphics.Typeface;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.roze.english.R;

/**
 * Created by emroz on 8/27/2017.
 */

public class ListViewAdapter extends ArrayAdapter<total_data> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<total_data> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapter(Context context, int resourceId,
                           List<total_data> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView big;
        TextView name;
        TextView regi;
        TextView marks;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.s_by_temp_preview_list, null);
            // Locate the TextViews in listview_item.xml
            holder.big = (TextView) view.findViewById(R.id.tv_big_l);
            holder.name = (TextView) view.findViewById(R.id.textView_std_name);
            holder.regi = (TextView) view.findViewById(R.id.textView_regi);
            holder.marks = (TextView) view.findViewById(R.id.tv_marks);

            Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

            holder.big.setTypeface(type);
            holder.name.setTypeface(type);
            holder.marks.setTypeface(type);
            holder.regi.setTypeface(type1);

            // Locate the ImageView in listview_item.xml
            //holder.flag = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Capture position and set to the TextViews
        holder.big.setText(worldpopulationlist.get(position).getBig_latter());
        holder.name.setText(worldpopulationlist.get(position).getName());
        holder.regi.setText(worldpopulationlist.get(position).getRegi());
        holder.marks.setText(worldpopulationlist.get(position).getMarks());
        // Capture position and set to the ImageView
        //holder.flag.setChecked(worldpopulationlist.get(position).getFlag());
        return view;
    }


    public List<total_data> getWorldPopulation() {
        return worldpopulationlist;
    }



    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}