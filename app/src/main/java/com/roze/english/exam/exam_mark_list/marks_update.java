package com.roze.english.exam.exam_mark_list;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.helper.AppConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class marks_update extends AppCompatActivity {
    String BASE_URL = "http://englishnurturebd.com/";
    public static String PREFS_NAME="math";

    DatabaseHelper db_helper;
    ProgressDialog pd;

    String[] name_array;
    String[] regi_array;
    String[] marks_array;
    int total=0;
    int index = 0;

    TextView tv_ex_name,tv_class,tv_batch,tv_date,tv_name,tv_regi;
    EditText et_marks;
    Button btn_ok;
    String name,ex_name,date,batch,ex_class,regi,marks,f_ex_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marks_update);

        pd = new ProgressDialog(marks_update.this);

        tv_ex_name = (TextView) findViewById(R.id.tv_ex_name);
        tv_class= (TextView) findViewById(R.id.tv_ex_class);
        tv_batch = (TextView) findViewById(R.id.tv_ex_batch);
        tv_date = (TextView) findViewById(R.id.tv_ex_date);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_regi = (TextView) findViewById(R.id.tv_regi);

        et_marks = (EditText) findViewById(R.id.et_marks);

        btn_ok =(Button) findViewById(R.id.btn_ok);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");

        tv_ex_name.setTypeface(type);
        tv_class.setTypeface(type1);
        tv_batch.setTypeface(type1);
        tv_date.setTypeface(type);
        tv_name.setTypeface(type);
        tv_regi.setTypeface(type1);
        et_marks.setTypeface(type1);
        btn_ok.setTypeface(type1);


        Intent intent = getIntent();
        ex_name = intent.getStringExtra("ex_name");
        ex_class = intent.getStringExtra("class");
        date = intent.getStringExtra("date");
        batch = intent.getStringExtra("batch");
        marks = intent.getStringExtra("marks");
        regi = intent.getStringExtra("regi");
        name = intent.getStringExtra("name");
        f_ex_name = intent.getStringExtra("full_ex_name");
        total = intent.getIntExtra("total",0);
        index = intent.getIntExtra("index",0);

        name_array = new String[total];
        regi_array = new String[total];
        marks_array = new String[total];

        name_array = intent.getStringArrayExtra("name_array");
        regi_array = intent.getStringArrayExtra("regi_array");
        marks_array = intent.getStringArrayExtra("marks_array");



        tv_ex_name.setText(ex_name);
        tv_class.setText(ex_class);
        tv_batch.setText(batch);
        tv_date.setText(date);
        tv_name.setText(name);
        tv_regi.setText(regi);
        et_marks.setText(marks);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setMessage("Updating marks...");
                pd.show();
                String mm = et_marks.getText().toString();
                update_marks(regi,mm,f_ex_name);

            }
        });


    }

    private void update_marks_sqlite(String regi22, String marks22, String exam_name22){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        db_helper = new DatabaseHelper(this);
        final SQLiteDatabase db = db_helper.getReadableDatabase();

        db_helper.update_exam_marks(regi22,marks22,exam_name22);
        pd.hide();
        marks_array[index] = marks22;

        editor.putString("changed_index",String.valueOf(index));
        editor.commit();
        finish();

        Intent intent = new Intent(getApplicationContext(),display_marks.class);
        intent.putExtra("total",total);
        intent.putExtra("name", name_array);
        intent.putExtra("regi",regi_array);
        intent.putExtra("marks",marks_array);
        intent.putExtra("scroll_index",index);
        startActivity(intent);

        //startActivity(new Intent(getApplicationContext(),choose_c_b_t.class));
    }
    private void update_marks(final String regi12, final String marks12, final String exam_name12){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.update_marks api = adapter.create(AppConfig.update_marks.class);

        api.update_exam_marks(
                regi12,
                marks12,
                exam_name12,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("success", "" + resp);
                            update_marks_sqlite(regi12,marks12,exam_name12);
                            //Toast.makeText(insert_marks.this, "Successfully Added", Toast.LENGTH_SHORT).show();

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        pd.hide();
                        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {

            finish();
            Intent intent = new Intent(getApplicationContext(),display_marks.class);
            intent.putExtra("total",total);
            intent.putExtra("name", name_array);
            intent.putExtra("regi",regi_array);
            intent.putExtra("marks",marks_array);
            intent.putExtra("scroll_index",index);
            startActivity(intent);

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
