package com.roze.english.exam.roll_search;

/**
 * Created by emroz on 8/31/2017.
 */

public class std_marks_data {
    String ex_name;
    String ex_date;
    String ex_marks;

    public std_marks_data(String ex_name, String ex_date, String ex_marks) {
        this.ex_name = ex_name;
        this.ex_date = ex_date;
        this.ex_marks = ex_marks;
    }

    public String getEx_name() {
        return ex_name;
    }

    public void setEx_name(String ex_name) {
        this.ex_name = ex_name;
    }

    public String getEx_date() {
        return ex_date;
    }

    public void setEx_date(String ex_date) {
        this.ex_date = ex_date;
    }

    public String getEx_marks() {
        return ex_marks;
    }

    public void setEx_marks(String ex_marks) {
        this.ex_marks = ex_marks;
    }
}
