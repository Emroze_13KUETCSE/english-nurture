package com.roze.english.exam.preview;

/**
 * Created by emroz on 8/29/2017.
 */

public class WorldPopulation {
    private String rank;
    private String country;
    private String population;
    private boolean flag;

    public WorldPopulation( boolean flag, String rank, String country,
                            String population) {
        this.rank = rank;
        this.country = country;
        this.population = population;
        this.flag = flag;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

}
