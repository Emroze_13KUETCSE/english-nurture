package com.roze.english.exam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.exam.preview.display;
import com.roze.english.helper.AppConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class insert_marks extends AppCompatActivity {

    public static String PREFS_NAME="math";
    RequestQueue requestQueue,requestQueue1,requestQueue2;


    String showUrl = "http://englishnurturebd.com/jason.php";
    String showUrl2 = "http://englishnurturebd.com/json.php";
    String showUrl3 = "http://englishnurturebd.com/json1.php";

    String BASE_URL = "http://englishnurturebd.com/";

    DatabaseHelper db_helper;

    Cursor cursor = null;
    Cursor cursor_list;
    Cursor cursor_marks;
    Cursor cursor_marks_result = null;


    TextView tv_name,tv_regi,tv1,tv2,tv_count;
    Button btn_pre,btn_next,btn_preview,btn_complete;
    EditText et;

    String[][] array;
    String[] marks;
    String[] name;
    String[] regi;
    int count = 0;
    int total = 0;

    int hh = 0;

    ProgressDialog pd;

    boolean check = true;
    int roll_and_marks_count = 0;
    String exam_name_from_preference="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_marks);

        pd = new ProgressDialog(insert_marks.this);

        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_regi = (TextView) findViewById(R.id.tv_regi);
        tv1 = (TextView) findViewById(R.id.tv_class);
        tv2 = (TextView) findViewById(R.id.tv_batch);
        tv_count = (TextView) findViewById(R.id.tv_count);


        btn_pre = (Button) findViewById(R.id.btn_pre);
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_preview = (Button) findViewById(R.id.btn_preview);
        btn_complete = (Button) findViewById(R.id.btn_complete);


        et = (EditText) findViewById(R.id.et);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final String match_class = preference.getString("s_class","").toString();
        final String match_batch = preference.getString("s_batch","").toString();
        exam_name_from_preference = preference.getString("exam_name","").toString();

        db_helper = new DatabaseHelper(this);
        final SQLiteDatabase db = db_helper.getReadableDatabase();


        cursor_list=db.rawQuery("SELECT rowid _id, * FROM exam_list",null);
        if(preference.getString("class","").equals("true")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class,null);
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class,null);
            tv1.setText("Class "+match_class);
            tv2.setText("No batch");
            cursor_marks=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class,null);

        }
        else if(preference.getString("class","").equals("false")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            tv1.setText("Class "+match_class);
            tv2.setText(match_batch);
            cursor_marks=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);

        }

        array = new String[cursor.getCount()][2];
        name = new String[cursor.getCount()];
        marks = new String[cursor.getCount()];
        regi = new String[cursor.getCount()];
        total = cursor.getCount();

        for(int a = 0; a < cursor.getCount();a++){
            for(int b = 0; b < 2; b++){
                array[a][b] = "-1";
            }
            marks[a] = "-1";
            regi[a] = "-1";
        }

        int i =0;
        if(cursor.moveToFirst()) {
            do {
                array[i][0] = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
                regi[i] = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
                name[i] = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
                i++;

            }while(cursor.moveToNext());
        }


        tv_name.setText(name[0]);
        tv_regi.setText(array[0][0]);
        tv_count.setText(String.valueOf(count+1)+"/"+ String.valueOf(total));

        btn_pre.setBackgroundResource(R.drawable.border5);
        btn_pre.setTextColor(Color.BLACK);




    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("back","").equals("false")){
            count = Integer.valueOf(preference.getString("index",""));
            tv_name.setText(name[count]);
            tv_regi.setText(array[count][0]);
            if(!array[count][1].equals("-1")){
                et.setText(array[count][1]);
            }else {
                et.setText("");
            }
            tv_count.setText(String.valueOf(count+1)+"/"+ String.valueOf(total));
            editor.putString("back","true");
            editor.commit();
        }


        btn_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

                if(hh != 0){
                    String mark = et.getText().toString();
                    if(mark.equals("")){
                        array[count][1] = "-1";
                        marks[count] = "-1";
                    }
                    else {
                        array[count][1] = mark;
                        marks[count] = mark;
                    }
                }
                else {
                    hh = 1;
                }
                count--;
                if(count == 0){
                    btn_pre.setBackgroundResource(R.drawable.border5);
                    btn_pre.setTextColor(Color.BLACK);

                }
                if(count < 0){
                    count = 0;
                    vib.vibrate(500);
                }
                else {
                    tv_name.setText(name[count]);
                    tv_regi.setText(array[count][0]);
                    if(!array[count][1].equals("-1")){
                        et.setText(array[count][1]);
                    }
                    else {
                        et.setText("");
                    }
                    tv_count.setText(String.valueOf(count+1)+"/"+ String.valueOf(total));
                    vib.vibrate(100);



                }

            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_pre.setBackgroundResource(R.drawable.border2);
                btn_pre.setTextColor(Color.WHITE);
                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

                hh = 0;
                if(count == total){
                }
                else if(count < total){
                    vib.vibrate(100);
                    String mark = et.getText().toString();
                    if(mark.equals("")){
                        array[count][1] = "-1";
                        marks[count] = "-1";
                    }
                    else {
                        array[count][1] = mark;
                        marks[count] = mark;
                    }


                    count++;
                    //Toast.makeText(insert_marks.this, String.valueOf(count)+" "+String.valueOf(marks[count-1]), Toast.LENGTH_SHORT).show();
                    if(count < total){
                        tv_name.setText(name[count]);
                        tv_regi.setText(array[count][0]);
                        if(!array[count][1].equals("-1")){
                            et.setText(array[count][1]);
                        }

                        tv_count.setText(String.valueOf(count+1)+"/"+ String.valueOf(total));
                        if(array[count][1].equals("-1")){
                            et.setText("");
                        }
                    }
                    else {

                        array[count-1][1] = mark;
                        marks[count-1] = mark;
                        Toast.makeText(insert_marks.this, String.valueOf(marks[count-1]), Toast.LENGTH_SHORT).show();
                        btn_next.setBackgroundResource(R.drawable.border5);
                        btn_next.setTextColor(Color.BLACK);
                        vib.vibrate(500);
                        Toast.makeText(insert_marks.this, "All marks are added.Please press Complete Button.", Toast.LENGTH_SHORT).show();

                    }

                }


            }
        });

        btn_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),display.class);
                intent.putExtra("total",total);
                intent.putExtra("name", name);
                intent.putExtra("regi",regi);
                intent.putExtra("marks",marks);
                startActivity(intent);
            }
        });

        btn_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mark = et.getText().toString();
                if(count >= total){
                    count = total-1;
                    array[count][1] = mark;
                    marks[count] = mark;
                    count++;
                }
                else {
                    array[count][1] = mark;
                    marks[count] = mark;
                    count++;
                }
                new SweetAlertDialog(insert_marks.this, SweetAlertDialog.WARNING_TYPE)

                        .setTitleText("Upload Marks?")
                        .setContentText(total+" Marks will be uploaded")
                        .setConfirmText("Yes,upload it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {

                                pd.setMessage("Updating marks..."+String.valueOf(total));
                                pd.show();

                                Thread t = new Thread() {

                                    @Override
                                    public void run() {
                                        try {
                                            while (!isInterrupted()) {
                                                Thread.sleep(100);
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if(roll_and_marks_count >= total ){
                                                            check = false;
                                                            pd.setMessage("Successfully added");
                                                            pd.hide();
                                                            finish();
                                                        }
                                                        if(check){
                                                            check = false;
                                                            pd.setMessage("Updating marks..."+String.valueOf(roll_and_marks_count)+"/"+
                                                                    String.valueOf(total));
                                                            update_marks(regi[roll_and_marks_count],marks[roll_and_marks_count],exam_name_from_preference);

                                                        }
                                                        //Toast.makeText(MainActivity.this, String.valueOf(list.getFirstVisiblePosition()+list.getChildCount()), Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                            }
                                        } catch (InterruptedException e) {
                                        }
                                    }
                                };
                                t.start();


                                // Toast.makeText(MainActivity.this, "After "+preference.getString("net",""), Toast.LENGTH_SHORT).show();



                            }
                        })
                        .show();



            }
        });


    }
    private void update_marks_sqlite(String regi22, String marks22, String exam_name22){

        db_helper.update_exam_marks(regi22,marks22,exam_name22);
        //pd.hide();
        roll_and_marks_count++;
        check = true;
        //startActivity(new Intent(getApplicationContext(),choose_c_b_t.class));
    }
    private void update_marks(final String regi12, final String marks12, final String exam_name12){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.update_marks api = adapter.create(AppConfig.update_marks.class);

        api.update_exam_marks(
                regi12,
                marks12,
                exam_name12,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("success", "" + resp);
                            roll_and_marks_count++;
                            check = true;
                            //update_marks_sqlite(regi12,marks12,exam_name12);
                            //Toast.makeText(insert_marks.this, "Successfully Added", Toast.LENGTH_SHORT).show();

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        pd.hide();
                        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
