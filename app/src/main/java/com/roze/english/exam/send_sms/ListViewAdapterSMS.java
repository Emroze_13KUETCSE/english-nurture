package com.roze.english.exam.send_sms;

/**
 * Created by emroz on 8/30/2017.
 */

import android.graphics.Typeface;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.roze.english.R;
import com.roze.english.exam.preview.total_data;

/**
 * Created by emroz on 8/27/2017.
 */

public class ListViewAdapterSMS extends ArrayAdapter<total_data> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<total_data> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapterSMS(Context context, int resourceId,
                           List<total_data> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView big;
        TextView name;
        TextView regi;
        TextView marks;
        TextView on_off;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final com.roze.english.exam.send_sms.ListViewAdapterSMS.ViewHolder holder;
        if (view == null) {
            holder = new com.roze.english.exam.send_sms.ListViewAdapterSMS.ViewHolder();
            view = inflater.inflate(R.layout.s_by_exam_sms_preview_list, null);
            // Locate the TextViews in listview_item.xml
            holder.big = (TextView) view.findViewById(R.id.tv_big_l);
            holder.name = (TextView) view.findViewById(R.id.textView_std_name);
            holder.regi = (TextView) view.findViewById(R.id.textView_regi);
            holder.marks = (TextView) view.findViewById(R.id.tv_marks);
            holder.on_off = (TextView) view.findViewById(R.id.tv_on_off);


            Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

            holder.big.setTypeface(type);
            holder.name.setTypeface(type);
            holder.marks.setTypeface(type);
            holder.regi.setTypeface(type1);

            holder.on_off.setTypeface(type1);
            // Locate the ImageView in listview_item.xml
            //holder.flag = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (com.roze.english.exam.send_sms.ListViewAdapterSMS.ViewHolder) view.getTag();
        }
        // Capture position and set to the TextViews
        holder.big.setText(worldpopulationlist.get(position).getBig_latter());
        holder.name.setText(worldpopulationlist.get(position).getName());
        holder.regi.setText(worldpopulationlist.get(position).getRegi());
        holder.marks.setText(worldpopulationlist.get(position).getMarks());
        // Capture position and set to the ImageView
        //holder.flag.setChecked(worldpopulationlist.get(position).getFlag());
        return view;
    }


    public List<total_data> getWorldPopulation() {
        return worldpopulationlist;
    }



    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
