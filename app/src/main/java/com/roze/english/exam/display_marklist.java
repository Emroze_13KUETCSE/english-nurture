package com.roze.english.exam;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.exam.roll_search.display_std_marks;
import com.roze.english.helper.AppConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class display_marklist extends AppCompatActivity {
    public static String PREFS_NAME="math";
    RequestQueue requestQueue,requestQueue1,requestQueue2;


    String showUrl = "http://englishnurturebd.com/jason.php";
    String showUrl2 = "http://englishnurturebd.com/json.php";
    String showUrl3 = "http://englishnurturebd.com/json1.php";

    String BASE_URL = "http://englishnurturebd.com/";

    ProgressDialog pd;

    TextView tv1,tv2;

    ListView lv;

    DatabaseHelper db_helper,db_h;

    String str="";

    Button btn_send,load;

    String marks,regi_from_server;
    String[] regi_in,regi_out;

    int array_size = 0;

    int in,out,match_in,match_out=0;

    Cursor cursor = null;
    Cursor cursor_list;
    Cursor cursor_marks;
    Cursor cursor_marks_result = null;

    int count ,len;

    int check = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_marklist);

        tv1 = (TextView) findViewById(R.id.tv_class_name);
        tv2 = (TextView) findViewById(R.id.tv_batch_time);



        lv = (ListView) findViewById(R.id.sms_lv);
        pd = new ProgressDialog(display_marklist.this);

        btn_send = (Button) findViewById(R.id.btn_send);
        load = (Button) findViewById(R.id.btn_load);

        Toast.makeText(this, "display_marklist", Toast.LENGTH_SHORT).show();

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv1.setTypeface(type1);
        tv2.setTypeface(type1);
    }
    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        db_helper = new DatabaseHelper(this);
        final SQLiteDatabase db = db_helper.getReadableDatabase();



        final String match_class = preference.getString("s_class","").toString();
        final String match_batch = preference.getString("s_batch","").toString();



        lv.setDivider(null);
        lv.setDividerHeight(0);
        cursor_list=db.rawQuery("SELECT rowid _id, * FROM exam_list",null);

        if(preference.getString("class","").equals("true")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class,null);
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class,null);
            tv1.setText("Class "+match_class);
            tv2.setVisibility(View.INVISIBLE);
            cursor_marks=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class,null);

        }
        else if(preference.getString("class","").equals("false")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
            tv1.setText("Class "+match_class);
            tv2.setText(match_batch);
            cursor_marks=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);

        }
        cursor_marks todoAdapter = new cursor_marks(this, cursor);


        lv.setAdapter(todoAdapter);

        array_size = cursor.getCount();
        regi_in = new String[array_size];


        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setMessage("Loading Marks...");
                pd.show();

                onScroll_load_marks(lv);
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                LinearLayout l_d = (LinearLayout) view.findViewById(R.id.ll_delete);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll11);
                LinearLayout l3 = (LinearLayout) view.findViewById(R.id.ll22);
                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                final EditText send = (EditText) view.findViewById(R.id.editText_marks);

                str = tvv.getText().toString();

                Toast.makeText(display_marklist.this, String.valueOf(i), Toast.LENGTH_SHORT).show();

                l_d.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(display.this, str, Toast.LENGTH_SHORT).show();
                    }
                });

                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {



                    }
                });
                l2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editor.putString("temp_regiID",str);
                        editor.commit();
                        Intent inn = new Intent(getApplicationContext(), display_std_marks.class);
                        startActivity(inn);
                    }
                });
                l3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editor.putString("temp_regiID",str);
                        editor.commit();
                        Intent inn = new Intent(getApplicationContext(), display_std_marks.class);
                        startActivity(inn);
                    }
                });

                editor.putString("temp_regiID",str);
                editor.commit();
            }
        });



        if(preference.getString("exam_list","").equals("true")){
            final EditText[] et = new EditText[array_size];
            final TextView[] tv = new TextView[array_size];

            View[] vii = new View[array_size];




            if(array_size != 0){
                /*if(cursor_list.getCount()!=0){
                    if(cursor_marks.moveToFirst()){
                        do{
                            for(int j = 0; j < lv.getAdapter().getCount(); j++){
                                vii[j] = lv.getChildAt(j);
                                et[j] = (EditText) vii[j].findViewById(R.id.editText_marks);
                                tv[j] = (TextView) vii[j].findViewById(R.id.tv_regi_lv);
                                if(tv[j].getText().toString().equals(cursor.getString(cursor.getColumnIndexOrThrow("regi")))){
                                    et[j].setText(cursor.getString(cursor.getColumnIndexOrThrow(preference.getString("exam_date",null))));
                                }
                            }

                        }while (cursor_marks.moveToNext());
                        pd.hide();
                    }
                }*/

            }
            else{
                pd.hide();
                Toast.makeText(this, "Marks is empty", Toast.LENGTH_SHORT).show();
            }


        }

        final EditText[] number = new EditText[array_size];
        final TextView[] regi = new TextView[array_size];


        if(preference.getString("sms","").equals("true")){
            btn_send.setText("Send SMS");
        }
        else{
            btn_send.setText("OK");
        }

        final Cursor finalCursor = cursor;
        final Cursor finalCursor1 = cursor;
        final Cursor finalCursor2 = cursor_marks_result;

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(preference.getString("sms","").equals("true")){
                    send_sms(lv);
                }
                else{
                    if(preference.getString("excel","").equals("true")){

                        //finish();
                    }
                    else{
                        update_marks(lv);
                    }

                }




            }
        });
    }

    private void insert_in_sqlite(String regi,String marks){

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        DatabaseHelper db_helper = new DatabaseHelper(this);

        db_helper.update_marks_table(regi,marks,preference.getString("exam_date",""));
    }
    private void update_marks(final String regi, final String marks){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.update_marks api = adapter.create(AppConfig.update_marks.class);

        api.update_exam_marks(
                regi,
                marks,
                preference.getString("exam_date",""),
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("success", "" + resp);

                            insert_in_sqlite(regi,marks);


                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();

                        } /*catch (JSONException e) {
                            Log.d("JsonException", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        }*/
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        pd.hide();
                        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    // for sms delivery report
    BroadcastReceiver sendBroadcastReceiver = new display_marklist.SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new display_marklist.DeliverReceiver();

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);


    }

    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS delivered",
                            Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "sms not delivered",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {



            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    count++;
                    check = 0;

                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:



                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:


                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:


                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:


                    break;
            }

        }
    }

    private void SQLite2Excel(String name){
        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/GonitNiketan/Result";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        // Export SQLite DB as EXCEL FILE
        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), db_helper.DATABASE_NAME, directory_path);
        /*sqliteToExcel.exportAllTables("users.xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onCompleted(String filePath) {
                //Utils.showSnackBar(view, "Successfully Exported");
                Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Exception e) {

            }
        });*/

        sqliteToExcel.exportSingleTable(db_helper.TABLE_NAME4, name+".xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }
            @Override
            public void onCompleted(String filePath) {
                Toast.makeText(display_marklist.this, "File created", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(Exception e) {

            }
        });
    }

    public void onScroll_load_marks(AbsListView v)
    {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final int childCount = lv.getChildCount();

        requestQueue2 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST,
                showUrl2, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int j = 0; j < std.length(); j++) {
                        JSONObject student = std.getJSONObject(j);

                        for (int i = 0; i < childCount; i++)
                        {
                            View v1 = lv.getChildAt(i);
                            TextView tx = (TextView) v1.findViewById(R.id.tv_regi_lv);
                            EditText et = (EditText) v1.findViewById(R.id.editText_marks);

                            if(student.getString("regi").equals(tx.getText().toString())){
                                et.setText(student.getString(preference.getString("exam_date","")));
                            }
                        }


                    }

                    pd.hide();

                } catch (JSONException e) {

                    pd.hide();
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Please try again1", Toast.LENGTH_SHORT).show();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());

                pd.hide();
                Toast.makeText(getApplicationContext(), "No internet cxonnection", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue2.add(jsonObjectRequest2);


    }

    public void update_marks(AbsListView v){
        final int childCount = lv.getChildCount();


        pd.setMessage("Updating...");
        pd.show();

        if(haveNetworkConnection() && array_size != 0){
            for (int i = 0; i < childCount; i++)
            {
                View v1 = lv.getChildAt(i);
                TextView tx = (TextView) v1.findViewById(R.id.tv_regi_lv);
                EditText et = (EditText) v1.findViewById(R.id.editText_marks);

                update_marks(tx.getText().toString(),et.getText().toString());

            }

            pd.hide();
            Toast.makeText(getApplicationContext(), "Successfully registered", Toast.LENGTH_SHORT).show();
            finish();
        }
        else{
            pd.hide();
            Toast.makeText(getApplicationContext(), "No net connection or Marks is empty", Toast.LENGTH_SHORT).show();
        }

    }

   public void send_sms(AbsListView v){

       final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
       final SharedPreferences.Editor editor=preference.edit();

       final int childCount = lv.getChildCount();

       final String[] sms = {""};

       final Cursor finalCursor = cursor;
       final Cursor finalCursor1 = cursor;
       final Cursor finalCursor2 = cursor_marks_result;

       pd.setMessage("Sending SMS...");
       pd.show();

       if(finalCursor.moveToFirst()){


           len = childCount;

           Thread t = new Thread() {

               @Override
               public void run() {
                   try {
                       while (!isInterrupted()) {
                           Thread.sleep(1000);
                           runOnUiThread(new Runnable() {
                               @Override
                               public void run() {

                                   if(check == 0 && count < len){
                                       check = 1;
                                       View v12 = lv.getChildAt(count);

                                       EditText number = (EditText) v12.findViewById(R.id.editText_marks);

                                       if (number.getText().toString().equals("")) {
                                           sms[0] = finalCursor.getString(finalCursor.getColumnIndexOrThrow("full_name")) + "\n" +
                                                   "Regi : " + finalCursor.getString(finalCursor.getColumnIndexOrThrow("regi")) + "\n" +
                                                   "Batch : " + finalCursor.getString(finalCursor.getColumnIndexOrThrow("batch_time")) +
                                                   "\nExam Date : " + preference.getString("exam_date", "") + "\n"
                                                   + "Absent in this exam\nGonit Niketan,Sanjoy Kumar Shaha.";
                                       } else {
                                           sms[0] = finalCursor.getString(finalCursor.getColumnIndexOrThrow("full_name")) + "\n" +
                                                   "Regi : " + finalCursor.getString(finalCursor.getColumnIndexOrThrow("regi"))
                                                   + "\n" +
                                                   "Batch : " + finalCursor.getString(finalCursor.getColumnIndexOrThrow("batch_time")) +
                                                   "\nExam Date : " + preference.getString("exam_date", "") + "\n"
                                                   + "Marks : " + number.getText().toString() + "\nGonit Niketan,Sanjoy Kumar Shaha.";
                                       }
                                       //Toast.makeText(display_marklist.this, sms, Toast.LENGTH_SHORT).show();
                                       sendSMS(finalCursor.getString(finalCursor.getColumnIndexOrThrow("mobile")), sms[0]);
                                       finalCursor.moveToNext();
                                   }
                                   if(count == len){
                                       pd.hide();
                                       finish();
                                   }


                               }
                           });

                       }
                   } catch (InterruptedException e) {
                   }
               }
           };
           t.start();
       }

   }
}
