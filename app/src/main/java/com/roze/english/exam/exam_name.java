package com.roze.english.exam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;

import net.cachapa.expandablelayout.ExpandableLayout;

public class exam_name extends AppCompatActivity {
    private ExpandableLayout expandableLayout0;
    DatePicker d1;
    TextView tv1,tv_pick;
    Button btn_pick,btn_ok;
    String exam_date="";
    EditText ex_name,f_marks;

    public static String PREFS_NAME="math";

    String BASE_URL = "http://englishnurturebd.com/";

    DatabaseHelper db_helper = new DatabaseHelper(this);


    ProgressDialog pd;
    public SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_name);

        Toast.makeText(this, "exam_name", Toast.LENGTH_SHORT).show();

        pd = new ProgressDialog(exam_name.this);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv_pick = (TextView) findViewById(R.id.tv_pick);

        btn_pick = (Button) findViewById(R.id.btn_pick_date);
        btn_ok = (Button) findViewById(R.id.btn_ok);

        d1 = (DatePicker) findViewById(R.id.datePicker);

        ex_name = (EditText) findViewById(R.id.et_exam_name);

        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM exam_marks", null);

        Toast.makeText(this,String.valueOf(cursor2.getColumnCount()) , Toast.LENGTH_SHORT).show();

        tv_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        btn_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int day = d1.getDayOfMonth();
                int month = d1.getMonth() + 1;
                int year = d1.getYear();

                exam_date = String.valueOf(day) + " "+String.valueOf(month)+" "+String.valueOf(year);

                tv_pick.setText(exam_date);
                expandableLayout0.collapse();

            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!exam_date.equals("")){

                    editor.putString("exam_date",exam_date);
                    editor.putString("exam_name",ex_name.getText().toString());
                    editor.commit();
                    startActivity(new Intent(getApplicationContext(),choose_c_b_t.class));
                    finish();
                    /*if(db_helper.check_alter_marks_table(exam_date)){
                        add_exam_name();
                    }
                    else{
                        pd.hide();
                        Toast.makeText(exam_name.this, "Already Added.Please goto Exam list", Toast.LENGTH_SHORT).show();
                    }
*/

                }
            }
        });

    }

    /*private void alter_in_sqlite(){

        db_helper.insert_into_marks_list(ex_name.getText().toString(),exam_date);
        db_helper.alter_marks_table(exam_date);
        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM exam_marks", null);

        Toast.makeText(this,String.valueOf(cursor2.getColumnCount()) , Toast.LENGTH_SHORT).show();
        pd.hide();
        //startActivity(new Intent(getApplicationContext(),choose_c_b_t.class));
    }

    private void add_exam_name(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        AppConfig.add_exam api = adapter.create(AppConfig.add_exam.class);

        api.add_exam_name(
                exam_date,
                ex_name.getText().toString(),
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            resp = reader.readLine();
                            Log.d("success", "" + resp);
                            alter_in_sqlite();
                            Toast.makeText(exam_name.this, "Successfully Added", Toast.LENGTH_SHORT).show();

                            //JSONObject jObj = new JSONObject(resp);
                            *//*int success = jObj.getInt("success");

                            if(success == 1){
                                Toast.makeText(getApplicationContext(), "Successfully inserted", Toast.LENGTH_SHORT).show();
                            } else{
                                Toast.makeText(getApplicationContext(), "Insertion Failed", Toast.LENGTH_SHORT).show();
                            }*//*

                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();

                        } *//*catch (JSONException e) {
                            Log.d("JsonException", e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        }*//*
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        pd.hide();
                        Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }*/
}
