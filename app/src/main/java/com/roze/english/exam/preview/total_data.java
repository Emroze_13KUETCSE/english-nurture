package com.roze.english.exam.preview;

/**
 * Created by emroz on 8/29/2017.
 */

public class total_data {
    private String name;
    private String regi;
    private String marks;
    private String big_latter;

    public total_data(String big_latter, String name, String regi,
                            String marks) {
        this.big_latter = big_latter;
        this.name = name;
        this.regi = regi;
        this.marks = marks;
    }

    public String getName() {
        return name;
    }

    public String getBig_latter() {
        return big_latter;
    }

    public void setBig_latter(String big_latter) {
        this.big_latter = big_latter;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegi() {
        return regi;
    }

    public void setRegi(String regi) {
        this.regi = regi;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }
}
