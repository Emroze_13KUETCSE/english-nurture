package com.roze.english.exam;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.roze.english.R;

/**
 * Created by emroz on 4/26/2017.
 */

public class cursor_std_marks extends CursorAdapter {

    String db_month;

    public cursor_std_marks(Context context, Cursor cursor) {
        super(context, cursor, 0);


    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.s_by_std_marks_list, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template

        TextView big_latter = (TextView) view.findViewById(R.id.tv_big_lv);
        TextView x_name = (TextView) view.findViewById(R.id.textView_exam_name);
        TextView x_date = (TextView) view.findViewById(R.id.textView_exam_date);

        Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

        big_latter.setTypeface(type);
        x_date.setTypeface(type);
        x_name.setTypeface(type1);


        // Extract properties from cursor
        String str_big_latter ;
        String str_f_name = cursor.getString(cursor.getColumnIndexOrThrow("exam_name"));
        String str_n_name = cursor.getString(cursor.getColumnIndexOrThrow("exam_date"));


        // String str_paid = cursor.getString(cursor.getColumnIndexOrThrow(db_month));




        // Populate fields with extracted properties

        x_name.setText(str_f_name);
        x_date.setText(str_n_name);




        //tvPriority.setText(String.valueOf(priority));
    }
}
