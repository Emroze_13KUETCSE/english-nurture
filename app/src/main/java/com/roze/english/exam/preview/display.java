package com.roze.english.exam.preview;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.roze.english.R;

import java.util.ArrayList;
import java.util.List;

public class display extends AppCompatActivity {
    public static String PREFS_NAME="math";
    ListView list;
    ListViewAdapter listviewadapter;
    List<total_data> worldpopulationlist = new ArrayList<total_data>();
    String[] name;
    String[] regi;
    String[] marks;
    String[] temp_regi;
    String[] temp_marks;
    int total=0;
    int non_zero = 0;
    String[] str_big_latter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display2);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        Intent intent = getIntent();
        total = intent.getIntExtra("total",0);
        name = new String[total];
        temp_regi = new String[total];
        temp_marks = new String[total];
        str_big_latter = new String[total];
        name = intent.getStringArrayExtra("name");
        temp_regi = intent.getStringArrayExtra("regi");
        temp_marks = intent.getStringArrayExtra("marks");

        for(int j = 0; j < temp_regi.length;j++){
            if(!temp_regi[j].equals("-1")){
                non_zero++;
            }
        }

        for(int k = 0; k < name.length; k++){
            if(name[k].equals("")){
                str_big_latter[k]="";
            }
            else {
                char c = name[k].charAt(0);
                str_big_latter[k] = String.valueOf(c);
            }
        }

        regi = new String[total];
        marks = new String[total];
        int b =0;
        int c = 0;
        for(int a = 0 ; a < temp_marks.length; a++){
            if(temp_marks[a].equals("-1")){
                marks[a] = "";
            }
            else {
                marks[a] = temp_marks[a];
            }
            if(!temp_regi[a].equals("-1")){
                regi[a] = temp_regi[a];
            }
        }

        for (int i = 0; i < regi.length; i++) {
            total_data worldpopulation = new total_data(str_big_latter[i],
                    name[i], regi[i], marks[i]);
            worldpopulationlist.add(worldpopulation);
        }

        // Locate the ListView in listview_main.xml
        list = (ListView) findViewById(R.id.listview);

        // Pass results to ListViewAdapter Class
        listviewadapter = new ListViewAdapter(this, R.layout.s_by_temp_preview_list,
                worldpopulationlist);

        // Binds the Adapter to the ListView
        list.setAdapter((ListAdapter) listviewadapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                editor.putString("index",String.valueOf(i));
                editor.putString("back","false");
                editor.commit();
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            editor.putString("back","true");
            editor.commit();
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
