package com.roze.english.exam.send_sms;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.exam.preview.total_data;
import com.roze.english.send_sms.sms_display;
import com.roze.english.service.MyExamService;
import com.roze.english.service.MyService;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class display_sms_list extends AppCompatActivity {
    TextView roll_list,tv_ex_name,tv_ex_class,tv_ex_batch,tv_ex_date,tv_highest_marks,tv_ex_h_regi;

    private String ex_name;
    private String ex_class;
    private String ex_batch;
    private String ex_date;

    public static String PREFS_NAME="math";
    ListView list;
    ListViewAdapterSMS listviewadapter;
    List<total_data> worldpopulationlist = new ArrayList<total_data>();
    String[] name;
    String[] regi;
    String[] marks;
    String[] temp_regi;
    String[] temp_marks;
    int total=0;
    int non_zero = 0;
    String[] str_big_latter;

    int change_index=0;

    EditText et_f_marks;
    Button btn_send;

    Boolean buffer_roll_check = false;
    int not_zero=0;
    int g = 0;

    String[] regi_in,regi_out;

    String[] buffer ;
    int[] regi_buffer;
    int[] regi_buffer_not_send;
    String[] send_marks;


    int array_size = 0;

    int in,out,match_in,match_out=0;

    int count_roll = 0;
    int count=0,len;
    int check = 0;

    int ii = -1;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    ProgressDialog pd;

    String h_marks="";
    String[] send_name;

    CheckBox cb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_sms_list);

        tv_ex_batch = (TextView) findViewById(R.id.tv_ex_batch);
        tv_ex_name = (TextView) findViewById(R.id.tv_ex_name);
        tv_ex_class = (TextView) findViewById(R.id.tv_ex_class);
        tv_ex_date = (TextView) findViewById(R.id.tv_ex_date);
        tv_highest_marks = (TextView) findViewById(R.id.tv_highest_marks);
        tv_ex_h_regi = (TextView) findViewById(R.id.tv_ex_h_regi);

        et_f_marks = (EditText)findViewById(R.id.et_full_marks);

        btn_send = (Button) findViewById(R.id.btn_send);

        cb = (CheckBox) findViewById(R.id.checkBox);
        roll_list  = (TextView) findViewById(R.id.tv_roll_list);


        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");

        tv_ex_batch.setTypeface(type1);
        tv_ex_h_regi.setTypeface(type1);
        tv_ex_date.setTypeface(type1);
        tv_ex_name.setTypeface(type);
        tv_ex_class.setTypeface(type1);
        tv_highest_marks.setTypeface(type);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        ex_class = preference.getString("s_class","").toString();
        ex_batch = preference.getString("s_batch","").toString();
        ex_date = preference.getString("s_date","").toString();
        ex_name = preference.getString("ex_name_a","").toString();
        change_index = Integer.valueOf(preference.getString("changed_index",""));


        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor = null;

        if(ex_batch.equals("")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + ex_class,null);

        }
        else if(!ex_batch.equals("")){
            cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + ex_class + " AND batch_time" + " = '"+ex_batch+"'",null);
        }

        String temp_ex_name="";
        for(int i = 0; i < ex_name.length();i++){
            if(ex_name.charAt(i) == 's' && ex_name.charAt(i+1) == 'k'){
                break;
            }
            temp_ex_name = temp_ex_name.concat(String.valueOf(ex_name.charAt(i)));
        }

        if(ex_class.equals("1")){
            tv_ex_class.setText("Class: 10");
        }
        else {
            tv_ex_class.setText("Class: "+ex_class);
        }

        tv_ex_batch.setText("Batch: "+ex_batch);
        tv_ex_name.setText("Exam Number: "+temp_ex_name);
        tv_ex_date.setText("Exam Date: "+ex_date);

        Intent intent = getIntent();
        total = intent.getIntExtra("total",0);
        name = new String[total];
        temp_regi = new String[total];
        temp_marks = new String[total];
        str_big_latter = new String[total];
        name = intent.getStringArrayExtra("name");
        temp_regi = intent.getStringArrayExtra("regi");
        temp_marks = intent.getStringArrayExtra("marks");

        for(int j = 0; j < temp_regi.length;j++){
            if(!temp_regi.equals("-1")){
                non_zero++;
            }
        }

        for(int k = 0; k < name.length; k++){
            if(name[k].equals("")){
                str_big_latter[k]="";
            }
            else {
                char c = name[k].charAt(0);
                str_big_latter[k] = String.valueOf(c);
            }
        }

        regi = new String[temp_regi.length];
        marks = new String[temp_regi.length];
        int b =0;
        int c = 0;
        for(int a = 0 ; a < temp_marks.length; a++){
            if(temp_marks[a].equals("-1")){
                marks[b] = "";
                b++;
            }
            else {
                marks[b] = temp_marks[a];
                b++;
            }

            if(!temp_regi[a].equals("-1")){
                regi[c] = temp_regi[a];
                c++;
            }
        }

        float[] marrrks = new float[temp_marks.length];
        for(int i = 0; i < temp_marks.length; i++){
            if(!temp_marks[i].equals("-1") || !temp_marks[i].equals("")){
                try{
                    marrrks[i] = Float.valueOf(temp_marks[i]);
                }
                catch (NumberFormatException e){
                    marrrks[i] = -1;
                }

            }
            else {
                marrrks[i] = -1;
            }

        }


        int[] indeex = new int[marrrks.length];
        for(int i =0; i < marrrks.length;i++){
            indeex[i] = -1;
        }
        float temp = marrrks[0];
        int jj = 0;
        for(int i = 0; i < marrrks.length; i++){
            if(marrrks[i] >= temp){
                if(marrrks[i] == temp){
                    indeex[jj] = i;
                    jj++;
                }
                else if(marrrks[i] > temp){
                    for(int ii =0; ii < marrrks.length;ii++){
                        indeex[ii] = -1;
                    }
                    jj=0;
                    temp = marrrks[i];
                    indeex[jj] = i;
                    jj++;
                }
            }

        }

        String indii="";

        for(int i = 0; i < indeex.length;i++){
            if(indeex[i] != -1){
                indii=indii.concat(String.valueOf(regi[indeex[i]])+" ");
            }
        }

        h_marks = String.valueOf(temp);
        tv_highest_marks.setText(h_marks);
        if(indii.length() > 50){
            String temppp="";
            for(int i =0; i < 50; i++){
                temppp = temppp.concat(String.valueOf(indii.charAt(i)));
            }
            temppp = temppp+"...";
            tv_ex_h_regi.setText(temppp);

        }else{
            tv_ex_h_regi.setText(indii);
        }


        final String finalIndii = indii;
        tv_highest_marks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(display_sms_list.this, SweetAlertDialog.NORMAL_TYPE)

                        .setTitleText("Highest Marks Regi :")
                        .setContentText(finalIndii)
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
        tv_ex_h_regi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(display_sms_list.this, SweetAlertDialog.NORMAL_TYPE)

                        .setTitleText("Highest Marks Regi :")
                        .setContentText(finalIndii)
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });

        for (int i = 0; i < regi.length; i++) {
            total_data worldpopulation = new total_data(str_big_latter[i],
                    name[i], regi[i], marks[i]);
            worldpopulationlist.add(worldpopulation);
        }

        // Locate the ListView in listview_main.xml
        list = (ListView) findViewById(R.id.listview);

        list.setSelection(21);

        // Pass results to ListViewAdapter Class
        listviewadapter = new ListViewAdapterSMS(this, R.layout.s_by_exam_sms_preview_list,
                worldpopulationlist);

        // Binds the Adapter to the ListView
        list.setAdapter((ListAdapter) listviewadapter);


        regi_buffer = new int[total];

        array_size = total;
        regi_in = new String[array_size];

        count = 1;

        final String finalTemp_ex_name = temp_ex_name;
        final int[] ary = new int[list.getCount()];
        for(int i = 0; i < list.getCount(); i++){
            ary[i] = -1;
        }
        final int[] Count = {0};
        final String[] aaa = {""};

        int cc=0;
        for(int kk = 0; kk < array_size; kk++){
            if(!marks[kk].equals("")){
                ary[kk] = kk;
                int r = Integer.valueOf(regi[kk]);
                regi_buffer[kk] = r;
                cc++;
            }
        }
        roll_list.setText(String.valueOf(cc));


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv_name = (TextView) view.findViewById(R.id.textView_std_name);
                TextView regii = (TextView) view.findViewById(R.id.textView_regi);
                TextView markss = (TextView) view.findViewById(R.id.tv_marks);

                String t_name = tv_name.getText().toString();
                String t_regi = regii.getText().toString();
                String t_marks = markss.getText().toString();

                String rrr = t_regi;

                if(ary[i] == -1){
                    ary[i] = i;
                }
                else {
                    ary[i] = -1;
                }


                int r = Integer.valueOf(rrr);


                    for(int a = 0; a < regi_buffer.length; a++){

                        if(regi_buffer[a] == r){
                            regi_buffer[a] = 0;
                            buffer_roll_check = true;
                        }
                    }

                if(buffer_roll_check == false){
                    regi_buffer[i] = r;
                    count_roll++;
                }
                String r_list = "";
                int zero=0;
                for(int c = 0; c < regi_buffer.length; c++){
                    if(regi_buffer[c] != 0){
                        zero++;
                    }
                }
                if(zero == regi_buffer.length){
                   roll_list.setText("");
                }

                roll_list.setText(String.valueOf(zero));
                buffer_roll_check = false;


            }
        });

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Toast.makeText(MainActivity.this, String.valueOf(list.getFirstVisiblePosition()), Toast.LENGTH_SHORT).show();
                                //Toast.makeText(MainActivity.this, aaa[0], Toast.LENGTH_SHORT).show();
                                for(int a = list.getFirstVisiblePosition(); a < list.getFirstVisiblePosition()+list.getChildCount(); a++ ){

                                    if(ary[a] == -1){
                                        //lv.getChildAt(a-lv.getFirstVisiblePosition()).setBackgroundColor(Color.WHITE);
                                        View vv = list.getChildAt(a-list.getFirstVisiblePosition());
                                        TextView tv = (TextView) vv.findViewById(R.id.tv_on_off);
                                        tv.setTextColor(Color.BLACK);
                                        tv.setBackgroundResource(R.drawable.border_off);
                                        tv.setText("Off");
                                    }
                                    else if(ary[a] == a){
                                        //lv.getChildAt(a-lv.getFirstVisiblePosition()).setBackgroundColor(Color.rgb(85,238,163));
                                        View vv = list.getChildAt(a-list.getFirstVisiblePosition());
                                        TextView tv = (TextView) vv.findViewById(R.id.tv_on_off);
                                        tv.setTextColor(Color.WHITE);
                                        tv.setBackgroundResource(R.drawable.border2);
                                        tv.setText("On");
                                    }

                                }

                                //Toast.makeText(MainActivity.this, String.valueOf(list.getFirstVisiblePosition()+list.getChildCount()), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

        final Cursor finalCursor1 = cursor;

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    int bb=0;
                    if(finalCursor1.moveToFirst()){
                        do{

                            if(ex_class.equals(finalCursor1.getString(finalCursor1.getColumnIndexOrThrow("course")))){
                                int re = Integer.valueOf(finalCursor1.getString(finalCursor1.getColumnIndexOrThrow("regi")));
                                regi_buffer[bb] = re;
                                ary[bb]=bb;
                                bb++;
                            }
                        }
                        while(finalCursor1.moveToNext());
                    }
                    roll_list.setText(String.valueOf(array_size));
                }
                else {
                    for(int i = 0; i < array_size;i++){
                        ary[i] = -1;
                        regi_buffer[i] = 0;
                    }
                    roll_list.setText(String.valueOf(0));

                }
            }
        });

        pd = new ProgressDialog(display_sms_list.this);

        final Cursor finalCursor = cursor;
        final String finalTemp_ex_name1 = temp_ex_name;
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int c = 0; c < regi_buffer.length; c++){
                    if(regi_buffer[c] != 0){
                        not_zero++;
                    }
                }

                regi_buffer_not_send = new int[not_zero];
                send_marks = new String[not_zero];
                send_name = new String[not_zero];
                //len = finalCursor.getCount() - not_zero;
                //buffer = new String[finalCursor.getCount() - not_zero];

                len = not_zero;
                buffer = new String[not_zero];

                int e =0;
                for(int c = 0; c < regi_buffer.length; c++){
                    if(regi_buffer[c] != 0){
                        regi_buffer_not_send[e] = regi_buffer[c];
                        e++;
                    }
                }

                int xx = 0;
                for(int x=0; x < regi.length;x++){
                    for(int y = 0; y < regi_buffer_not_send.length; y++ ){
                        String rrrr = String.valueOf(regi_buffer_not_send[y]);
                        if(regi[x].equals(rrrr)){
                            send_marks[xx] = marks[x];
                            send_name[xx] = name[x];
                            xx++;
                        }
                    }
                }

                int buf = 0;
                if(finalCursor.moveToFirst()){

                    do{

                        if(ex_class.equals(finalCursor.getString(finalCursor.getColumnIndexOrThrow("course")))){
                            int re = Integer.valueOf(finalCursor.getString(finalCursor.getColumnIndexOrThrow("regi")));
                            int f = 0;
                            for (int d = 0 ; d < regi_buffer_not_send.length; d++){
                                if(re == regi_buffer_not_send[d]){
                                    String numm = finalCursor.getString(finalCursor.getColumnIndexOrThrow("mobile"));
                                    if(!numm.equals("")
                                            ||numm != null
                                            ||numm.charAt(0) == '1'
                                            || numm.charAt(0) == '0'
                                            || numm.contains("+880")
                                            ){
                                        if(numm.charAt(0) == '1'){
                                            String sss_n = "0";
                                            sss_n = sss_n.concat(numm);
                                            buffer[buf] = sss_n;
                                            buf++;
                                        }
                                        else if(numm.charAt(0) == '0'){
                                            buffer[buf] = numm;
                                            buf++;
                                        }
                                    }
                                    else {
                                        buffer[buf] = "0";
                                        buf++;
                                        //error_no++;
                                    }
                                    break;
                                }
                                /*else {
                                    f++;
                                }*/
                            }
                            /*if(f == regi_buffer_not_send.length){
                                String numm = finalCursor.getString(finalCursor.getColumnIndexOrThrow("mobile"));
                                if(!numm.equals("")
                                        ||numm != null
                                        ||numm.charAt(0) == '1'
                                        || numm.charAt(0) == '0'
                                        || numm.contains("+880")
                                        ){
                                    if(numm.charAt(0) == '1'){
                                        String sss_n = "0";
                                        sss_n = sss_n.concat(numm);
                                        buffer[buf] = sss_n;
                                        buf++;
                                    }
                                    else if(numm.charAt(0) == '0'){
                                        buffer[buf] = numm;
                                        buf++;
                                    }
                                }
                            }*/
                            //regi_buffer[buf] = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
                        }
                    }
                    while(finalCursor.moveToNext());
                }


                new SweetAlertDialog(display_sms_list.this, SweetAlertDialog.WARNING_TYPE)

                        .setTitleText("Send SMS?")
                        .setContentText(not_zero+" SMS will be sent")
                        .setConfirmText("Yes,send it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {

                                sDialog.dismissWithAnimation();

                                len = buffer.length;

                                Intent intent=new Intent(display_sms_list.this, MyExamService.class);
                                Bundle b1=new Bundle();
                                b1.putStringArray("number_ary",buffer);
                                intent.putExtras(b1);

                                Bundle b2=new Bundle();
                                b2.putString("ex_full_marks",et_f_marks.getText().toString());
                                intent.putExtras(b2);

                                Bundle b3=new Bundle();
                                b3.putString("h_marks",h_marks);
                                intent.putExtras(b3);

                                Bundle b4=new Bundle();
                                b4.putIntArray("regi_ary",regi_buffer_not_send);
                                intent.putExtras(b4);

                                Bundle b5=new Bundle();
                                b5.putStringArray("marks_ary",send_marks);
                                intent.putExtras(b5);

                                Bundle b6=new Bundle();
                                b6.putStringArray("name_ary",send_name);
                                intent.putExtras(b6);

                                Bundle b7=new Bundle();
                                b7.putString("ex_name",finalTemp_ex_name1);
                                intent.putExtras(b7);

                                Bundle b8=new Bundle();
                                b8.putString("ex_date",ex_date);
                                intent.putExtras(b8);

                                startService(intent);

                                Thread t = new Thread() {

                                    @Override
                                    public void run() {
                                        try {
                                            while (!isInterrupted()) {
                                                Thread.sleep(500);
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {


                                                            count = preference.getInt("sms_count",0);
                                                            //Toast.makeText(sms_display.this, String.valueOf(len), Toast.LENGTH_SHORT).show();
                                                            if(count <= len){


                                                                pd.setCanceledOnTouchOutside(false);
                                                                pd.setMessage("Sending SMS..."+String.valueOf(count)+"/"+
                                                                        String.valueOf(len)+"\nRegi: "+regi_buffer_not_send[count]);
                                                                pd.show();


                                                            }




                                                    }
                                                });

                                            }
                                        } catch (InterruptedException e) {
                                        }
                                    }
                                };
                                t.start();

                            }
                        })
                        .show();



                not_zero=0;
            }
        });
    }

   /* // for sms delivery report
    BroadcastReceiver sendBroadcastReceiver = new display_sms_list.SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new display_sms_list.DeliverReceiver();

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliveryBroadcastReciever);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        SmsManager sm = SmsManager.getDefault();
        ArrayList<String> parts =sm.divideMessage(message);
        int numParts = parts.size();

        Toast.makeText(this, String.valueOf(numParts), Toast.LENGTH_SHORT).show();

        ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
        ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));

        for (int i = 0; i < numParts; i++) {
            sentIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(SENT), 0));
            deliveryIntents.add(PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(DELIVERED), 0));
        }

        sm.sendMultipartTextMessage(phoneNumber,null, parts, sentIntents, deliveryIntents);

    }

    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Toast.makeText(getBaseContext(), "SMS delivered",
                       //     Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    //Toast.makeText(getBaseContext(), "sms not delivered",
                       //     Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {



            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Toast.makeText(context, "OK"+String.valueOf(count), Toast.LENGTH_SHORT).show();
                    count++;
                    g++;
                    check=0;


                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(context, "Generic", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(context, "No service", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(context, "NULL PDU", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(context, "Radio OFF", Toast.LENGTH_SHORT).show();

                    break;
            }

        }
    }

    public static int[] removeElement(int[] original, int element){
        int[] n = new int[original.length - 1];
        System.arraycopy(original, 0, n, 0, element );
        System.arraycopy(original, element+1, n, element, original.length - element-1);
        return n;
    }*/
}
