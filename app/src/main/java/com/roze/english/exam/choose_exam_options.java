package com.roze.english.exam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.roze.english.R;
import com.roze.english.exam.exam_mark_list.choose;
import com.roze.english.exam.send_sms.choose_sms;
import com.roze.english.excel.choose_excel;
import com.roze.english.students;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class choose_exam_options extends AppCompatActivity {
    TextView tv_insert_m,tv_show_m,tv_send_s,tv_payment,tv0,tv1,tv2,tv3,tv4,tv_excel;
    public static String PREFS_NAME="math";

    String BASE_URL = "http://englishnurturebd.com/";

    String showUrl = "http://englishnurturebd.com/jason1.php";
    String showUrl2 = "http://englishnurturebd.com/json.php";

    RequestQueue requestQueue,requestQueue1,requestQueue2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_exam_options);


        //etc();

        tv_insert_m = (TextView) findViewById(R.id.tv_t);
        tv_show_m = (TextView) findViewById(R.id.tv_e);
        tv_send_s = (TextView) findViewById(R.id.tv_d);
        tv_payment = (TextView) findViewById(R.id.tv_p);
        tv_excel = (TextView) findViewById(R.id.tv_excel);

        tv0 = (TextView) findViewById(R.id.tv0);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");


        tv0.setTypeface(type);
        tv1.setTypeface(type);
        tv2.setTypeface(type);
        tv3.setTypeface(type);
        tv4.setTypeface(type);


        tv_insert_m.setTypeface(type1);
        tv_show_m.setTypeface(type1);
        tv_send_s.setTypeface(type1);
        tv_payment.setTypeface(type1);
        Toast.makeText(this, "choose_exam_options", Toast.LENGTH_SHORT).show();

        tv_insert_m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),exam_name.class));
            }
        });
        tv_show_m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),choose_regi.class));
            }
        });
        tv_send_s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putString("sms","true");
                editor.putString("excel","false");
                editor.commit();
                startActivity(new Intent(getApplicationContext(),choose_sms.class));
            }
        });
        tv_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putString("sms","false");
                editor.putString("excel","false");
                editor.commit();
                startActivity(new Intent(getApplicationContext(),choose.class));
            }
        });
        tv_excel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putString("sms","false");
                editor.putString("excel","true");
                editor.commit();
                startActivity(new Intent(getApplicationContext(),choose_excel.class));
            }
        });


    }

    public void etc(){
        requestQueue1 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST,
                showUrl2, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i < std.length(); i++) {
                        JSONObject student = std.getJSONObject(i);

                        students std_item = new students();

                        if(i>=0 && i <= 9){
                            Toast.makeText(choose_exam_options.this, student.getString("EXAM 1sk10 9 2017sk7sk"), Toast.LENGTH_SHORT).show();
                        }

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                    Toast.makeText(choose_exam_options.this, e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                Toast.makeText(choose_exam_options.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue1.add(jsonObjectRequest2);

    }
}
