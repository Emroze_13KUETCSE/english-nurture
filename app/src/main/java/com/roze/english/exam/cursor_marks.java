package com.roze.english.exam;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.roze.english.R;

/**
 * Created by emroz on 4/25/2017.
 */

public class cursor_marks extends CursorAdapter {

    String db_month;
    public static String PREFS_NAME="math";
    public cursor_marks(Context context, Cursor cursor) {
        super(context, cursor, 0);


    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.s_by_marks_list, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template

        TextView big_latter = (TextView) view.findViewById(R.id.tv_big_lv);
        TextView f_name = (TextView) view.findViewById(R.id.tv_full_name_lv);
        TextView n_name = (TextView) view.findViewById(R.id.tv_nick_name_lv);
        TextView regi_ID = (TextView) view.findViewById(R.id.tv_regi_lv);
        EditText marks = (EditText) view.findViewById(R.id.editText_marks);

        Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

        big_latter.setTypeface(type);
        f_name.setTypeface(type);
        n_name.setTypeface(type1);
        regi_ID.setTypeface(type1);
        marks.setTypeface(type1);


        // Extract properties from cursor
        String str_big_latter ;
        String str_f_name = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
        String str_n_name = cursor.getString(cursor.getColumnIndexOrThrow("nick_name"));
        String str_regi = cursor.getString(cursor.getColumnIndexOrThrow("regi"));


        // String str_paid = cursor.getString(cursor.getColumnIndexOrThrow(db_month));
        if(str_f_name.equals("")){
            str_big_latter="";
        }
        else {
            char c = str_f_name.charAt(0);
            str_big_latter = String.valueOf(c);
        }
        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        String match_class = preference.getString("s_class","").toString();
        String match_batch = preference.getString("s_batch","").toString();
        String ex_date = preference.getString("exam_date","").toString();


        /*DatabaseHelper db_helper = new DatabaseHelper(context);

        final Cursor cursor_list,cursor_marks;
        SQLiteDatabase db = db_helper.getReadableDatabase();
        cursor_list=db.rawQuery("SELECT rowid _id, * FROM exam_list",null);
        cursor_marks=db.rawQuery("SELECT rowid _id,* FROM exam_marks WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);

        Toast.makeText(context, String.valueOf(cursor_marks.getCount()), Toast.LENGTH_SHORT).show();
        //Toast.makeText(context, cursor_marks.getString(), Toast.LENGTH_SHORT).show();
        if(cursor_list.getCount()!=0){
            if(cursor_marks.moveToFirst()){
                do{

                    //Toast.makeText(context, cursor.getString(cursor.getColumnIndexOrThrow("regi")), Toast.LENGTH_SHORT).show();
                        if(str_regi.equals(cursor_marks.getString(cursor_marks.getColumnIndexOrThrow("regi")))) {
                            marks.setText(cursor_marks.getString(cursor_marks.getColumnIndexOrThrow("'"+ex_date+"'")));
                        }

                }while (cursor_marks.moveToNext());

            }
        }*/
        // Populate fields with extracted properties
        big_latter.setText(str_big_latter);
        f_name.setText(str_f_name);
        n_name.setText(str_n_name);
        regi_ID.setText(str_regi);




        //tvPriority.setText(String.valueOf(priority));
    }
}
