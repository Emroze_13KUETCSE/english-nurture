package com.roze.english.exam.roll_search;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.Std_details;

import java.util.ArrayList;
import java.util.List;

public class display_std_marks extends AppCompatActivity {
    public static String PREFS_NAME="math";

    ProgressDialog pd;

    TextView tv1,tv2;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    String str="";

    Button btn_send;

    String temp_regi;

    String[] exam_list,exam_name_list,exam_date,exam_marks;
    String ex_class;
    int exam_list_count = 0;

    TextView tv_regi;

    ListViewAdapter_marks listviewadapter;
    List<std_marks_data> worldpopulationlist = new ArrayList<std_marks_data>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_std_marks);

        tv1 = (TextView) findViewById(R.id.tv_class_name);
        tv2 = (TextView) findViewById(R.id.tv_batch_time);
        tv_regi = (TextView) findViewById(R.id.tv_regi);



        lv = (ListView) findViewById(R.id.lv);
        pd = new ProgressDialog(display_std_marks.this);

        btn_send = (Button) findViewById(R.id.btn_std);

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv1.setTypeface(type1);
        tv2.setTypeface(type1);
        tv_regi.setTypeface(type1);

        Toast.makeText(this, "display_std_marks", Toast.LENGTH_SHORT).show();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        temp_regi = preference.getString("temp_regiID","");

        tv_regi.setText(temp_regi);

        ex_class = String.valueOf(temp_regi.charAt(2));

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putString("temp_regiID",temp_regi);
                editor.commit();
                Intent inn = new Intent(getApplicationContext(), Std_details.class);
                startActivity(inn);
            }
        });


        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor11,cursor12,cursor;

        cursor=db.rawQuery("SELECT rowid _id, * FROM students",null);
        cursor11=db.rawQuery("SELECT rowid _id, * FROM exam_list",null);
        cursor12=db.rawQuery("SELECT rowid _id, * FROM exam_marks",null);

        if(cursor.moveToFirst()){

            do{

                if(cursor.getString(cursor.getColumnIndexOrThrow("regi")).equals(temp_regi)){
                    tv1.setText(cursor.getString(cursor.getColumnIndexOrThrow("full_name")));
                    tv2.setText(cursor.getString(cursor.getColumnIndexOrThrow("nick_name")));

                }
            }
            while(cursor.moveToNext());
        }

        exam_list = new String[cursor11.getCount()];
        exam_date = new String[cursor11.getCount()];

        for(int i = 0; i < cursor11.getCount();i++){
            exam_list[i] = "0";
            exam_date[i] = "0";
        }

        if(cursor11.moveToFirst()){
            do{
                if(cursor11.getString(cursor11.getColumnIndexOrThrow("class")).equals(ex_class)){
                    exam_list[exam_list_count] = cursor11.getString(cursor11.getColumnIndexOrThrow("exam_name"));
                    exam_date[exam_list_count] = cursor11.getString(cursor11.getColumnIndexOrThrow("exam_date"));
                    exam_list_count++;
                }
            }
            while(cursor11.moveToNext());
        }

        String[] send_exam_list = new String[exam_list_count];
        String[] send_exam_date = new String[exam_list_count];

        int a =0;
        for(int i =0; i < exam_list.length;i++){
            if(!exam_list[i].equals("0")){
                send_exam_list[a] = exam_list[i];
                send_exam_date[a] = exam_date[i];
                a++;
            }
        }

        exam_marks = new String[exam_list_count];
        exam_name_list = new String[exam_list_count];

        if(cursor12.moveToFirst()){
            do{
                if(cursor12.getString(cursor12.getColumnIndexOrThrow("regi")).equals(temp_regi)){

                    for(int i = 0; i < send_exam_list.length;i++){
                        exam_marks[i] = cursor12.getString(cursor12.getColumnIndexOrThrow(send_exam_list[i]));
                    }
                    break;
                }
            }
            while(cursor12.moveToNext());
        }

        for(int j = 0; j < send_exam_list.length;j++){
            String temp_ex_name="";
            for(int i = 0; i < send_exam_list[j].length();i++){
                if(send_exam_list[j].charAt(i) == 's' && send_exam_list[j].charAt(i+1) == 'k'){
                    break;
                }
                temp_ex_name = temp_ex_name.concat(String.valueOf(send_exam_list[j].charAt(i)));
            }
            exam_name_list[j] = temp_ex_name;
        }

        for (int i = 0; i < exam_list_count; i++) {
            std_marks_data worldpopulation = new std_marks_data(
                    exam_name_list[i], send_exam_date[i], exam_marks[i]);
            worldpopulationlist.add(worldpopulation);
        }

        listviewadapter = new ListViewAdapter_marks(this, R.layout.s_by_std_marks_list,
                worldpopulationlist);

        // Binds the Adapter to the ListView
        lv.setAdapter((ListAdapter) listviewadapter);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


            }
        });





    }

}

