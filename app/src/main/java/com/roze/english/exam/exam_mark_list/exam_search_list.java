package com.roze.english.exam.exam_mark_list;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.exam.exam_list;
import com.roze.english.helper.AppConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class exam_search_list extends AppCompatActivity {
    public static String PREFS_NAME="math";

    ProgressDialog pd;

    TextView tv1,tv2;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    String ex_name;

    Cursor cursor1;
    Cursor cursor;

    Cursor cursor_marks_result;

    String[] exam_name1;
    String exam_date1;
    String[] exam_f_name1;
    int length_array = 0;

    String[] name;
    String[] regi;
    String[] marks;

    String[][] joint_marks;
    String[][] joint_name;

    int total =0;

    search_list_adapter listviewadapter;
    List<search_list_element> list_data = new ArrayList<search_list_element>();


    String BASE_URL = "http://englishnurturebd.com/";

    String showUrl2 = "http://englishnurturebd.com/json.php";
    String showUrl3 = "http://englishnurturebd.com/jason1.php";


    RequestQueue requestQueue1,requestQueue2;

    int len = 0;
    int aa = 0;
    String match_date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_search_list);

        Intent intent = getIntent();
        length_array = intent.getIntExtra("len",0);
        //Toast.makeText(this, String.valueOf(length_array), Toast.LENGTH_SHORT).show();
        exam_name1 = new String[length_array];
        exam_f_name1 = new String[length_array];
        exam_name1 = intent.getStringArrayExtra("ex_name");
        exam_f_name1 = intent.getStringArrayExtra("ex_f_name");

        tv1 = (TextView) findViewById(R.id.tv_class_name);

        lv = (ListView) findViewById(R.id.sms_lv);
        pd = new ProgressDialog(exam_search_list.this);

        /*for(int aaaa = 0; aaaa < length_array; aaaa++){
            Toast.makeText(this, exam_name1[aaaa], Toast.LENGTH_SHORT).show();
        }*/

        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv1.setTypeface(type1);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final String match_class = preference.getString("s_class","").toString();
        final String match_batch = preference.getString("s_batch","").toString();
        match_date = preference.getString("s_date","").toString();


        Toast.makeText(exam_search_list.this, "class "+ match_class, Toast.LENGTH_SHORT).show();

        for (int i = 0; i < length_array; i++) {
            search_list_element worldpopulation = new search_list_element(exam_f_name1[i],
                    exam_name1[i], match_date);
            list_data.add(worldpopulation);
        }

        listviewadapter = new search_list_adapter(this, R.layout.s_by_exam_list,
                list_data);

        // Binds the Adapter to the ListView
        lv.setAdapter((ListAdapter) listviewadapter);

        /*Cursor cursor5;

        if(preference.getString("class","").equals("true")){
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = '" + match_class+"'",null);
            cursor=db.rawQuery("SELECT rowid _id, * FROM exam_list WHERE exam_date" + " = '"+match_date+"'"+" AND class = '" + match_class+ "'",null);
            cursor1=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = '" + match_class+"'",null);
            cursor5=db.rawQuery("SELECT rowid _id, * FROM exam_list",null);

            //Toast.makeText(this, String.valueOf(cursor.getColumnName(3)), Toast.LENGTH_SHORT).show();
            //Toast.makeText(this, match_class+" "+match_date, Toast.LENGTH_SHORT).show();
        }
        else if(preference.getString("class","").equals("false")){
            cursor_marks_result=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = '" + match_class + "' AND batch_time" + " = '"+match_batch+"'",null);
            cursor=db.rawQuery("SELECT rowid _id, * FROM exam_list WHERE " + "class" + " = '" + match_class + "' AND batch" + " = '"+match_batch+"'"+ " AND exam_date" + " = '"+match_date+"'",null);
            cursor1=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = " + match_class + " AND batch_time" + " = '"+match_batch+"'",null);
        }

        cursor_exam_list todoAdapter = new cursor_exam_list(this, cursor);
        lv.setAdapter(todoAdapter);*/





        //Toast.makeText(this, cursor_marks_result.getColumnName(5)+" "+String.valueOf(cursor_marks_result.getColumnCount()), Toast.LENGTH_SHORT).show();


        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tvv = (TextView) view.findViewById(R.id.tv_nick_name_lv);
                TextView tvv1 = (TextView) view.findViewById(R.id.tv_hide);
                final int j = i;
                TextView del = (TextView) view.findViewById(R.id.tv_del);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll2);

                final String date = tvv.getText().toString();
                final String name1 = tvv1.getText().toString();

                String temp_ex_name="";
                for(int iq = 0; iq < name1.length();iq++){
                    if(name1.charAt(iq) == 's' && name1.charAt(iq+1) == 'k'){
                        break;
                    }
                    temp_ex_name = temp_ex_name.concat(String.valueOf(name1.charAt(iq)));
                }
                new SweetAlertDialog(exam_search_list.this, SweetAlertDialog.WARNING_TYPE)

                        .setTitleText("Delete this item??")
                        .setContentText(temp_ex_name+" will be deleted")
                        .setConfirmText("Yes,delete it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {

                                pd.setMessage("Deleting...");
                                pd.show();
                                delete_from_server(j,name1,date);
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();


                return true;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv = (TextView) view.findViewById(R.id.tv_hide);
                ex_name = tv.getText().toString();
                TextView del = (TextView) view.findViewById(R.id.tv_del);
                TextView tvv = (TextView) view.findViewById(R.id.tv_nick_name_lv);
                editor.putString("ex_name_a",ex_name);
                editor.commit();
                final String date = tvv.getText().toString();

                pd.setMessage("Loading Marks...");
                pd.show();

                query_sqlite(match_class,ex_name);
                //etc(match_class,ex_name);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void delete_from_sqlite(String ex_name){
        //db_helper.delete_exam_list(ex_name);
        db_helper.delete_exam_marks_column(ex_name);
    }
    private void delete_from_server(final int pos,final String exam_name, String exam_date ){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        AppConfig.delete_exam_list api = adapter.create(AppConfig.delete_exam_list.class);

        api.delete_ex_list_f_server(
                exam_name,
                exam_date,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                           // delete_from_sqlite(exam_name);
                            Toast.makeText(getApplicationContext(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            pd.hide();

                            String [] temp = new String[exam_name1.length-1];
                            String [] temp1 = new String[exam_name1.length-1];
                            int a =0;
                            for(int i = 0; i < exam_name1.length;i++){
                                if(i != pos){
                                    temp[a] = exam_name1[i];
                                    temp1[a] = exam_f_name1[i];
                                    a++;
                                }
                            }
                            int len = exam_name1.length - 1;
                            exam_name1 = new String[len];
                            exam_f_name1 = new String[len];

                            for (int i = 0; i < len; i++){
                                exam_name1[i]= temp[i];
                                exam_f_name1[i] = temp1[i];
                            }

                            search_list_adapter listviewadapter1;
                            List<search_list_element> list_data1 = new ArrayList<search_list_element>();

                            for (int i = 0; i < len; i++) {
                                search_list_element worldpopulation1 = new search_list_element(exam_f_name1[i],
                                        exam_name1[i], match_date);
                                list_data1.add(worldpopulation1);
                            }

                            listviewadapter1 = new search_list_adapter(getApplicationContext(), R.layout.s_by_exam_list,
                                    list_data1);

                            // Binds the Adapter to the ListView
                            lv.setAdapter((ListAdapter) listviewadapter1);



                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.hide();
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    public  void query_sqlite(final String s_class,final String ex_name){
        SQLiteDatabase db = db_helper.getWritableDatabase();

        Cursor cursor=null;

        cursor=db.rawQuery("SELECT rowid _id, * FROM students",null);
        aa=0;
        len = 0;
        Toast.makeText(exam_search_list.this, "classxxx "+ s_class, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, String.valueOf(cursor.getCount()), Toast.LENGTH_SHORT).show();
        if (cursor.moveToFirst()) {
            do {
                if(cursor.getString(cursor.getColumnIndexOrThrow("course")).equals(s_class)){

                    len++;

                    //name[aa] = "no";
                    //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                }
            } while (cursor.moveToNext());
        }
        regi = new String[len];
        name = new String[len];
        marks = new String[len];
        joint_marks = new  String[len][2];
        joint_name = new  String[len][2];

        if (cursor.moveToFirst()) {
            do {
                if(cursor.getString(cursor.getColumnIndexOrThrow("course")).equals(s_class)){

                    joint_name[aa][0] = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
                    joint_name[aa][1] = cursor.getString(cursor.getColumnIndexOrThrow("full_name"));
                    aa++;
                    //name[aa] = "no";
                    //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                }
            } while (cursor.moveToNext());
        }
        Arrays.sort(joint_name, new Comparator<String[]>() {
            @Override
            public int compare(final String[] entry1, final String[] entry2) {
                final String time1 = entry1[0];
                final String time2 = entry2[0];
                return time1.compareTo(time2);
            }
        });

        for(int a = 0; a < len; a++){
            regi[a] = joint_marks[a][0];
            marks[a] = joint_marks[a][1];
            name[a] = joint_name[a][1];
        }

        Toast.makeText(exam_search_list.this, "here"+ String.valueOf(len), Toast.LENGTH_SHORT).show();

        etc(s_class,ex_name);

    }
    public void etc(final String s_class,final String ex_name){

        requestQueue1 = Volley.newRequestQueue(getApplicationContext());

        aa=0;
        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST,
                showUrl2, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    /*for (int i = 0; i <= std.length(); i++) {
                        if(i == std.length()){
                            regi = new String[len];
                            name = new String[len];
                            marks = new String[len];
                            joint_marks = new  String[len][2];
                            joint_name = new  String[len][2];


                            //Toast.makeText(exam_search_list.this, String.valueOf(len), Toast.LENGTH_SHORT).show();


                            //etc1(s_class);

                            break;
                        }
                        JSONObject student = std.getJSONObject(i);

                        if( student.getString("course").equals(s_class)){
                            len++;
                        }

                    }*/

                    for (int ii = 0; ii < std.length(); ii++) {
                        JSONObject student = std.getJSONObject(ii);

                        if(student.getString("course").equals(s_class)){

                            joint_marks[aa][0] = student.getString("regi");
                            joint_marks[aa][1] = student.getString(ex_name);
                            //name[aa] = "no";
                            aa++;
                            //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                        }

                    }
                    //array sorting
                    Arrays.sort(joint_marks, new Comparator<String[]>() {
                        @Override
                        public int compare(final String[] entry1, final String[] entry2) {
                            final String time1 = entry1[0];
                            final String time2 = entry2[0];
                            return time1.compareTo(time2);
                        }
                    });
                    for(int a = 0; a < len; a++){
                        regi[a] = joint_marks[a][0];
                        marks[a] = joint_marks[a][1];
                        name[a] = joint_name[a][1];
                    }
                    Intent intent = new Intent(getApplicationContext(),display_marks.class);
                    intent.putExtra("total",len);
                    intent.putExtra("name", name);
                    intent.putExtra("regi",regi);
                    intent.putExtra("marks",marks);
                    intent.putExtra("scroll_index",0);
                    startActivity(intent);
                    aa=0;
                    len=0;
                    pd.hide();



                } catch (JSONException e) {

                    e.printStackTrace();

                    pd.hide();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue1.add(jsonObjectRequest2);
        requestQueue1.getCache().clear();
        requestQueue1.getCache().remove(showUrl2);


    }
    public void etc1(final String s_class){

        aa=0;
        requestQueue2 = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest3 = new JsonObjectRequest(Request.Method.POST,
                showUrl3, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                try {
                    JSONArray std = response.getJSONArray("students");
                    for (int i = 0; i < std.length(); i++) {

                            JSONObject student = std.getJSONObject(i);

                            if(student.getString("course").equals(s_class)){

                                joint_name[aa][0] = student.getString("regi");
                                joint_name[aa][1] = student.getString("s_full");
                                //name[aa] = "no";
                                aa++;
                                //Toast.makeText(choose.this, student.getString("exam_name"), Toast.LENGTH_SHORT).show();
                            }


                    }

                    //array sorting
                    Arrays.sort(joint_name, new Comparator<String[]>() {
                        @Override
                        public int compare(final String[] entry1, final String[] entry2) {
                            final String time1 = entry1[0];
                            final String time2 = entry2[0];
                            return time1.compareTo(time2);
                        }
                    });

                    for(int a = 0; a < len; a++){
                        regi[a] = joint_marks[a][0];
                        marks[a] = joint_marks[a][1];
                        name[a] = joint_name[a][1];
                    }

                    Toast.makeText(exam_search_list.this, "here"+String.valueOf(len), Toast.LENGTH_SHORT).show();
                    /*Intent intent = new Intent(getApplicationContext(),display_marks.class);
                    intent.putExtra("total",len);
                    intent.putExtra("name", name);
                    intent.putExtra("regi",regi);
                    intent.putExtra("marks",marks);
                    intent.putExtra("scroll_index",0);
                    startActivity(intent);*/
                    aa=0;
                    len=0;
                    pd.hide();

                } catch (JSONException e) {

                    e.printStackTrace();

                    pd.hide();
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.append(error.getMessage());
                pd.hide();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue2.add(jsonObjectRequest3);
        //requestQueue2.getCache().clear();
        //requestQueue2.getCache().remove(showUrl3);

    }


}
