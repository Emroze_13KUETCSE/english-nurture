package com.roze.english.exam.roll_search;

/**
 * Created by emroz on 8/31/2017.
 */
import android.graphics.Typeface;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.roze.english.R;

/**
 * Created by emroz on 8/27/2017.
 */

public class ListViewAdapter_marks extends ArrayAdapter<std_marks_data> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<std_marks_data> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapter_marks(Context context, int resourceId,
                           List<std_marks_data> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView name;
        TextView date;
        TextView marks;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final com.roze.english.exam.roll_search.ListViewAdapter_marks.ViewHolder holder;
        if (view == null) {
            holder = new com.roze.english.exam.roll_search.ListViewAdapter_marks.ViewHolder();
            view = inflater.inflate(R.layout.s_by_std_marks_list, null);
            // Locate the TextViews in listview_item.xml

            holder.name = (TextView) view.findViewById(R.id.textView_exam_name);
            holder.date = (TextView) view.findViewById(R.id.textView_exam_date);
            holder.marks = (TextView) view.findViewById(R.id.tv_marks);

            Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");


            holder.name.setTypeface(type);
            holder.marks.setTypeface(type);
            holder.date.setTypeface(type1);

            // Locate the ImageView in listview_item.xml
            //holder.flag = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (com.roze.english.exam.roll_search.ListViewAdapter_marks.ViewHolder) view.getTag();
        }
        // Capture position and set to the TextViews
        holder.name.setText(worldpopulationlist.get(position).getEx_name());
        holder.date.setText(worldpopulationlist.get(position).getEx_date());
        holder.marks.setText(worldpopulationlist.get(position).getEx_marks());
        // Capture position and set to the ImageView
        //holder.flag.setChecked(worldpopulationlist.get(position).getFlag());
        return view;
    }


    public List<std_marks_data> getWorldPopulation() {
        return worldpopulationlist;
    }



    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
