package com.roze.english.exam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.roze.english.R;
import com.roze.english.exam.roll_search.display_std_marks;

public class choose_regi extends AppCompatActivity {
    public static String PREFS_NAME="math";

    EditText et_roll;
    Button btn_search;
    String roll_et="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_regi);
        et_roll = (EditText) findViewById(R.id.et_roll);
        btn_search = (Button) findViewById(R.id.btn_search);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        Toast.makeText(this, "choose_regi", Toast.LENGTH_SHORT).show();
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roll_et = et_roll.getText().toString();

                if(roll_et.equals("") || roll_et.length() < 6){
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

                    vib.vibrate(500);
                    Toast.makeText(choose_regi.this, "Invalid", Toast.LENGTH_SHORT).show();
                    et_roll.setText("");
                }
                else{
                    editor.putString("temp_regiID",roll_et);
                    editor.commit();
                    Intent inn = new Intent(getApplicationContext(), display_std_marks.class);
                    startActivity(inn);
                }

            }
        });

    }
}
