package com.roze.english.exam;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.roze.english.DatabaseHelper;
import com.roze.english.R;
import com.roze.english.exam.exam_mark_list.display_marks;
import com.roze.english.helper.AppConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class exam_list extends AppCompatActivity {
    public static String PREFS_NAME="math";

    String BASE_URL = "http://englishnurturebd.com/";

    ProgressDialog pd;

    TextView tv1,tv2;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);


    String s_class,s_date;
    Cursor cursor,cursor1;

    String[] name;
    String[] regi;
    String[] marks;
    int total =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_list);
        tv1 = (TextView) findViewById(R.id.tv_class_name);



        lv = (ListView) findViewById(R.id.sms_lv);
        pd = new ProgressDialog(exam_list.this);


        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/font6.ttf");
        tv1.setTypeface(type1);

        Toast.makeText(this, "exam_list", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor5;



        //lv.setDivider(null);
        //lv.setDividerHeight(0);

        cursor5=db.rawQuery("SELECT rowid _id, exam_name, exam_date, class, batch FROM exam_list",null);

        cursor_exam_list todoAdapter = new cursor_exam_list(this, cursor5);


        lv.setAdapter(todoAdapter);


        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tvv = (TextView) view.findViewById(R.id.tv_nick_name_lv);
                TextView tvv1 = (TextView) view.findViewById(R.id.tv_hide);

                TextView del = (TextView) view.findViewById(R.id.tv_del);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll2);

                final String date = tvv.getText().toString();
                final String name1 = tvv1.getText().toString();

                String temp_ex_name="";
                for(int iq = 0; iq < name1.length();iq++){
                    if(name1.charAt(iq) == 's' && name1.charAt(iq+1) == 'k'){
                        break;
                    }
                    temp_ex_name = temp_ex_name.concat(String.valueOf(name1.charAt(iq)));
                }
                new SweetAlertDialog(exam_list.this, SweetAlertDialog.WARNING_TYPE)

                        .setTitleText("Delete this item??")
                        .setContentText(temp_ex_name+" will be deleted")
                        .setConfirmText("Yes,delete it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {

                                pd.setMessage("Deleting...");
                                pd.show();
                                delete_from_server(name1,date);

                            }
                        })
                        .show();


                return true;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                TextView tvv = (TextView) view.findViewById(R.id.tv_nick_name_lv);
                TextView tvv1 = (TextView) view.findViewById(R.id.tv_hide);

                TextView del = (TextView) view.findViewById(R.id.tv_del);
                LinearLayout l2 = (LinearLayout) view.findViewById(R.id.ll2);

                final String date = tvv.getText().toString();
                final String name1 = tvv1.getText().toString();


                del.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        new AlertDialog.Builder(exam_list.this)
                                .setTitle("Delete entry")
                                .setMessage("Are you sure you want to delete this entry?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        pd.setMessage("Deleting...");
                                        pd.show();
                                        delete_from_server(name1,date);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(R.drawable.alert)
                                .show();
                    }
                });

                s_date = date;
                int h=0;

                for(int g = 0; g < name1.length(); g++){
                    if(name1.charAt(g) == 's' && name1.charAt(g+1) == 'k'){
                        if(h == 1){
                           s_class = String.valueOf(name1.charAt(g+2));
                           break;
                        }
                        h++;
                    }
                }

                cursor=db.rawQuery("SELECT rowid _id, * FROM exam_marks WHERE " + "course" + " = '" + s_class+"'",null);
                cursor1=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + "course" + " = '" + s_class+"'",null);

                total = cursor1.getCount();
                regi = new String[total];
                name = new String[total];
                marks = new String[total];

                int g=0;
                if(cursor1.moveToFirst()) {
                    do {

                        regi[g] = cursor1.getString(cursor1.getColumnIndexOrThrow("regi"));
                        name[g] = cursor1.getString(cursor1.getColumnIndexOrThrow("full_name"));
                        g++;

                    }while(cursor1.moveToNext());
                }

                int ii = 0;
                if(cursor.moveToFirst()) {
                    do {
                        marks[ii] = cursor.getString(cursor.getColumnIndexOrThrow(name1));
                        ii++;

                    }while(cursor.moveToNext());
                }


                    Intent intent = new Intent(getApplicationContext(),display_marks.class);
                    intent.putExtra("total",total);
                    intent.putExtra("name", name);
                    intent.putExtra("regi",regi);
                    intent.putExtra("marks",marks);
                    startActivity(intent);


                editor.putString("exam_date",date);
                editor.putString("ex_name",name1);
                editor.putString("ex_name_a",name1);
                editor.commit();






                Toast.makeText(exam_list.this, date, Toast.LENGTH_SHORT).show();




            }
        });



    }
    private void delete_from_sqlite(String ex_name){
        //db_helper.delete_exam_list(ex_name);
        db_helper.delete_exam_marks_column(ex_name);
    }
    private void delete_from_server(final String exam_name, String exam_date ){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL) //Setting the Root URL
                .build();

        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        AppConfig.delete_exam_list api = adapter.create(AppConfig.delete_exam_list.class);

        api.delete_ex_list_f_server(
                exam_name,
                exam_date,
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
                            String resp;
                            delete_from_sqlite(exam_name);
                            Toast.makeText(getApplicationContext(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            pd.hide();
                            finish();
                            //Toast.makeText(display.this, result.toString(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),exam_list.class));


                        } catch (IOException e) {
                            Log.d("Exception", e.toString());
                            pd.hide();
                            Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.hide();
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    public void delete_exam_marks_column(String exam_name){
        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor;
        cursor=db.rawQuery("SELECT rowid _id,* FROM exam_list",null);


        String[] mark_list = new String[cursor.getCount()-1];
        int i =0;
        if(cursor.moveToFirst()){

            do{

                if(!exam_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("exam_name")))){
                    mark_list[i] = cursor.getString(cursor.getColumnIndexOrThrow("exam_name"));
                    i++;
                }
                else {
                    Toast.makeText(this, cursor.getString(cursor.getColumnIndexOrThrow("exam_name")), Toast.LENGTH_SHORT).show();
                }
            }
            while(cursor.moveToNext());
        }


        Cursor cursor1;
        cursor1 = db.rawQuery("SELECT rowid _id,* FROM exam_marks",null);
        String[][] ex_marks = new String[cursor1.getCount()][cursor1.getColumnCount()-2];

        int a = 0;
        int b = 0;
        if(cursor1.moveToFirst()){
            do{
                ex_marks[a][0] = cursor.getString(cursor.getColumnIndexOrThrow("regi"));
                ex_marks[a][1] = cursor.getString(cursor.getColumnIndexOrThrow("batch_time"));
                ex_marks[a][2] = cursor.getString(cursor.getColumnIndexOrThrow("course"));

                for(int ii =0; ii < mark_list.length; ii++){
                    ex_marks[a][ii+3]= cursor.getString(cursor.getColumnIndexOrThrow(mark_list[ii]));
                }
                a++;
            }while(cursor1.moveToNext());
        }

        /*String query2 = "DROP TABLE IF EXISTS "+TABLE_NAME2;
        db.execSQL(query2);
        db.execSQL(TABLE_CREATE2);

        ContentValues values = new ContentValues();

        for(int aa = 0; aa < cursor1.getCount(); aa++){
            values.put(COLUMN_regi, ex_marks[a][0]);
            values.put(COLUMN_batch_time,ex_marks[a][1]);
            values.put(COLUMN_course,ex_marks[a][2]);

            db.insert(TABLE_NAME2,null,values);
        }

        for(int k = 0; k < mark_list.length;k++){
            alter_marks_table(mark_list[k]);
        }

        for(int jj = 0 ; jj < cursor1.getCount();jj++){
            for(int kk = 0; kk < mark_list.length;kk++){
                update_marks_table(ex_marks[jj][0],ex_marks[jj][kk+3],mark_list[kk]);
            }
        }*/


    }
}
