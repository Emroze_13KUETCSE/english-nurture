package com.roze.english.exam;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.roze.english.R;

/**
 * Created by emroz on 4/25/2017.
 */

public class cursor_exam_list  extends CursorAdapter {

    String db_month;

    public cursor_exam_list(Context context, Cursor cursor) {
        super(context, cursor, 0);


    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.s_by_exam_list, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template

        TextView big_latter = (TextView) view.findViewById(R.id.tv_big_lv);
        TextView f_name = (TextView) view.findViewById(R.id.tv_full_name_lv);
        TextView n_name = (TextView) view.findViewById(R.id.tv_nick_name_lv);
        TextView del = (TextView) view.findViewById(R.id.tv_del);
        TextView hhh = (TextView) view.findViewById(R.id.tv_hide);


        Typeface type1 = Typeface.createFromAsset(context.getAssets(),"fonts/font6.ttf");
        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");

        big_latter.setTypeface(type);
        f_name.setTypeface(type);
        n_name.setTypeface(type1);
        del.setTypeface(type1);




        // Extract properties from cursor
        String str_big_latter ;
        String str_f_name = cursor.getString(cursor.getColumnIndexOrThrow("exam_name"));
        String str_n_name = cursor.getString(cursor.getColumnIndexOrThrow("exam_date"));


        String temp_ex_name="";
        for(int i = 0; i < str_f_name.length();i++){
            if(str_f_name.charAt(i) == 's' && str_f_name.charAt(i+1) == 'k'){
                break;
            }
            temp_ex_name = temp_ex_name.concat(String.valueOf(str_f_name.charAt(i)));
        }

        // String str_paid = cursor.getString(cursor.getColumnIndexOrThrow(db_month));

        // Populate fields with extracted properties

        hhh.setText(str_f_name);
        f_name.setText(temp_ex_name);
        n_name.setText(str_n_name);

        //tvPriority.setText(String.valueOf(priority));
    }
}
