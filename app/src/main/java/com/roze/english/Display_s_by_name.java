package com.roze.english;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Display_s_by_name extends AppCompatActivity {

    public static String PREFS_NAME="math";

    TextView tv1;

    ListView lv;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    String match_name="",f_n_name="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_s_by_name);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        tv1 = (TextView) findViewById(R.id.tv_no_result);
        tv1.setVisibility(View.INVISIBLE);

        lv = (ListView) findViewById(R.id.s_by_name_lv);

        match_name = preference.getString("temp_name","").toString();
        f_n_name = preference.getString("f_n_name","").toString();



        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor todoCursor = db.rawQuery("SELECT rowid _id, full_name, batch_time, course, school, nick_name FROM students", null);

        Cursor cursor;

        cursor=db.rawQuery("SELECT rowid _id, * FROM students WHERE " + f_n_name + " = '"+match_name+"'",null);



        int length = cursor.getCount();
        int i =0;
        if(cursor.moveToFirst()){

            do{
                if(f_n_name.equals("full_name")){
                    if(!match_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("full_name")))){
                        i++;
                    }
                }
                else if(f_n_name.equals("nick_name")){
                    if(!match_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("nick_name")))){
                        i++;
                    }
                }

            }
            while(cursor.moveToNext());
        }

        if(length == i){
            tv1.setVisibility(View.VISIBLE);
        }


        CursorAdaptar_s_by_r_class_and_batch todoAdapter = new CursorAdaptar_s_by_r_class_and_batch(this, cursor);
// Attach cursor adapter to the ListView
        lv.setAdapter(todoAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView tvv = (TextView) view.findViewById(R.id.tv_regi_lv);
                String str = tvv.getText().toString();

                editor.putString("temp_regiID",str);
                editor.commit();

                Intent inn = new Intent(getApplicationContext(), Std_details.class);
                startActivity(inn);

                Toast.makeText(getApplicationContext(), str,
                        Toast.LENGTH_SHORT).show();

            }
        });
    }
}
